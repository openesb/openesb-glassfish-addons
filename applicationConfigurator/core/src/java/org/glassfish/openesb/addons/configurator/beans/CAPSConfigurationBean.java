/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-jbi-components.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-jbi-components.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)CAPSConfigurationBean.java
 *
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */
package org.glassfish.openesb.addons.configurator.beans;
import java.io.IOException;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.el.ELContext;
import javax.el.MethodExpression;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;


import com.sun.appserv.management.config.ConnectorConnectionPoolConfig;
import com.sun.appserv.management.config.CustomResourceConfig;
import org.glassfish.openesb.addons.configurator.model.ConfigurableBean;
import org.glassfish.openesb.addons.configurator.model.ConfigurationBean;
import org.glassfish.openesb.addons.configurator.model.JMSConfigurationBean;
import org.glassfish.openesb.addons.configurator.model.JMSConfigurationHelper;
import org.glassfish.openesb.addons.configurator.model.JMSProperty;
import org.glassfish.openesb.addons.configurator.util.BundleUtil;
import org.glassfish.openesb.addons.configurator.util.CapsConstants;
import org.glassfish.openesb.addons.configurator.util.JMSUtil;
import com.sun.enterprise.tools.admingui.util.AMXUtil;
import com.sun.webui.jsf.component.Alert;
import com.sun.webui.jsf.component.DropDown;
import com.sun.webui.jsf.component.PasswordField;
import com.sun.webui.jsf.component.Property;
import com.sun.webui.jsf.component.PropertySheet;
import com.sun.webui.jsf.component.PropertySheetSection;
import com.sun.webui.jsf.component.TextArea;
import com.sun.webui.jsf.component.TextField;
import com.sun.webui.jsf.model.Option;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Configuration Editor control and model class.
 * @author Sun Microsystems
 * @version $Revision $
 */
public class CAPSConfigurationBean implements Serializable {
	protected transient Logger mLog = Logger
			.getLogger(CAPSConfigurationBean.class.getName());

	private String title = " title";
	private Alert mAlert = new Alert();
	private NumberFormat mNumberFormat = NumberFormat.getInstance();
	private DateFormat mDateFormat = DateFormat.getDateInstance();
	transient private boolean mSaveButtonDisabled, mRenderAlertMessage;
	private PropertySheet mPropertySheet = new PropertySheet();
	private final static String CONFIG_PROP_NAME = "Configuration";
	transient private String mAlertMessage = "", mAlertSummary, mAlertType;
	private static String CONNECTOR = "connectorConnectionPool",
			ENVIRONMENT = "environmentOverride";
	private Map<String, String> mId2Secname = new HashMap<String, String>();
	private Map<String, String> mId2Propname = new HashMap<String, String>();
	private static String ALERT_TYPE_ERROR = "error",
			ALERT_TYPE_SUCCESS = "success", ALERT_TYPE_WARNING = "warning",
			ALERT_TYPE_INFO = "info";

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public boolean isSaveButtonDisabled() {
		return mSaveButtonDisabled;
	}

	public void setSaveButtonDisabled(boolean saveButtonDisabled) {
		this.mSaveButtonDisabled = saveButtonDisabled;
	}

	public void setConfigurationAlert(Alert configurationAlert) {
		//mAlert = configurationAlert;
		mRenderAlertMessage = false;
		mAlert = configurationAlert;
		mAlertMessage = "";
	}

	/**
	 * Getter method for the Configuration Alert.
	 * @return The alert object.
	 */
	public Alert getConfigurationAlert() {
		mAlert.setId("configure-alert");
		mAlert.setType(ALERT_TYPE_SUCCESS);
		mAlert.setDetail(getAlertMessage());
		mAlert.setSummary(getAlertSummary());
		mAlert.setRendered(getAlertMessageRendered());
		mRenderAlertMessage = false;
		mAlertMessage = "";
		return mAlert;
	}

	public void setAlertType(String type) {
		mAlertType = type;
	}

	public String getAlertType() {
		return mAlertType;
	}

	/** Get the value of property alertDetail. */
	public String getAlertMessage() {
		return mAlertMessage;
	}

	public void setAlertMessage(String msg) {
		mAlertMessage = msg;
	}

	/** Get the value of property alertRendered. */
	public boolean getAlertMessageRendered() {
		return mRenderAlertMessage;
	}

	public void setRenderAlertMessage(boolean value) {
		mRenderAlertMessage = value;
	}

	/** return the value of alert summary     */
	public String getAlertSummary() {
		return mAlertSummary;
	}

	public void setAlertSummary(String summary) {
		mAlertSummary = summary;
	}

	private MethodExpression getNumberValidation() {
		FacesContext facesContext = FacesContext.getCurrentInstance();
		ELContext elcontext = facesContext.getELContext();
		MethodExpression methodExpression = facesContext.getApplication()
				.getExpressionFactory().createMethodExpression(
						elcontext,
						"#{CAPSConfigurationBean.validateNumber}",
						null,
						new Class[] { FacesContext.class, UIComponent.class,
								Object.class });
		return methodExpression;

	}

	private MethodExpression getDateValidation() {
		FacesContext facesContext = FacesContext.getCurrentInstance();
		ELContext elcontext = facesContext.getELContext();
		MethodExpression methodExpression = facesContext.getApplication()
				.getExpressionFactory().createMethodExpression(
						elcontext,
						"#{CAPSConfigurationBean.validateDate}",
						null,
						new Class[] { FacesContext.class, UIComponent.class,
								Object.class });
		return methodExpression;

	}

	/**
	 * This method throws validator exception if specified String is invalid.
	 *
	 * @param context
	 * @param component
	 * @param value
	 * @throws ValidatorException
	 *
	 * @see javax.faces.validator.Validator#validate(javax.faces.context.FacesContext,
	 *      javax.faces.component.UIComponent, java.lang.Object)
	 */
	public void validateDate(FacesContext context, UIComponent component,
			Object value) throws ValidatorException {
		String msgString = null;
		FacesMessage msg = null;

		if (value != null) {
			String stringObject = (String) value;
			Date date = null;
			try {
				date = mDateFormat.parse(stringObject);
			} catch (ParseException e) {
				msgString = value + " "
						+ BundleUtil.getString("caps.configuration.msgNotDate");
				msg = new FacesMessage(msgString);
				msg.setSeverity(FacesMessage.SEVERITY_ERROR);

				// If there are previous "error" messages, then we should append the
				// current message "msg" to the end.  This way all error messages 
				// will be displayed in the alert.
				if (mAlertMessage != null && mAlertMessage.length() > 0) {
					mAlertMessage = mAlertMessage + "<br>" + msgString;
				} else {
					mAlertMessage = msgString;
				}
				mRenderAlertMessage = true;
				displayValidationAlertMessage(mAlertMessage);
				throw new ValidatorException(msg);
			}

		} else {
			msgString = BundleUtil.getString("caps.configuration.msgDateNull");
			msg = new FacesMessage(msgString);
			msg.setSeverity(FacesMessage.SEVERITY_ERROR);
			mAlertMessage = msgString;
			mRenderAlertMessage = true;
			displayValidationAlertMessage(mAlertMessage);
			throw new ValidatorException(msg);
		}
	}

	/**
	 * This method throws validator exception if specified String is invalid.
	 *
	 * @param context
	 * @param component
	 * @param value
	 * @throws ValidatorException
	 *
	 * @see javax.faces.validator.Validator#validate(javax.faces.context.FacesContext,
	 *      javax.faces.component.UIComponent, java.lang.Object)
	 */
	public void validateNumber(FacesContext context, UIComponent component,
			Object value) throws ValidatorException {
		String msgString = null;
		FacesMessage msg = null;

		if (value != null) {

			String stringObject = (String) value;
			Number number = null;
			try {
				number = mNumberFormat.parse(stringObject);
			} catch (ParseException e) {
				msgString = value
						+ " "
						+ BundleUtil
								.getString("caps.configuration.msgNotNumber");
				msg = new FacesMessage(msgString);
				msg.setSeverity(FacesMessage.SEVERITY_ERROR);

				// If there are previous "error" messages, then we should append the
				// current message "msg" to the end.  This way all error messages 
				// will be displayed in the alert.
				if (mAlertMessage != null && mAlertMessage.length() > 0) {
					mAlertMessage = mAlertMessage + "<br>" + msgString;
				} else {
					mAlertMessage = msgString;
				}
				mRenderAlertMessage = true;
				displayValidationAlertMessage(mAlertMessage);
				throw new ValidatorException(msg);
			}

		} else {
			msgString = BundleUtil
					.getString("caps.configuration.msgIntegerNull");
			msg = new FacesMessage(msgString);
			msg.setSeverity(FacesMessage.SEVERITY_ERROR);
			mAlertMessage = msgString;
			mRenderAlertMessage = true;
			displayValidationAlertMessage(mAlertMessage);
			throw new ValidatorException(msg);
		}
		//        
		//        if ((mAlertMessage == null) || ( mRenderAlertMessage == false)) {
		//            resetAlerts();
		//        }

	}


	public void reset() {
		mAlertMessage = null;
		mAlertSummary = null;
		mAlertType = ALERT_TYPE_SUCCESS;
		mRenderAlertMessage = false;
		mSaveButtonDisabled = false;
	}

	//    private void resetAlerts() {
	//      	mAlertMessage = null;
	//    	mAlertSummary = null;
	//    	mAlertType = ALERT_TYPE_SUCCESS;
	//        mRenderAlertMessage = false;
	//        //setAlertType(ALERT_TYPE_ERROR);
	//        //setAlertSummary(Messages.getString("jbi.configuration.alert"));
	//    }

	private void displayAlertMessage(String type, String summary, String msg) {
		setAlertType(type);
		setAlertSummary(summary);
		setRenderAlertMessage(true);
		setAlertMessage(msg);

		mAlert.setId("configure-alert");
		mAlert.setType(getAlertType());
		mAlert.setDetail(getAlertMessage());
		mAlert.setSummary(getAlertSummary());
		mAlert.setRendered(true);
		//displayConfigurationAlert();
	}

	private void displayValidationAlertMessage(String msg) {
		// displayAlertMessage("error",Messages.getString("jbi.configuration.alert.validation"),msg);
		displayAlertMessage(ALERT_TYPE_ERROR, BundleUtil
				.getString("caps.configuration.alert.validation"), msg);
	}

	private ConfigurationBean conBean = null;
	private String mJndiName = null;
	private String mConfigType = null;




	public  synchronized void createPropertySheet(String jndiName, String configType) {

		try {			
			mJndiName = jndiName;
			mConfigType = configType;
			String configuration = null;
			conBean = new ConfigurationBean(jndiName);
			JMSConfigurationHelper helper = new JMSConfigurationHelper();
			Map<String, String> properties = new HashMap<String, String>();
			if (CONNECTOR.equals(configType)) {
				ConnectorConnectionPoolConfig pool = AMXUtil.getDomainConfig()
						.getConnectorConnectionPoolConfigMap().get(jndiName);
				String adapterName = ((ConnectorConnectionPoolConfig) pool).getResourceAdapterName();
	    		if (CapsConstants.JMS_ADAPTER_NAME.equals(adapterName)) {
	    			conBean.setSpecialty(ConfigurationBean.JMSJCA_CONNECTION_POOL);
	    			//encoded = "B64-GZ:H4sIAAAAAAAAAOVX3W/bNhB/nv+Km/2SAI7UIe0wpHKKzFmCBk3Wxg46oOgDI51sphKpkpQd//e7oz4suUm2DsEeNr+IPt79eN88Rm/u8wxWaKzUajL8KXgxBFSxTqRaTIY387ODX4ZvjgdRrFUqF6URjviOBxA5zItMOKQ1RHG64C/02WCQSEtMmyuR42Q4HKjq61n9L1qJTCbCaRN2iEsUCRqIDXqYUzqFpCAnpVKJSfPf4EpWWg+htGj42xrSOcRjJmhjIwvGo6N+6B9+HIXb9VaPsFKkQ7EYe8N6dtlSHdzl9kAkonCsd9cHQ6iMvricwVQrVQNMezyP63oc/fhpenoyP/nUkwCLzlGELKTawI4Cnz+TPV2MPvpDLn/82FmBMXvdwlKvwWm2jo3gpQC2ijy/+ueHFsKQf9htPZ92XHVz/W4I0k6XWsa0k4rMoifoLKtYOsRripi4zYjPmdJTPhrpuhS1g+/h3aYg4mx+/fbqfCcaj7tmvkRWDnQKjpZbX4wZT8YiyzbNJoUp96ccheFSW3dUaOMCgBOw7GCR+U1SF+5UIokJ1ksZLyHR5HmlHSV7Sj7yTieXORmXmTDAUCBUAgz3ulLj6vRtPwVBISaWZW+xOo6rCKTy/L97qyiRJGZJADOdI+C9oOLGKru8YsLgEeRfSbExfVxc1CtrM79aOldQCrZrW/3xALNSwYVYCZhtLHUNuERrxQLhQ4klwt7F5Yf9MVgX1+J+tRWeTy9nTMzty1eHPSq8DF4Fh2NY13rRtxX7iLezYokG4fIDheOwQ3+nFzLmaDEqtZjcCzcmdQ1qhDiy50YmY+/qu1u9Ve/iV/rDDAGccSWy9+qQs9/smNxMRouEqQIs1WzWT5VuEUEmKZ6xUL1IBXClHeEthfPAnHOUKFJR1RSGaieB243faToQq0kc2yzkDGIGS8nv5YWlNLKWRCktmuydZhKVg+tSOZlj8HRJP1nWddGkoszctzuNaInbYvKHVbRvkcIHoaKwbR5/r6nckJe5/J+/nTDylUf+nk7S6FOFdi2zjONe1lHpNNp+ewn+W1F5T3m41iZ5/qhskXtRIf7fVGw2VCpJLfAXgWqA/t+Bqm+K549TC/w9xdPcW3SNfcHNgbedepo0/q4Vbgxa4e5W4XuswtcUtDynZmf5ZoMEqUFSMNfSLWEUAN3slvp4fQLF1xmdtd21bswxpDSfloauyT0MFoG/efd97xXJSqiYABuOAE6xQMVDNanV7dXj9phcbIA8p7upxT2agmDItdTFH23VtQL+Vtz/F7Nu1EwPg5G//tgDgxHpdzE9CW6Uv72mZ5PIzyS+09EteEejwH1A80GwHcLOREz6bI5b4bmmq+sJ0Wr/cXk/WzwhX+0/IM8sAfFTpIK0ogZSSUcj2iTyXqbMCWjiDnhQC1LL2YH3LrjG9Gw2rf48DFcYvZL0nghKQ1gT8GipzHgmDOOjkF9U4a30WWJ3ZOnhUVLtbAhEqlgWrE3ZXh+9HuQtbs56DIbeVpT0bJWdREXb3p4Cohj7BBuMvAdc7B1pdfwFnQ3eE/ycUlGXbvLzC/oxf3cKG4y8zy+FotnPTAajuRHK8tw656ofjKZLQUdnExY8R4WGhuK6NsbVYNWOpXsWqWh1XHIJVzMuD2MJOiEzu99mwZX+42Ti21FLertQ2uD8foecVzPp2oii4JdvtftcXTUK64dj9UgOdx/TTPIv6CjcPqt32f4E4LKWk6oPAAA=";
	    			configuration = JMSUtil.getJMSTemplate();
	    			try {
	        			String connUrl = (String) pool
	        			    .getPropertyValue(CapsConstants.CONNECTIONURL_KEY);
	        			properties.put(CapsConstants.MSURL, connUrl);
	        		} catch (Exception e) {
	        		}
	        		
	                try {
	        			String username = (String) pool
	        			    .getPropertyValue(CapsConstants.USERNAME_KEY);
	        	        properties.put(CapsConstants.MSUsername, username);
	                } catch (Exception e) {
	                }
	                
	                try {
	        			String pwd = (String) pool
	        			    .getPropertyValue(CapsConstants.PASSWORD_KEY);
	        	        properties.put(CapsConstants.MSPassword, pwd);
	                } catch (Exception e) {
	                }
	                
	                try {
	        			String options = (String) pool
	        			.getPropertyValue(CapsConstants.OPTIONS_KEY);
	        			properties.put(CapsConstants.MSOptions, options);
	                } catch (Exception e) {
	                }
	                properties.put(CapsConstants.CONFIGURATION_KEY, configuration);
					helper.populateJMSConfigurationBean(conBean, properties);
	    		} else {
	    			//other rars
	    		}
 
			} 

			if (conBean != null) {
				mId2Propname.clear();
				mId2Secname.clear();

				if (mPropertySheet != null) {
					List origChildren = mPropertySheet.getChildren();
					origChildren.clear();
				}
				mLog.fine(CAPSConfigurationBean.class.getName()
						+ "cratePropertySheet " + "for " + jndiName);
				JMSConfigurationBean jms = conBean.getJMSConfigInstance();
				if (jms != null){
					PropertySheetSection jmsSection = new PropertySheetSection();
                    jmsSection.setId("propSheet");
                    jmsSection.setLabel(jms.getName());
                    Map jmsProps = jms.getProperties();
                    int i = 0;
                    for(Iterator ip = jmsProps.keySet().iterator(); ip.hasNext();)
                    {
                        String pName = (String)ip.next();
                        JMSProperty prop = (JMSProperty)jmsProps.get(pName);
                        Property property1 = new Property();
                        String propId = (new StringBuilder()).append("prop__").append(i).toString();
                        property1.setId(propId);
                        mId2Propname.put(propId, pName);
                        property1.setLabel((new StringBuilder()).append(prop.getDisplayName()).append(":").toString());
                        String propValue = prop.getValue();
                        if(propValue == null)
                            propValue = (String)prop.getDefault();
                        if("Password".equals(pName) || "MSPassword".equals(pName))
                        {
                            PasswordField password = new PasswordField();
                            password.setValue(propValue);
                            password.setId((new StringBuilder()).append("_in_").append(i).toString());
                            property1.getChildren().add(password);
                        } else
                        if("Options".equals(pName) || "MSOptions".equals(pName))
                        {
                            TextArea ta = new TextArea();
                            ta.setId((new StringBuilder()).append("in_").append(i).toString());
                            ta.setValue(propValue);
                            ta.setColumns(100);
                            ta.setRows(10);
                            property1.getChildren().add(ta);
                        } else
                        {
                            TextField textField1 = new TextField();
                            textField1.setId((new StringBuilder()).append("in_").append(i).toString());
                            if(propValue == null)
                                textField1.setValue("");
                            else
                                textField1.setValue(propValue);
                            textField1.setColumns(50);
                            property1.getChildren().add(textField1);
                        }
                        property1.setOverlapLabel(false);
                        property1.setLabelAlign("left");
                        property1.setNoWrap(true);
                        String desc = prop.getDescription();
                        if(desc != null && !desc.trim().equals(""))
                            property1.setHelpText(desc);
                        jmsSection.getChildren().add(property1);
                        i++;
                    }
                    mPropertySheet.getChildren().add(jmsSection);

                }

			
				

			} else {
				mLog.severe("can't decode configuration properties for" + "["
						+ mJndiName + "]");
				mLog.severe("configuration property" + "\n");
				mLog.severe(configuration);
				
				return;
			}
		} catch (Exception e) {
			mLog.log(Level.SEVERE, e.getMessage(), e);
		}

	}

	public void setPropertySheet(PropertySheet propSheet) {
		this.mPropertySheet = propSheet;
	}

	public PropertySheet getPropertySheet() {
		return mPropertySheet;
	}

	public synchronized void save() {

		if (conBean != null) {
			if (conBean.getSpecialty() == ConfigurationBean.JMSJCA_CONNECTION_POOL) {
				//save properties;
				if (mPropertySheet != null) {
					ConnectorConnectionPoolConfig pool = AMXUtil
					    .getDomainConfig()
					    .getConnectorConnectionPoolConfigMap().get(
							mJndiName);
					JMSConfigurationBean configModel = conBean.getJMSConfigInstance();
					List sectionList = mPropertySheet.getChildren();
					for (int i = 0; i < sectionList.size(); i++) {
						PropertySheetSection propSheetSection = (PropertySheetSection) sectionList
								.get(i);
						String secId = propSheetSection.getId();
					
						List propList = propSheetSection.getChildren();
						for (int j = 0; j < propList.size(); j++) {
							Property property = (Property) propList.get(j);
							String propId = property.getId();
							String propName = mId2Propname.get(propId);
							for (Iterator iter2 = property.getChildren()
									.iterator(); iter2.hasNext();) {
								UIInput comp = (UIInput) iter2.next();
								Object value = comp.getValue();
							    JMSProperty prop = configModel.getProperties().get(propName);
								if (value instanceof String) {
									//if (value == null || "".equals(value)) {
									//	value = "<" + propName + ">";
									//}
									if ("".equals(value)) {
										prop.setValue("");
										if (pool.existsProperty(getJCANames(propName))) {
											pool.removeProperty(getJCANames(propName));
										}										
									} else {
										if (CapsConstants.MSOptions
												.equals(propName)) {
												value = JMSConfigurationHelper.packOptions(CapsConstants.JMS_OPTIONS_DELIM, (String)value);
										} else if (CapsConstants.MSPassword.equals(propName)) {
											    value = JMSConfigurationHelper.encodeJMSPassword((String)value);
										}
										pool.setPropertyValue(getJCANames(propName), (String) value);
									
									}

								} 
							}
						}
					}
				}
			} else {
				//other jca

			}

		}
		//TODO check null pool and connection and alert save failure
		setAlertMessage(null);
		displaySavedAlertMessage();
	}


//	private void saveProperty(Property property, ISection secModel) {
//		String propId = property.getId();
//		String propName = mId2Propname.get(propId);
//		for (Iterator iter2 = property.getChildren().iterator(); iter2
//				.hasNext();) {
//			UIInput comp = (UIInput) iter2.next();
//			Object value = comp.getValue();
//
//			IParameter para = secModel.getParameter(propName);
//			if (value instanceof String) {
//				setValues((String) value, para);
//			} else {
//				para.setValue(value);
//			}
//
//		}
//	}
	
    private  String getJCANames(String name) {
    	if (CapsConstants.MSURL.equals(name)) return CapsConstants.CONNECTIONURL_KEY;
    	if (CapsConstants.MSPassword.equals(name)) return CapsConstants.PASSWORD_KEY;
    	if (CapsConstants.MSUsername.equals(name)) return CapsConstants.USERNAME_KEY;
    	if (CapsConstants.MSOptions.equals(name)) return CapsConstants.OPTIONS_KEY;
    	return name;
    }

	

	public void revert() {
		setAlertMessage(null);
		displayAlertMessage(ALERT_TYPE_SUCCESS, BundleUtil
				.getString("caps.configuration.alert.revert.success"), " ");
	}


	private void displaySavedAlertMessage() {
		displayAlertMessage(ALERT_TYPE_SUCCESS, BundleUtil
				.getString("caps.configuration.alert.save.success"), " ");
	}

}
