/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 * 
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common Development
 * and Distribution License("CDDL") (collectively, the "License").  You
 * may not use this file except in compliance with the License. You can obtain
 * a copy of the License at https://glassfish.dev.java.net/public/CDDL+GPL.html
 * or glassfish/bootstrap/legal/LICENSE.txt.  See the License for the specific
 * language governing permissions and limitations under the License.
 * 
 * When distributing the software, include this License Header Notice in each
 * file and include the License file at glassfish/bootstrap/legal/LICENSE.txt.
 * Sun designates this particular file as subject to the "Classpath" exception
 * as provided by Sun in the GPL Version 2 section of the License file that
 * accompanied this code.  If applicable, add the following below the License
 * Header, with the fields enclosed by brackets [] replaced by your own
 * identifying information: "Portions Copyrighted [year]
 * [name of copyright owner]"
 * 
 * Contributor(s):
 * 
 * If you wish your version of this file to be governed by only the CDDL or
 * only the GPL Version 2, indicate your decision by adding "[Contributor]
 * elects to include this software in this distribution under the [CDDL or GPL
 * Version 2] license."  If you don't indicate a single choice of license, a
 * recipient has the option to distribute your version of this file under
 * either the CDDL, the GPL Version 2 or to extend the choice of license to
 * its licensees as provided above.  However, if you add GPL Version 2 code
 * and therefore, elected the GPL Version 2 license, then the option applies
 * only if the new code is made subject to such option by the copyright
 * holder.
 */

package org.glassfish.openesb.addons.configurator.handlers;


import java.util.logging.Level;
import java.util.logging.Logger;

import javax.faces.context.FacesContext;

import org.glassfish.openesb.addons.configurator.beans.CAPSConfigurationBean;
import org.glassfish.openesb.addons.configurator.beans.DataBean;
import com.sun.jsftemplating.annotation.Handler;
import com.sun.jsftemplating.annotation.HandlerInput;
import com.sun.jsftemplating.layout.descriptors.handler.HandlerContext;



/**
 * Provides jsftemplating handlers for List/Show tables
 */
public class DataHandlers {
	private static Logger sLog = Logger.getLogger("org.glassfish.openesb.addons.configurator.handlers");

	private final static String DATABEAN = "DataBean",
			CAPSCONFIGBEAN = "CAPSConfigurationBean",
			DATABEAN_TYPE = "org.glassfish.openesb.addons.configurator.beans.DataBean",
			CAPSCONFIGBEAN_TYPE = "org.glassfish.openesb.addons.configurator.beans.CAPSConfigurationBean",
			INSTANCEBEAN = "InstanceUIBean",
			CONNECTOR = "connectorConnectionPool",
			ENVIRONMENT = "environmentOverride",
			ENVIRONMENT_ENV = "environmentENVOverride",
			ENVIRONMENT_CM = "environmentCMOverride";

	public DataBean getDataBean() {
		sLog.fine("DataHandlers.<getDataBean>()");

		FacesContext facesContext = FacesContext.getCurrentInstance();
		Object existingBean = facesContext.getExternalContext().getSessionMap()
				.get(DATABEAN);

		if (null == existingBean) {
			try {
				Class beanClass = Class.forName(DATABEAN_TYPE);
				existingBean = beanClass.newInstance();
				sLog.fine("DataHandlers.getDataBean() BEAN_NAME=" + DATABEAN
						+ ", className=" + DATABEAN_TYPE + ", bean="
						+ existingBean);
				facesContext.getExternalContext().getSessionMap().put(DATABEAN,
						existingBean);
			} catch (Exception ex) {
				sLog.log(Level.FINE,
						("DataHandlers.<getDataBean>() caught ex=" + ex), ex);
			}
		}
		return (DataBean) existingBean;
	}

	public CAPSConfigurationBean getCAPSConfigurationBean() {

		sLog.fine("DataHandlers.<getCAPSConfigurationBean>()");

		FacesContext facesContext = FacesContext.getCurrentInstance();
		Object existingBean = facesContext.getExternalContext().getSessionMap()
				.get(CAPSCONFIGBEAN);

		if (null == existingBean) {
			try {
				Class beanClass = Class.forName(CAPSCONFIGBEAN_TYPE);
				existingBean = beanClass.newInstance();
				sLog.fine("DataHandlers.getCAPSConfigurationBean() BEAN_NAME="
						+ CAPSCONFIGBEAN + ", className=" + CAPSCONFIGBEAN_TYPE
						+ ", bean=" + existingBean);
				facesContext.getExternalContext().getSessionMap().put(
						CAPSCONFIGBEAN, existingBean);
			} catch (Exception ex) {
				sLog
						.log(
								Level.FINE,
								("DataHandlers.<getCAPSConfigurationBean>() caught ex=" + ex),
								ex);
			}
		}
		return (CAPSConfigurationBean) existingBean;

	}

	/**
	 * <p>
	 * Sets the specified type of table row group data for a table
	 * <p>
	 * Input value: "tableType" -- Type: <code> java.lang.String</code> Valid
	 * types: 'connectionPool,' or 'environmentOverride,'
	 * 
	 * @param handlerCtx
	 *            <code>HandlerContext</code> provides inputs and outputs.
	 */
	@Handler(id = "setConfigTableData", input = { @HandlerInput(name = "tableType", type = String.class, required = true) })
	public void setConfigTableData(HandlerContext context) {
		String tableType = (String) context.getInputValue("tableType");
		sLog.fine("DataHandlers.setConfigTableData(), tableType=" + tableType);

		DataBean dataBean = getDataBean();
		if (CONNECTOR.equals(tableType)) {
			dataBean.setConnectorConnectionPoolTableData();

		} 

	}

	/**
	 * <p>
	 * Sets the specified type of table row group data for a table
	 * <p>
	 * Input value: "tableType" -- Type: <code> java.lang.String</code> Valid
	 * types: 'connectionPool,' or 'environmentOverride,'
	 * 
	 * @param handlerCtx
	 *            <code>HandlerContext</code> provides inputs and outputs.
	 */
	@Handler(id = "setPropertySheet", input = {
                 @HandlerInput(name="jndiName", type=String.class, required=true),           
                 @HandlerInput(name="configType", type=String.class, required=true)})

    public void setPropertySheet(HandlerContext context)
    {
        String jndiName = (String) context.getInputValue("jndiName");
        String configType = (String) context.getInputValue("configType");
        sLog.fine("DataHandlers.setPropertySheet(), name=" + jndiName);
        CAPSConfigurationBean configBean = getCAPSConfigurationBean();
        // configBean.createPropertySheet(getInstanceUIBean(), jndiName);
        configBean.createPropertySheet(jndiName, configType);
      
    }
    
    
    /**
     *  <p> This handler saves the values in Configuration editor to glassfish.</p>
     *  @param  context The HandlerContext.
     */
    @Handler(id="capsSaveConfiguration")
    public void capsSaveConfiguration(HandlerContext handlerCtx)
    {
    	CAPSConfigurationBean configBean = getCAPSConfigurationBean();
        configBean.save();
    }
    
    /**
     *  <p> This handler revert the values in Configuration editor from glassfish.</p>
     *  @param  context The HandlerContext.
     */
    @Handler(id="capsRevertConfiguration")
    public void capsRevertConfiguration(HandlerContext handlerCtx)
    {
    	CAPSConfigurationBean configBean = getCAPSConfigurationBean();
        configBean.revert();
    }
    
}
