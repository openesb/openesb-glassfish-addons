/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-jbi-components.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-jbi-components.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ConfigurationBean.java
 *
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package org.glassfish.openesb.addons.configurator.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


/**
 * 
 * @author Sun Microsystems
 */
public class ConfigurationBean implements Serializable {
	private String mPath, mName;
	private JMSConfigurationBean mInstance;
	public static final byte GENERAL = 0, JMS_REP_ENVIRONMENT = 1, JMSJCA_CONNECTION_POOL = 2;
	private boolean mCompressed = false;
	private byte mSpecialty = GENERAL;

	public ConfigurationBean(String path) {
		mPath = path;
		//TODO
		mName = mPath;
		
		mCompressed = false;
		mSpecialty = GENERAL;
	}

	public byte getSpecialty() {
		return mSpecialty;
	}

	public void setSpecialty(byte value) {
		mSpecialty = value;
	}
	
	public void setCompressed (boolean value) {
		mCompressed = value;
	}
	
	public boolean isCompressed() {
		return mCompressed;
	}

	public String getName() {
		return mName;
	}





	public String getPath() {
		return mPath;
	}

	/*************\
	 * config api *
	\*************/

	private JMSConfigurationBean mJMSConfig;

	public JMSConfigurationBean getJMSConfigInstance() {
		return mJMSConfig;
	}

	public void setJMSConfigInstance(JMSConfigurationBean value) {
		mJMSConfig = value;
	}
}
