/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-jbi-components.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-jbi-components.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)JMSConfigurationHelper.java
 *
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package org.glassfish.openesb.addons.configurator.model;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;

import org.glassfish.openesb.addons.configurator.util.CapsConstants;
import org.glassfish.openesb.addons.configurator.util.JMSUtil;

/**
 * Helper class for creating JMS Configuration bean object.
 * @author Sun Microsystems
 *
 */
public class JMSConfigurationHelper {
	
	/** The path separator string */
	public static final String SEPARATOR = "/";
	/** The path separator character */
	public static final char SEPARATOR_CHAR = '/';
	/** Opening tag for the template within the configuration **/
	private static final String OPENING_TEMPLATE_TAG = "<template>";
	/** Closing tag for the template within the configuration **/
	private static final String CLOSING_TEMPLATE_TAG = "</template>";
	/** Opening tag for the template within the configuration **/
	private static final String OPENING_INSTANCE_TAG = "<instance>";
	/** Closing tag for the template within the configuration **/
	private static final String CLOSING_INSTANCE_TAG = "</instance>";

	private static final String CDATA_OPENING_TAG = "<![CDATA[";
	private static final String ENCONDED_CDATA_OPENING_TAG = "&lt;![CDATA[";
	private static final String CDATA_CLOSING_TAG = "]]>";
	private static final String ENCODED_CDATA_CLOSING_TAG = "]]&gt;";

	private static final String HEADER_ENDING = "?>";

	private static final String GZ_B64_PREPFIX = "B64-GZ:";
    private Logger mLog = Logger.getLogger(JMSConfigurationHelper.class.getName());
    private static final String XMLRSTR = "org/glassfish/openesb/addons/configurator/resources/XMLWriter";
    private static final String LRSTR = "org/glassfish/openesb/addons/configurator/resources/localizable";

    /** Resource bundle object. */
    private ResourceBundle rb;

    /** Localizable resource bundle */
    private ResourceBundle lb;
    
    
	public JMSConfigurationHelper() {
        lb = ResourceBundle.getBundle(LRSTR);
        rb = ResourceBundle.getBundle(XMLRSTR);
	}
	
	public  void populateJMSConfigurationBean(ConfigurationBean root, Map<String, String> properties) {
		root.setJMSConfigInstance(createJMSConfigurationBean(properties));
	}
	public JMSConfigurationBean createJMSConfigurationBean(Map<String, String> properties) {
		String template = JMSUtil.getJMSTemplate();
		JMSConfigurationBean bean = null;
		try {
			bean = getConfiguration(template.getBytes("UTF-8"));
			Map <String, JMSProperty> props = bean.getProperties();
		    for (Iterator it = props.keySet().iterator(); it.hasNext();) {
			    String name = (String) it.next();
			    JMSProperty prop =  props.get(name);
				if (properties.containsKey(prop.getName())) {
					String value = properties.get(prop.getName());
					if (CapsConstants.MSOptions.equals(prop.getName())) {
						value = unpackOptions(value);
					} else if (CapsConstants.MSPassword.equals(prop.getName())) {
						value = decodeJMSPassword(value);
					}					
					prop.setValue(value);
				}

			}
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			mLog.severe(e.getMessage());
		}
		return bean;

		
	}
private static String decodeJMSPassword(String encoded)  {
	if (encoded.startsWith(CapsConstants.ENC08)) {
		try {
			return Base64Coder.decode(encoded.substring(CapsConstants.ENC08.length()), "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
	} 
	return encoded;
}

public static String encodeJMSPassword(String decoded) {
	return CapsConstants.ENC08 + Base64Coder.encode(decoded);
}


private static final String MARK = "JMSJCA.sep=";

 
/**
 * 
 * Packs a multi-line string into a single line string.
 * 
 * 
 * 
 * Example:
 * 
 * original=null
 * 
 * toPack=
 * 
 * a=b
 * 
 * c=d,e
 * 
 * 
 * 
 * results
 * 
 * JMSJCA.sep=,a=b,c=d\,e
 * 
 * 
 * 
 * @param original
 *            String to take the separator from; may be null; "," is default
 * 
 * @param toPack
 *            multiline string to convert into a single line string
 * 
 * @return singleline string
 * 
 */

public static String packOptions(String original, String toPack) {

	// Determine separator

	char sep = original == null || !original.startsWith(MARK)
	|| original.length() <= MARK.length()

	? ',' : original.charAt(MARK.length());

	// Escape separators

	toPack = toPack.replace("" + sep, "\\" + sep);

	// Normalize line endings

	toPack = toPack.replace("\r\n", "\n");

	toPack = toPack.replace('\r', '\n');

	// Fix line endings

	toPack = toPack.replace('\n', sep);

	return MARK + sep + toPack;

}
/**
 * 
 * Unpacks a single-line into a multi-line string. A packed string should start
 * 
 * with mark=s where s is the separator. Separators are replaced with line
 * 
 * endings; separators can be escaped with a backslash.
 * 
 * 
 * 
 * Example:
 * 
 * JMSJCA.sep=,a=b,c=d\,e
 * 
 * becomes
 * 
 * a=b
 * 
 * c=d,e
 * 
 * 
 * 
 * @param toUnpack
 *            Single line string
 * 
 * @return multi line string
 * 
 */

public static String unpackOptions(String toUnpack) {
	if (toUnpack == null || !toUnpack.startsWith(MARK)
			|| toUnpack.length() <= MARK.length()) {

		return toUnpack;

	} else {

		StringBuffer ret = new StringBuffer();

		char sep = toUnpack.charAt(MARK.length());

		for (int i = MARK.length() + 1; i < toUnpack.length(); i++) {

			char c = toUnpack.charAt(i);

			if (c == sep) {

				ret.append("\r\n");

			} else if (c == '\\') {

				// Is next char a delimiter?

				if (i < toUnpack.length() - 1
						&& toUnpack.charAt(i + 1) == sep) {

					ret.append(sep);

					i++;

				} else {

					ret.append(c);

				}

			} else {

				ret.append(c);

			}

		}

		return ret.toString();

	}

}
	 /**
     * Gets a JMSConfigurationBean
     *
     * @param inTemplate template input source.
     * @return a JMSconfiguration object.
     */
    private JMSConfigurationBean getConfiguration(byte[] inTemplate) {
        Element template = null;
        Document doc;
        try {
            DocumentBuilderFactory df = DocumentBuilderFactory.newInstance();

            doc = df.newDocumentBuilder().parse(new ByteArrayInputStream(inTemplate));

            template = doc.getDocumentElement();

        } catch (Exception e) {
            //e.printStackTrace();
            mLog.log(Level.SEVERE, e.getMessage(), e);
        }

        return getTemplateConfiguration(template);


    }
    
    /**
     * XML Configuration creator.
     *
     * @param root XML configuration element
     *
     * @return Configuration object.
     *
     * @throws FormatException on format exception
     */
    public JMSConfigurationBean getTemplateConfiguration(Element root) {
    	JMSConfigurationBean cfg = new JMSConfigurationBean();
        String str = rb.getString("configurationTag");
        NodeList nl = root.getElementsByTagName(str);

        Element configurationTag = (Element) nl.item(0);
        //str = configurationTag.getAttribute(rb.getString("name")); 
        //cfg.setName(str);
        //str = element.getAttribute(rb.getString("displayName"));     
        //nl = element.getElementsByTagName(rb.getString("headerTag"));
        //processTemplateHeader((Element) nl.item(0), cfg);

        nl = configurationTag.getChildNodes();
        String sectionTag = rb.getString("sectionTag");

        for (int i = 0; i < nl.getLength(); i++) {
            if (nl.item(i) instanceof Element) {
                Element temp = (Element) nl.item(i);

                if (temp.getTagName().equals(sectionTag)) {
                    processTemplateSection((Element) nl.item(i), cfg);
                }
            }
        }

        return cfg;
    }
    
   

    /**
     * Process a Template section.
     *
     * @param e Configuratin element.
     * @param c Configuration object.;
     *
     * @throws FormatException on format error.
     */
    private void processTemplateSection(Element e, JMSConfigurationBean cfg) {
        String sectionName = e.getAttribute(rb.getString("name"));

        cfg.setName(sectionName);

        sectionName = e.getAttribute(rb.getString("displayName"));

        if (!(sectionName == null) && !sectionName.equals("")) {
        	cfg.setDisplayName(sectionName);
        }

        cfg.setDescription(getCDataValue(e, rb.getString("description")));
       
        NodeList nl = e.getChildNodes();
        String parameterTag = rb.getString("parameterTag");
        for (int i = 0; i < nl.getLength(); i++) {
            if (nl.item(i) instanceof Element) {
                Element temp = (Element) nl.item(i);
                if (temp.getTagName().equals(parameterTag)) {
                    processTemplateParameter(temp, cfg);
                }
            }
        }
        //no nested section


    }
    
    /**
     * Process a Template parameter.
     *
     * @param e parameter element.
     * @param s parameter container.
     *
     * @throws FormatException on format error.
     */
    private void processTemplateParameter(Element e, JMSConfigurationBean cfg) {
        String name = e.getAttribute(rb.getString("name"));
  
        JMSProperty p = cfg.createProperty(name);      
        cfg.addProperty(p);
        name = e.getAttribute(rb.getString("displayName"));
        if ((name != null) && !name.equals("")) {
            p.setDisplayName(name);
        }

        p.setContextValidator(getCDataValue(e, rb.getString("validator")));
        p.setDescription(getCDataValue(e, rb.getString("description")));
        p.setIsCollection(getBooleanAttribute(e,
                rb.getString("isCollection"), false));

        p.setIsChoice(getBooleanAttribute(e, rb.getString("isChoice"), false));
        p.setIsChoiceEditable(getBooleanAttribute(e, rb.getString("isChoiceEditable"), false));

        p.setIsWritable(getBooleanAttribute(e, rb.getString("isWritable"),
                true));
        p.setIsReadable(getBooleanAttribute(e, rb.getString("isReadable"),
                true));

        p.setIsEnabled(getBooleanAttribute(e, rb.getString("isEnabled"), true));
        //String str = e.getAttribute(rb.getString("isEncrypted"));
        //JMS does not need encryption
        
        NodeList nl = e.getElementsByTagName(rb.getString("default"));

        if (nl.getLength() > 0) {
            nl = ((Element) nl.item(0)).getElementsByTagName(rb.getString(
                        "valueTag"));

            if (nl.getLength() > 0) {
            	 String val = null;
               
                     for (int i = 0; i < nl.getLength(); i++) {
                         Element element = (Element) nl.item(i);
                         NodeList children = element.getChildNodes();

                         for (int j = 0; j < children.getLength(); j++) {
                             Node v = children.item(j);
                             if (v.getNodeValue() == null) {
                                 continue;
                             }
                             val = v.getNodeValue();

                     }
                p.addDefault(val);
                }
             
            }
        }
        //JMS is not choiceable

    }
    
    /**
     * Helper to get a boolean value from an xml document.
     *
     * @param e element.
     * @param aname attribute name.
     * @param def default value.
     *
     * @return a boolean object or null.
     */
    private boolean getBooleanAttribute(Element e, String aname, boolean def) {
        String str = e.getAttribute(aname);

        if ((str == null) || str.equals("")) {
            return def;
        }

        return Boolean.valueOf(str).booleanValue();
    }
    
   
    
    /**
     * Helper function to get cdata.
     *
     * @param e element.
     * @param nodeName node name.
     *
     * @return string.
     */
    private String getCDataValue(Element e, String nodeName) {
        List l = getCData(e, nodeName);

        if (l.size() > 0) {
            return l.get(0) + "";
        }

        return null;
    }
    
    /**
     * Gets context validation code.
     *
     * @param e validator element.
     * @param aname attribute name.
     *
     * @return validation code.
     */
    private List getCData(Element e, String aname) {
        NodeList n = e.getChildNodes();
        ArrayList rlist = new ArrayList();

        for (int i = 0; i < n.getLength(); i++) {
            if (n.item(i) instanceof Element) {
                Element temp = (Element) n.item(i);

                if (aname.equals(temp.getTagName())) {
                    NodeList list = temp.getChildNodes();

                    for (int j = 0; j < list.getLength(); j++) {
                        if (list.item(j) instanceof Text) {
                            rlist.add(((Text) list.item(j)).getData());
                        }
                    }
                }
            }
        }

        return rlist;
    }
    
	/**
	 * Searches for the template in the given configuration string and decodes the content
	 *
	 * @param aConfiguration Configuration value containing the template
	 *
	 * @return Template in the configuration or null if not found
	 **/
	public static String getTemplate(String encoded) {
		String lConfiguration = decode(encoded);
		int lStartIndex = lConfiguration.indexOf(OPENING_TEMPLATE_TAG)
				+ OPENING_TEMPLATE_TAG.length();
		int lEndIndex = lConfiguration.indexOf(CLOSING_TEMPLATE_TAG);
		if (lStartIndex < 0 || lEndIndex < 0 || lStartIndex > lEndIndex) {
			return null;
		} else {
			return lConfiguration.substring(lStartIndex, lEndIndex);
		}
	}
	
	/**
	 * Decodes the given XML got from MBeans. Three steps:
	 * 1. Decode Base64; 2. decompress if zipped; 3. remove extra tags
	 *
	 * @param pXML XML text to be decoded
	 *
	 * @return XML value decoded
	 **/
	public static String decode(String rpXML) {
		// HotFix 105255: the original logging had tremendous impact on performance

		String pXML = decodeGZB64(rpXML);
		int lCount = 0;
		// First decode all opening tags
		while (true) {
			int lIndex = pXML.indexOf(ENCONDED_CDATA_OPENING_TAG);
			if (lIndex < 0) {
				// All found so exit
				break;
			} else {
				lCount++;
				pXML = pXML.substring(0, lIndex)
						+ CDATA_OPENING_TAG
						+ pXML.substring(lIndex
								+ ENCONDED_CDATA_OPENING_TAG.length());
			}
		}
		// Now decode the closing tags
		while (true) {
			int lIndex = pXML.indexOf(ENCODED_CDATA_CLOSING_TAG);
			if (lIndex < 0) {
				// All found so exit
				break;
			} else {
				lCount--;
				pXML = pXML.substring(0, lIndex)
						+ CDATA_CLOSING_TAG
						+ pXML.substring(lIndex
								+ ENCODED_CDATA_CLOSING_TAG.length());
			}
		}
		if (lCount != 0) {
			throw new IllegalArgumentException(
					"Opening and closing tags do not match");
		}
		return pXML;
	}
	
	/**
	 * Decodes the given XML got from MBeans. Two steps:
	 * 1. Decode Base64; 2. decompress if zipped;
	 *
	 * @param pXML XML text to be decoded
	 *
	 * @return XML value decoded
	 **/
	public static String decodeGZB64(String rpXML) {
		// HotFix 105255: the original logging had tremendous impact on performance

		String pXML = null;
		if (rpXML.startsWith("B64-GZ")) {
			try {
				String stage1 = rpXML.substring(GZ_B64_PREPFIX.length());
				byte[] stage2 = Base64Coder.decode(stage1.getBytes("UTF-8"));
				byte[] stage3 = decompress(stage2);
				pXML = new String(stage3, "UTF-8");
			} catch (Exception e) {

			}

		} else if (rpXML.startsWith("BASE64")) {
			//detect compress format here
			String cfgOnly = rpXML.substring("BASE64".length());
			try {
				pXML = Base64Coder.decode(cfgOnly, "UTF-8");
			} catch (Exception e) {

			}
		} else {
			pXML = rpXML;
		}
		return pXML;
	}
	
	public static byte[] compress(byte[] data) throws IOException {

		ByteArrayInputStream bin = new ByteArrayInputStream(data);
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		GZIPOutputStream zos = new GZIPOutputStream(bos);

		byte[] buff = new byte[1024];
		int count = 0;
		while ((count = bin.read(buff, 0, 1024)) != -1) {
			zos.write(buff, 0, count);
		}

		try {
			zos.close();
			bin.close();
		} catch (Exception e) {
		}

		return bos.toByteArray();

	}

	public static byte[] decompress(byte[] compdata) throws IOException {

		ByteArrayInputStream bin = new ByteArrayInputStream(compdata);
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		GZIPInputStream gzis = new GZIPInputStream(bin);

		byte[] buff = new byte[1024];
		int count = 0;
		while ((count = gzis.read(buff, 0, 1024)) != -1) {
			bos.write(buff, 0, count);
		}

		try {
			gzis.close();
			bos.close();
		} catch (Exception e) {
		}

		return bos.toByteArray();
	}

    
}


