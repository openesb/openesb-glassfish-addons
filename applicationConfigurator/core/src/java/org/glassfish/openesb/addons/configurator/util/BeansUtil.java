/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-jbi-components.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-jbi-components.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)BeansUtil.java
 *
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */
package org.glassfish.openesb.addons.configurator.util;

import java.util.logging.Level;

import javax.faces.context.FacesContext;

import org.glassfish.openesb.addons.configurator.beans.DataBean;

public class BeansUtil {
	
	
	/**
	 * This method init a data bean and put the bean in Session context with 
	 * <code>beanKey</code>
	 */
	public static Object getBean (String beanKey, String className ) {
		
		
		    	//sLog.fine("DataHandlers.<getDataBean>()");

		    	FacesContext facesContext = FacesContext.getCurrentInstance();
		    	Object existingBean =
		    		facesContext.getExternalContext().getSessionMap().get(beanKey);

		    	if (null == existingBean)
		    	{
		    		try
		    		{
		    			Class beanClass = Class.forName(className);
		    			existingBean = beanClass.newInstance();
		    			
		    			facesContext.getExternalContext().getSessionMap().put(beanKey, existingBean);
		    		}
		    		catch (Exception ex)
		    		{
//		    			sLog.log(Level.FINE,
//		    					("DataHandlers.<getDataBean>() caught ex=" + ex),
//		    					ex);
		    		}
		    	}
		    	return existingBean;
		    }
		

}
