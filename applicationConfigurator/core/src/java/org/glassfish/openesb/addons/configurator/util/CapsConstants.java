/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-jbi-components.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-jbi-components.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)BundleUtil.java
 *
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */
package org.glassfish.openesb.addons.configurator.util;

public class CapsConstants {
	
	//no i18n
	public static final String CAPS_ROOT_TREE_NODE_TEXT="ESB";
	public static final String CAPS_CONNECTOR_CONNECTION_POOL_TREE_NODE_TEXT=
		"Connector Connection Pools";

	public static final String CAPS_FOLDER_IMG="/resource/images/folder.gif";
	public static final String CAPS_CONN_IMG="/resource/images/caps/connectionPool.gif";
	
	public static final String CONFIGURATION_KEY = "Configuration";
	//properties key words in JMS JCA 

	public static final String PASSWORD_KEY = "Password";
	public static final String USERNAME_KEY = "UserName";
	public static final String CONNECTIONURL_KEY = "ConnectionURL";
	public static final String OPTIONS_KEY = "Options";
	
	public static final String MSURL = "MSURL";
	public static final String MSUsername = "MSUsername";
	public static final String MSPassword = "MSPassword";
	public static final String MSOptions = "MSOptions";
	
    public static final String ENC07 = "__:ENC:07:";
    public static final String ENC08 = "__:ENC:08:";
    
    //constant for JMS Connector connection
    public static final String JMS_ADAPTER_NAME = "sun-jms-adapter";
    public static final String JMS_OPTIONS_PREFIX = "JMSJCA.sep=";
    public static final String JMS_OPTIONS_DELIM = ",";
    	

}
