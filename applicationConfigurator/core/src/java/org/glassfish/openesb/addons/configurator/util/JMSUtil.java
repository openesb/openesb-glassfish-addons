/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-jbi-components.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-jbi-components.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)JMSUtil.java
 *
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */
package org.glassfish.openesb.addons.configurator.util;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;



public class JMSUtil {
	
	private static String mJMSTemplate;
	private static final int LENTH = 1024;
    private static String JMS_TEMPLAT_PATH = "org/glassfish/openesb/addons/jmsjca/UnifiedServerConfigTemplate.xml";
	
    static {
		InputStream templat = JMSUtil.class.getClassLoader().getResourceAsStream(JMS_TEMPLAT_PATH);
		InputStreamReader in  = new InputStreamReader(templat);
        StringBuffer sb = new StringBuffer();
        int count;
        char[] buf = new char[LENTH];
        try {
			while ((count = in.read(buf)) > 0) {
			    sb.append(buf, 0, count);
			}
		} catch (IOException e1) {

		}
        try {
			in.close();
		} catch (IOException e) {

		}
        mJMSTemplate =  sb.toString();
    }

	public static String getJMSTemplate() {
	        return mJMSTemplate;
	
	}

}
