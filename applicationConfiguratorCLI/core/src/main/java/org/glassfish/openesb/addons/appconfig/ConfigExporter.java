/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-jbi-components.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-jbi-components.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ConfigExporter.java
 *
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */
package org.glassfish.openesb.addons.appconfig;

import com.sun.enterprise.cli.framework.CLILogger;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import javax.management.*;
import org.w3c.dom.*;
import java.util.*;
import java.util.Map.Entry;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;

/**
 *
 * @author Sun Microsystems
 */
public class ConfigExporter {

    private ConfigExtracter ce;
    private JNDIHandler jh;
      
    private static final String CONNECTIONURL = Constants.JMS_CONNECTIONURL;
    private static final String USERNAME = Constants.JMS_USERNAME;
    private static final String PASSWORD = Constants.JMS_PASSWORD;
    private static final String OPTIONS = Constants.JMS_OPTIONS;
    
    private static Set<String> JMS_PROPS = new HashSet<String>();
    
    static{
        JMS_PROPS.add(CONNECTIONURL);
        JMS_PROPS.add(USERNAME);
        JMS_PROPS.add(PASSWORD);
        JMS_PROPS.add(OPTIONS);
    }
    
    public ConfigExporter(String configDir, MBeanServerConnection mbsc) {
        ce = new ConfigExtracter(configDir);
        jh = new JNDIHandler(mbsc);
    }

    public void exportJCAConfig(String jcaName, boolean overwrite) throws Exception {
                
        String extSysId = Constants.JCA_PREFIX + jcaName;
        
        ObjectName jcaPool = jh.getJCAPoolObjectName(jcaName);
        String raName = jh.getJCAPoolRAName(jcaPool);
        
        if( Constants.SUN_JMS_ADAPTER_NAME.equals(raName) ){
            doJMSJCAExport(extSysId, jcaPool, overwrite);
        } else {
            String config = jh.getJCAPoolConfiguration(jcaName);
            ce.extract(extSysId, config, overwrite);
        }

    }

    public void exportLDAPConfig(boolean overwrite) throws Exception {

        String ldapconfig = null;
        try {
            ldapconfig = jh.getCRConfiguration(Constants.LDAP);
        } catch (JMException e) {
        }

        if (ldapconfig == null) {
            //no CAPS Override object in the server. Hand create the configuration
            InputStream is = this.getClass().getResourceAsStream("ldap/caps-ldap-config.properties");
            //generate properties
            Properties ldapProps = new Properties();
            ldapProps.load(is);
            ldapconfig = createConfiguration(ldapProps);
        }

        ce.extract(Constants.LDAP, ldapconfig, overwrite);
    }
  
    private void doJMSJCAExport(String extSysId, ObjectName jcaPool, boolean overwrite) throws Exception{
        
            InputStream is = this.getClass().getResourceAsStream("jms/JMS_RA_PROPERTIES.xml");
            Document modDesc = DOMUtil.parse(is);
            
            // Unified JMS JCA template
            InputStream istempl =
                    this.getClass().getResourceAsStream("jms/UnifiedServerConfigTemplate.xml");

            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            byte[] buf = new byte[1024];
            int count = 0;
            while ((count = istempl.read(buf)) != -1) {
                bos.write(buf, 0, count);
            }
            bos.close();
            byte[] bytes = bos.toByteArray();
            String configTemplate = new String(bytes, "UTF-8");

            ESConfig esc = new ESConfig(configTemplate);
            configTemplate = fixJMSJCATemplateParamNames(esc);
 
            XPath xp = XPathFactory.newInstance().newXPath();
            
            Set<String> setPropNames = JMS_PROPS;
            
            AttributeList al = jh.getProperties(jcaPool);
            for (Object object : al) {
                Attribute attr = (Attribute) object;
                if( setPropNames.contains(attr.getName()) ) {
                    // Some connector pool properties could have been deleted
                    // using web admin console. 
                    // Extract only the properties that exist.

                    String propName = attr.getName();
                    String propXP = Constants.RAR_PROP_VALUE_XP.replace("?", propName);
                    Node valNode = (Node) xp.evaluate(propXP, modDesc, XPathConstants.NODE);
//                    String value = jh.getPropertyValue(jcaPool, propName);
                    String value = (String) attr.getValue();
                    
                    if(OPTIONS.equals(propName)){
                        if(!value.startsWith(Constants.JMSJCA_MARK)){
                            value = Constants.JMSJCA_MARK + "," + value;
                        }
                        value = ConfigUtil.unpackOptions(value);
                    }
                    
                    if(PASSWORD.equals(propName)){
                        if(!value.startsWith(Constants.ENC08)){
                              value = Base64.encode(value, Constants.UTF_8);
                              value = Constants.ENC08 + value;
                        }
                    }
                    
                    value = Constants.CDATA_PREFIX + value + Constants.CDATA_SUFFIX;
                    valNode.setTextContent(value);
                }
            }
            
            // Delegate to the extracter to create property files.
            ce.extract(extSysId, configTemplate, modDesc, overwrite);
        
    }
    
    /*
     * The shared JMS JCA template (obtained from eGateLib at compile time) 
     * does not have correct param names.
     * Fix the param names and return String of the updated template.
     * 
     */
    private String fixJMSJCATemplateParamNames(ESConfig esc) throws Exception{
       
        XPath xp = XPathFactory.newInstance().newXPath();

        Map<String, Node> paramNodes = esc.getTemplateParamNodes();
        Set<String> paramKeys = paramNodes.keySet();
//        print("Tempate params: " + paramKeys);
        for (String paramKey : paramKeys) {
            
            Node paramNode = paramNodes.get(paramKey);
            
            String propName = paramKey.substring(
                    paramKey.lastIndexOf(Constants.KEY_SEPARATOR) + 1);
//            print("prop name: " + propName);
            if("MSURL".equals(propName)){
                Element e = (Element) paramNode;
                e.setAttribute("name", CONNECTIONURL);
            } else if("MSUsername".equals(propName)){
                Element e = (Element) paramNode;
                e.setAttribute("name", USERNAME);
            } else if("MSPassword".equals(propName)){
                Element e = (Element) paramNode;
                e.setAttribute("name", PASSWORD);
            } else if("MSOptions".equals(propName)){
                Element e = (Element) paramNode;
                e.setAttribute("name", OPTIONS);
            }
        }
        
        Node template = esc.getTemplate();
        return DOMUtil.writeToString(template);
    }

    private String createConfiguration(Properties props) throws Exception {

        Document doc = DOMUtil.createDocument();
        Element root = doc.createElement("configuration");
        doc.appendChild(root);

        Element template = doc.createElement("template");
        root.appendChild(template);

        Element tcfg = doc.createElement("cfg");
        template.appendChild(tcfg);

        Element tconfiguration = doc.createElement("configuration");
        tcfg.appendChild(tconfiguration);

        Element theader = doc.createElement("header");
        tconfiguration.appendChild(theader);

        Element tsection = doc.createElement("section");
        tsection.setAttribute("name", "parameter-settings");
        tconfiguration.appendChild(tsection);

        Element param = null;
        Element val = null;
        Element desc = null;
        // add params to template
        for (Entry<Object, Object> entry : props.entrySet()) {
            String key = (String) entry.getKey();
            //String value = (String) entry.getValue();

            param = doc.createElement("parameter");
            param.setAttribute("name", key);
            param.setAttribute("displayName", key);
            param.setAttribute("type", "STRING");
            if ("password".equals(key)) {
                param.setAttribute("isEncrypted", "true");
            } else {
                param.setAttribute("isEncrypted", "false");
            }

            desc = doc.createElement("description");
            CDATASection cdata = doc.createCDATASection(key);
            desc.appendChild(cdata);
            param.appendChild(desc);

            tsection.appendChild(param);
        }

        Element instance = doc.createElement("instance");
        root.appendChild(instance);

        Element icfg = doc.createElement("cfg");
        instance.appendChild(icfg);

        Element iconfiguration = doc.createElement("configuration");
        icfg.appendChild(iconfiguration);

        Element iheader = doc.createElement("header");
        iconfiguration.appendChild(iheader);

        Element isection = doc.createElement("section");
        isection.setAttribute("name", "parameter-settings");
        iconfiguration.appendChild(isection);

        // add params to instance
        for (Entry<Object, Object> entry : props.entrySet()) {
            String key = (String) entry.getKey();
            String value = (String) entry.getValue();

            param = doc.createElement("parameter");
            param.setAttribute("name", key);
            val = doc.createElement("value");
            CDATASection cdata = doc.createCDATASection(value);
            val.appendChild(cdata);
            param.appendChild(val);

            isection.appendChild(param);
        }

        //for testing only - dump to external file
        //String filepath = "tmp" + File.separator + "configuration.xml";
        //ReConfigUtil.writeToFile(doc, filepath);

        String xml = ConfigUtil.DOMToString(doc);
        String b64 = ConfigUtil.encodeB64(xml);

        return b64;
    }
    
    private static void print(String msg) {
        CLILogger.getInstance().printMessage(msg);
    }
}
