/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-jbi-components.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-jbi-components.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ConfigUtil.java
 *
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */
package org.glassfish.openesb.addons.appconfig;

import com.sun.enterprise.cli.framework.CLILogger;
import java.io.*;
import java.util.*;
import java.util.zip.*;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.*;
import org.w3c.dom.*;

/**
 *
 * @author Sun Microsystems
 */
public class ConfigUtil {

    public static boolean validModule(String modName, String modType, Document modDesc) throws Exception {
  
        if (modDesc == null) {
            return false;
        }
  
        // ignore StartUpConnector.rar
        if (Constants.MODULE_RAR.equals(modType) && modName.startsWith("StartUpConnector")) {
            return false;
        }
  
        if( getDescriptorConfig(modType, modDesc) == null ){
            return false;
        }
        
        return true;
    }
    
    public static String getDescriptorConfig(String modType, Document modDesc) throws Exception {
        
        XPath xp = XPathFactory.newInstance().newXPath();

        Node nodeConfig = null;
        if (Constants.MODULE_JAR.equals(modType)) {
            nodeConfig = (Node) xp.evaluate(Constants.JAR_CONFIG_XP, modDesc, XPathConstants.NODE);
        } else if (Constants.MODULE_RAR.equals(modType)) {
            nodeConfig = (Node) xp.evaluate(Constants.RAR_CONFIG_XP, modDesc, XPathConstants.NODE);
            if (nodeConfig == null) {
                nodeConfig = (Node) xp.evaluate(Constants.JMS_CONFIG_TEMPLATE_XP, modDesc, XPathConstants.NODE);
            }
        }

        if (nodeConfig == null) {
            //print("no config");
            return null;
        }

        return nodeConfig.getTextContent();
    }

    public static String decode(String encoded) throws Exception {
        if (encoded.startsWith(Constants.B64_PREFIX)) {
            return decodeAndCleanB64(encoded);
        } else if (encoded.startsWith(Constants.B64_GZ_PREFIX)) {
            return decodeAndCleanB64GZ(encoded);
        } else {
            return encoded;
        }
    }

    public static String decodeAndCleanB64GZ(String encoded) throws Exception {
//        encoded = encoded.substring(Constants.B64_GZ_PREFIX.length());
//        byte[] gzipd = Base64.decode(encoded.getBytes(Constants.UTF_8));
//        // UnZip
//        byte[] unzipd = ConfigUtil.decompress(gzipd);
//
//        String decoded = new String(unzipd, Constants.UTF_8);
//
//        if (decoded.startsWith(Constants.CDATA_PREFIX)) {
//            decoded = decoded.substring(Constants.CDATA_PREFIX.length(), decoded.length() - Constants.CDATA_SUFFIX.length());
//        }

        String decoded = decodeB64GZ(encoded);
        return cleanCDATATags(decoded);
    }

    public static String decodeB64GZ(String encoded) throws Exception {
    
        encoded = encoded.substring(Constants.B64_GZ_PREFIX.length());
        byte[] gzipd = Base64.decode(encoded.getBytes(Constants.UTF_8));
        // UnZip
        byte[] unzipd = ConfigUtil.decompress(gzipd);

        String decoded = new String(unzipd, Constants.UTF_8);

        if (decoded.startsWith(Constants.CDATA_PREFIX)) {
            decoded = decoded.substring(Constants.CDATA_PREFIX.length(), decoded.length() - Constants.CDATA_SUFFIX.length());
        }

        return decoded;
    }
    
    public static String decodeAndCleanB64(String encoded) throws Exception {
//        encoded = encoded.substring(Constants.B64_PREFIX.length());
//        String decoded = Base64.decode(encoded, Constants.UTF_8);
//        if (decoded.startsWith(Constants.CDATA_PREFIX)) {
//            decoded = decoded.substring(Constants.CDATA_PREFIX.length(), decoded.length() - Constants.CDATA_SUFFIX.length());
//        }

        String decoded = decodeB64(encoded);
        return cleanCDATATags(decoded);
    }

        
    public static String decodeB64(String encoded) throws Exception {
        encoded = encoded.substring(Constants.B64_PREFIX.length());
        String decoded = Base64.decode(encoded, Constants.UTF_8);
        if (decoded.startsWith(Constants.CDATA_PREFIX)) {
            decoded = decoded.substring(Constants.CDATA_PREFIX.length(), decoded.length() - Constants.CDATA_SUFFIX.length());
        }

        return decoded;
    }
    
    public static String encodeB64GZ(String str) throws Exception {
        byte[] bytes = str.getBytes(Constants.UTF_8);
//        byte[] compbytes = ConfigUtil.compress(bytes);
//        String encoded = new String(Base64.encode(compbytes), Constants.UTF_8);
//        return Constants.B64_GZ_PREFIX + encoded;
        return encodeB64GZ(bytes);
    }

    
    public static String encodeB64GZ(byte[] bytes) throws Exception {
        byte[] compbytes = ConfigUtil.compress(bytes);
        String encoded = new String(Base64.encode(compbytes), Constants.UTF_8);
        return Constants.B64_GZ_PREFIX + encoded;
    }
    
    public static String encodeB64(String str) throws Exception {
        //BASE64Encoder encoder = new BASE64Encoder();
        //return Constants.B64_PREFIX + encoder.encodeBuffer(Constants.CDATA_PREFIX + str + Constants.CDATA_SUFFIX);
        String cdataStr = Constants.CDATA_PREFIX + str + Constants.CDATA_SUFFIX;
        String encoded = Base64.encode(cdataStr, Constants.UTF_8);
        return Constants.B64_PREFIX + encoded;
    }

    public static String cleanCDATATags(String xml) {
        xml = (xml.replaceAll(
                Constants.CDATA_START_ENTITY_REF,
                Constants.CDATA_PREFIX)).replaceAll(
                Constants.CDATA_END_ENTITY_REF,
                Constants.CDATA_SUFFIX);
        return xml;
    }

    public static String DOMToString(Document doc) {
        String str = null;
        try {
            StringWriter output = new StringWriter();
            TransformerFactory.newInstance().newTransformer().transform(
                    new DOMSource(doc), new StreamResult(output));
            // print(output.toString());
            str = output.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return str;
    }

    public static boolean isEditable(NamedNodeMap tAttribs) {
        Node isEnabledAttrib = tAttribs.getNamedItem("isEnabled");
        boolean isEnabled = true;
        if (isEnabledAttrib != null) {
            isEnabled = Boolean.parseBoolean(isEnabledAttrib.getTextContent());
        }

        Node isReadableAttrib = tAttribs.getNamedItem("isReadable");
        boolean isReadable = true;
        if (isReadableAttrib != null) {
            isReadable = Boolean.parseBoolean(isReadableAttrib.getTextContent());
        }

        if (!isEnabled || !isReadable) {
            return false;
        }

        return true;
    }

    public static boolean checkNameConflicts(Map<String, Node> params) {

        Map<String, String> names = new HashMap<String, String>();
        Set<String> keys = params.keySet();
        for (Iterator<String> it = keys.iterator(); it.hasNext();) {
            String key = it.next();
            String name = key.substring(key.lastIndexOf(ESConfig.KEY_SEPARATOR) + 1);
            if (names.containsKey(name)) {
                return true;
            } else {
                names.put(name, name);
            }
        }

        return false;
    }

    public static byte[] compress(byte[] data) throws IOException {

        ByteArrayInputStream bin = new ByteArrayInputStream(data);
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        GZIPOutputStream zos = new GZIPOutputStream(bos);

        byte[] buff = new byte[1024];
        int count = 0;
        while ((count = bin.read(buff, 0, 1024)) != -1) {
            zos.write(buff, 0, count);
        }

        try {
            zos.close();
            bin.close();
        } catch (Exception e) {
        }

        return bos.toByteArray();

    }

    public static byte[] decompress(byte[] compdata) throws IOException {

        ByteArrayInputStream bin = new ByteArrayInputStream(compdata);
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        GZIPInputStream gzis = new GZIPInputStream(bin);

        byte[] buff = new byte[1024];
        int count = 0;
        while ((count = gzis.read(buff, 0, 1024)) != -1) {
            bos.write(buff, 0, count);
        }

        try {
            gzis.close();
            bos.close();
        } catch (Exception e) {
        }

        return bos.toByteArray();
    }

    public static String getEnvId(Document descDoc, Document projectInfoDoc,
            String modType) throws Exception {
        String envId = null;
        String configjndiName = getConfigJndiName(descDoc, modType);
        if (configjndiName != null) {
            envId = configjndiName;
        } else {
            if (projectInfoDoc != null) {
                envId = getProjectInfoEnvId(projectInfoDoc);
            }
        }
        return envId;
    }

    public static String getCMId(Document projectInfoDoc) throws Exception {
        String cmId = null;
        if (projectInfoDoc != null) {
            cmId = getProjectInfoCMId(projectInfoDoc);
        }

        return cmId;
    }

    public static String getConfigJndiName(Document modDesc, String modType)
            throws Exception {
        String configjndiName = null;
        configjndiName = DOMUtil.getPropertyValue("ConfigurationJNDIName", modDesc, modType);
        if (configjndiName != null) {
            String capsEnvPrefix = Constants.CAPSENV_ROOT + Constants.JNDI_PATH_SEPARATOR;
            if (configjndiName.startsWith(capsEnvPrefix)) {
                configjndiName = configjndiName.substring(capsEnvPrefix.length());
            }
        }

        return configjndiName;
    }
    
    private static String getProjectInfoEnvId(Document projInfoDoc)
            throws Exception {
        XPath xpath = XPathFactory.newInstance().newXPath();
        String env = "";
        String logicalHost = "";
        String externalName = "";
        String externalMode = "";

        try {
            String envParamXP = "//instance/cfg/configuration/section/parameter[@name='EnvironmentName']/value";
            Node nodeEnvParam = (Node) xpath.evaluate(envParamXP, projInfoDoc,
                    XPathConstants.NODE);
            if (nodeEnvParam == null) {
                throw new Exception("PINFE_NoEnv");
            }
            env = nodeEnvParam.getTextContent();
            // print("Got env: " + env);

            String logicalHostXP = "//instance/cfg/configuration/section/parameter[@name='LogicalHostName']/value";
            Node nodeHostParam = (Node) xpath.evaluate(logicalHostXP,
                    projInfoDoc, XPathConstants.NODE);
            if (nodeHostParam == null) {
                throw new Exception("PINFE_NoLH");
            }
            logicalHost = nodeHostParam.getTextContent();
            // print("Got LogicalHost: " + logicalHost);

            String externalXP = "//instance/cfg/configuration/section/parameter[@name='ExternalSystem']/value";
            Node nodeESParam = (Node) xpath.evaluate(externalXP, projInfoDoc,
                    XPathConstants.NODE);
            if (nodeESParam != null) {
                externalName = nodeESParam.getTextContent();
            }

            if (externalName.equals("")) {
                // if ProjectInfo does not have ExternalSystem element,
                // use ComponentName. This would be the case in case of
                // BPEL (eInsight) ProjectInfo.
                String componentXP = "//instance/cfg/configuration/section/parameter[@name='ComponentName']/value";
                Node nodeCompParam = (Node) xpath.evaluate(componentXP, projInfoDoc,
                        XPathConstants.NODE);
                if (nodeCompParam == null) {
                    throw new Exception("PINFE_NoESCM");
                }

                externalName = nodeCompParam.getTextContent();
            }

            String externalModeXP = "//instance/cfg/configuration/section/parameter[@name='ExternalSystemMode']/value";
            Node nodeModeParam = (Node) xpath.evaluate(externalModeXP, projInfoDoc,
                    XPathConstants.NODE);
            if (nodeModeParam != null) {
                externalMode = nodeModeParam.getTextContent();
            }

        // print("Got Component: " + component);
        } catch (XPathExpressionException xpe) {
//             xpe.printStackTrace();
            throw new Exception("PINFE: " + xpe.getMessage(), xpe.getCause());
        }

        if (!"".equals(externalMode)) {
            return Constants.ENV + Constants.ID_SEPARATOR +
                    env + Constants.ID_SEPARATOR + logicalHost +
                    Constants.ID_SEPARATOR + externalName + "_" + externalMode;
        } else {
            return Constants.ENV + Constants.ID_SEPARATOR +
                    env + Constants.ID_SEPARATOR + logicalHost +
                    Constants.ID_SEPARATOR + externalName;
        }

    }

    private static String getProjectInfoCMId(Document projInfoDoc)
            throws Exception {
        //<ProjectName>/<CollabName>/<ComponentName>
        XPath xpath = XPathFactory.newInstance().newXPath();
        String projectName = "";
        String CollabName = "";
        String componentName = "";
        try {
            String projParamXP = "//instance/cfg/configuration/section/parameter[@name='ProjectName']/value";
            Node nodeProjParam = (Node) xpath.evaluate(projParamXP, projInfoDoc,
                    XPathConstants.NODE);
            if (nodeProjParam == null) {
                throw new Exception("PINFE_NoPN");
            }
            projectName = nodeProjParam.getTextContent();
            // print("Got env: " + env);

            String collabXP = "//instance/cfg/configuration/section/parameter[@name='CollabName']/value";
            Node nodeCollabParam = (Node) xpath.evaluate(collabXP,
                    projInfoDoc, XPathConstants.NODE);
            if (nodeCollabParam == null) {
                throw new Exception("PINFE_NoCN");
            }
            CollabName = nodeCollabParam.getTextContent();
            // print("Got LogicalHost: " + logicalHost);

            String componentXP = "//instance/cfg/configuration/section/parameter[@name='ComponentName']/value";
            Node nodeCompParam = (Node) xpath.evaluate(componentXP, projInfoDoc,
                    XPathConstants.NODE);
            if (nodeCompParam == null) {
                throw new Exception("PINFE_NoESCM");
            }

            componentName = nodeCompParam.getTextContent();

        // print("Got Component: " + component);
        } catch (XPathExpressionException xpe) {
//             xpe.printStackTrace();
            throw new Exception("PINFE: " + xpe.getMessage(), xpe.getCause());
        }
        return Constants.CM + Constants.ID_SEPARATOR +
                projectName + Constants.ID_SEPARATOR +
                CollabName + Constants.ID_SEPARATOR + componentName;
    }

    public static String getCMapParamsFromProjectInfo(Document projInfoDoc)
            throws Exception {
        XPath xpath = XPathFactory.newInstance().newXPath();

        String xp = "//instance/cfg/configuration/section/parameter[@name='CMapParams']/value";
        Node cmapParams = (Node) xpath.evaluate(xp, projInfoDoc,
                XPathConstants.NODE);
        if (cmapParams == null) {
            return null;
        }

        return cmapParams.getTextContent();

    }

//    public static String idToPath(String extSysId, String separator) {
//        StringBuffer sb = new StringBuffer();
//        StringTokenizer st = new StringTokenizer(extSysId, ";", false);
//        sb.append(st.nextToken());
//        while (st.hasMoreTokens()) {
//            //sb.append(File.separator + st.nextToken());
//            sb.append(separator + st.nextToken());
//        }
//        return sb.toString();
//    }

    public static String getModuleType(String modName) {
        String modType = null;
        if (modName.endsWith(".jar")) {
            modType = Constants.MODULE_JAR;
        } else if (modName.endsWith(".rar")) {
            modType = Constants.MODULE_RAR;
        }
        return modType;
    }

    public static String getModuleDescPath(String modName, String modExtractDir) {
        String descPath = "";
        if (modName.endsWith(".jar")) {
            descPath = modExtractDir + "/META-INF/ejb-jar.xml";
        } else if (modName.endsWith(".rar")) {
            descPath = modExtractDir + "/META-INF/ra.xml";
        }

        return descPath;
    }
    
    //private static final String MARK = "JMSJCA.sep=";
    private static final String MARK = Constants.JMSJCA_MARK;
    
    /**
     * Packs a multi-line string into a single line string.
     *
     * Example:
     *   original=null
     *   toPack=
     *     a=b
     *     c=d,e
     *    
     *   results
     *     JMSJCA.sep=,a=b,c=d\,e
     *
     * @param original String to take the separator from; may be null; "," is default
     * @param toPack multiline string to convert into a single line string
     * @return singleline string
     */
    public static String packOptions(String original, String toPack) {
        // Determine separator
        char sep = original == null || !original.startsWith(MARK) 
                || original.length() <= MARK.length() ? ',' : original.charAt(MARK.length());

        // Escape separators
        toPack = toPack.replace("" + sep, "\\" + sep);

        // Normalize line endings
        toPack = toPack.replace("\r\n", "\n");
        toPack = toPack.replace('\r', '\n');

        // Fix line ending
        toPack = toPack.replace('\n', sep);
        return MARK + sep + toPack;
    }


    /**
     * Unpacks a single-line into a multi-line string. A packed string should start
     * with mark=s where s is the separator. Separators are replaced with line
     * endings; separators can be escaped with a backslash.
     *
     * Example:
     *    JMSJCA.sep=,a=b,c=d\,e
     * become
     *    a=b
     *    c=d,e
     *
     * @param toUnpack Single line string
     * @return multi line string
     */
    public static String unpackOptions(String toUnpack) {

        if (toUnpack == null || !toUnpack.startsWith(MARK) 
                || toUnpack.length() <= MARK.length()) {
            return toUnpack;
        } else {
            StringBuffer ret = new StringBuffer();
            char sep = toUnpack.charAt(MARK.length());
            for (int i = MARK.length() + 1; i < toUnpack.length(); i++) {
                char c = toUnpack.charAt(i);
                if (c == sep) {
                    ret.append("\r\n");
                } else if (c == '\\') {
                    // Is next char a delimiter?
                    if (i < toUnpack.length() - 1 && toUnpack.charAt(i + 1) == sep) {
                        ret.append(sep);
                        i++;
                    } else {
                        ret.append(c);

                    }
                } else {
                    ret.append(c);
                }
            }
            return ret.toString();
        }
    }

    private static void print(String msg) {
        CLILogger.getInstance().printMessage(msg);
    }
}
