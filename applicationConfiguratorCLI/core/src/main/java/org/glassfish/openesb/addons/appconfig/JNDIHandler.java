/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-jbi-components.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-jbi-components.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)JNDIHandler.java
 *
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */
package org.glassfish.openesb.addons.appconfig;

import com.sun.enterprise.cli.framework.CLILogger;
import java.io.IOException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Properties;

import javax.management.Attribute;
import javax.management.AttributeList;
import javax.management.MBeanServerConnection;
import javax.management.MBeanServerFactory;
import javax.management.ObjectName;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;
import javax.management.JMException;

import java.util.Iterator;
import java.util.Set;
import javax.management.MBeanInfo;

/**
 * @author Sun Microsystems
 * 
 */
public class JNDIHandler {

    private static final String OBJ_NAME_CUSTOM_RESOURCES = "com.sun.appserv:type=resources,category=config";
    private MBeanServerConnection mbsc = null;

    /**
     * 
     */
    public JNDIHandler(MBeanServerConnection mbsc) {
        this.mbsc = mbsc;
    }

    /**
     * 
     */
    public JNDIHandler(String jmxUrl, String user, String passwd) throws Exception {
        try {
            initMBeanServerConnection(jmxUrl, user, passwd);
        } catch (IOException ex) {
            throw new Exception(ex.getMessage(), ex);
        } catch (JMException ex) {
            throw new Exception(ex.getMessage(), ex);
        }
    }

    public JNDIHandler() {
        initMBeanServerConnection();
    }

    /*
     * Import External System config
     */
    public void importExternalSystemConfig(String extSysId, String config ) throws IOException, JMException {
        String jndiName = getJNDIName(Constants.CAPSENV_ROOT,
                extSysId);
        if (existsCR(jndiName)) {
            updateCR(jndiName, config);
        } else {
            createCR(jndiName, config);
        }
    }

    /*
     * Import Environment Override config
     */
    public void importJMSExternalSystemConfig(String extSysId, Properties props) throws Exception {

        String jndiName = getJNDIName(Constants.CAPSENV_ROOT,
                extSysId);

        cleanProperties(props);
        if (existsCR(jndiName)) {
            updateCR(jndiName, props);
        } else {
            createCR(jndiName, props);
        }

    }

    private void cleanProperties(Properties props){
        Enumeration keys = props.keys();
        while (keys.hasMoreElements() ){
            String key = (String) keys.nextElement();
            String val = props.getProperty(key);
            if ("".equals(val)){
                // if a property has no value, it causes MBeanException:
                // with message: Property <key> can not be removed.
                props.setProperty(key, "<" + key + ">");
            }
        }
        
    }
    /*
     * Import Connector Connection Pool config
     */
//    public void importCCPConfig(String config, String poolName) throws IOException, JMException {
//        updateJCAConfig(poolName, config);
//    }

    public boolean existsCR(String jndiName) throws IOException {
        boolean crExists = false;
        ObjectName objName = null;
        try {
            //invoke call below throws exceptions when the mbean does not exists.
            //Its difficult to distinguish this condition with real error 
            //conditions. TODO: need a better way to check if the instance exists.
            objName = (ObjectName) mbsc.invoke(new ObjectName(
                    OBJ_NAME_CUSTOM_RESOURCES), "getCustomResourceByJndiName",
                    new Object[]{jndiName},
                    new String[]{"java.lang.String"});
        } catch (JMException ex) {
            //see notes above.
        } catch (IOException ex) {
            //see notes above.
        }

        if (objName != null) {
            crExists = true;
        }

        return crExists;
    }

    public ObjectName getJCAPoolObjectName(String jcaName) throws Exception {
        ObjectName pool = (ObjectName) mbsc.invoke(new ObjectName(
                OBJ_NAME_CUSTOM_RESOURCES), "getConnectorConnectionPoolByName",
                new Object[]{jcaName},
                new String[]{"java.lang.String"});

        return pool;
    }

    public String getJCAPoolRAName(ObjectName jcaPoolObjectName) throws IOException, JMException {
        return (String) mbsc.getAttribute(jcaPoolObjectName, "resource-adapter-name");
    }

    public String getJCAPoolConfiguration(ObjectName jcaPoolObjectName) throws Exception {
        return getConfiguration(jcaPoolObjectName);
    }

    public String getJCAPoolConfiguration(String jcaName) throws Exception {
        ObjectName pool = (ObjectName) mbsc.invoke(new ObjectName(
                OBJ_NAME_CUSTOM_RESOURCES), "getConnectorConnectionPoolByName",
                new Object[]{jcaName},
                new String[]{"java.lang.String"});

        return getConfiguration(pool);
    }

    
    public String getCRConfiguration(String extSysId) throws Exception {

        String jndiName = getJNDIName(Constants.CAPSENV_ROOT,
                extSysId);
        //first check if there already an Environment Override(EO) in the server
        ObjectName cr = getCR(jndiName);

        if (cr != null) {
            return getConfiguration(cr);
        } else {
            return null;
        }

    }

    private ObjectName getCR(String jndiName) throws IOException, JMException {
        //TODO: handle this better. There should be another way. As invoke() 
        //call below throw Exception when the instance does not exists.

        ObjectName objName = (ObjectName) mbsc.invoke(new ObjectName(
                OBJ_NAME_CUSTOM_RESOURCES), "getCustomResourceByJndiName",
                new Object[]{jndiName},
                new String[]{"java.lang.String"});

        return objName;
    }

    private void createCR(String jndiName, String config) throws IOException, JMException {
//        print("create Custom Resource: " + jndiName);
        // create custom resource
        AttributeList attrs = new AttributeList();
        // String jndiName = getJNDIName(ConfigConstants.CAPSENV_ROOT,
        // extSysId);
        attrs.add(new Attribute("jndi_name", jndiName));
        attrs.add(new Attribute("res_type", "java.lang.String"));
        attrs.add(new Attribute("factory_class",
                "com.sun.soabi.extsysconfigoverrides.ObjFactory"));

        Properties props = new Properties();
        props.put(Constants.Configuration, config);

        mbsc.invoke(new ObjectName(OBJ_NAME_CUSTOM_RESOURCES),
                "createCustomResource",
                new Object[]{attrs, props, null}, new String[]{
                    attrs.getClass().getName(), "java.util.Properties",
                    "java.lang.String"
                });


    }

    private void createCR(String jndiName, Properties props) throws IOException, JMException {
//        print("create Properties Custom Resource: " + jndiName);
        
        // create custom resource
        AttributeList attrs = new AttributeList();
        // String jndiName = getJNDIName(ConfigConstants.CAPSENV_ROOT,
        // extSysId);
        attrs.add(new Attribute("jndi_name", jndiName));
        attrs.add(new Attribute("res_type", "java.util.Properties"));
        attrs.add(new Attribute("factory_class",
                "com.sun.soabi.extsysconfigoverrides.ObjFactory"));

        mbsc.invoke(new ObjectName(OBJ_NAME_CUSTOM_RESOURCES),
                "createCustomResource",
                new Object[]{attrs, props, null},
                new String[]{attrs.getClass().getName(),
                    "java.util.Properties",
                    "java.lang.String"
                });

    }

    private void updateCR(String jndiName, String config) throws IOException, JMException {
//        print("update Custom Resource: " + jndiName);
        // update existing custom resource
        String strObjName = "com.sun.appserv:category=config" + ",jndi-name=" + jndiName + ",type=custom-resource";
        updateProperty(strObjName, Constants.Configuration, config);
    }

    private void updateCR(String jndiName, Properties props) throws Exception {
//        print("update Custom Resource: " + jndiName);
        
        String strObjName = "com.sun.appserv:category=config" + ",jndi-name=" + jndiName + ",type=custom-resource";
        Set keys = props.keySet();
        for (Iterator it = keys.iterator(); it.hasNext();) {
            String key = (String) it.next();
            String value = props.getProperty(key);
            updateProperty(strObjName, key, value);
        }
    }

    public void updateJCAConfig(String poolName, String config) throws IOException, JMException {
//        print("update Connector Connection Pool: " + poolName);
        // update existing connector connection pool
        // com.sun.appserv:category=config,name=FilePool,type=connector-connection-pool
        String strObjName = "com.sun.appserv:category=config" + ",name=" + poolName + ",type=connector-connection-pool";
        updateProperty(strObjName, Constants.Configuration, config);
    }

    public void updateJMSJCAConfig(String poolName, Properties props) throws Exception {
//        print("update Connector Connection Pool: " + poolName);
        
        String strObjName = "com.sun.appserv:category=config" + ",name=" + poolName + ",type=connector-connection-pool";
        Set keys = props.keySet();
        for (Iterator it = keys.iterator(); it.hasNext();) {
            String key = (String) it.next();
            String value = props.getProperty(key);
            if (!Constants.Configuration.equals(key)) {
                updateProperty(strObjName, key, value);
            }
        }
    }
   
//    private void updateConfiguration(String strObjName, String config) throws IOException, JMException {
//        Attribute attr = new Attribute(ConfigConstants.Configuration, config);
//        mbsc.invoke(new ObjectName(strObjName), "setProperty",
//                new Object[]{attr}, new String[]{attr.getClass().getName()});
//    }
    private void updateProperty(String strObjName, String key, String value) throws IOException, JMException {
        Attribute attr = new Attribute(key, value);
        mbsc.invoke(new ObjectName(strObjName), "setProperty",
                new Object[]{attr}, new String[]{attr.getClass().getName()});
    }

    public static String getJNDIName(String root, String extSysId) {
        //return root + ConfigUtil.idToPath(extSysId, "/");
        return root + Constants.JNDI_PATH_SEPARATOR + 
                extSysId.replace(Constants.ID_SEPARATOR, 
                Constants.JNDI_PATH_SEPARATOR);
    }

    public AttributeList getProperties(ObjectName objName) throws IOException, JMException {
        return  (AttributeList) mbsc.invoke(objName, "getProperties", null, null);
    }

    public String getPropertyValue(ObjectName objName, String propName) throws IOException, JMException {
        return (String) mbsc.invoke(objName,
                "getPropertyValue",
                new Object[]{propName},
                new String[]{"java.lang.String"});
    }

    private String getConfiguration(ObjectName objName) throws IOException, JMException {
        String config = (String) mbsc.invoke(objName,
                "getPropertyValue",
                new Object[]{Constants.Configuration},
                new String[]{"java.lang.String"});
        return config;
    }

    private void initMBeanServerConnection(String jmxUrl, String user,
            String passwd) throws IOException, JMException {
        HashMap env = new HashMap();
        String[] credentials = new String[]{user, passwd};
        env.put("jmx.remote.credentials", credentials);
        JMXServiceURL url = new JMXServiceURL(jmxUrl);
        JMXConnector jmxc = JMXConnectorFactory.connect(url, env);
        mbsc = jmxc.getMBeanServerConnection();
    }

    private void initMBeanServerConnection() {
        mbsc = (MBeanServerConnection) MBeanServerFactory.findMBeanServer(null).get(0);
    }

    private static void print(String msg) {
        CLILogger.getInstance().printMessage(msg);
    }            
    /**
     * @param args
     */
//    public static void main(String[] args) {
//        String configDir = "C:\\CAPS52\\Hawaii\\earConfigurator\\test\\ears\\config";
//        String urlStr = "service:jmx:rmi:///jndi/rmi://localhost:8686/jmxrmi";
//        String user = "admin";
//        String password = "adminadmin";
//    }
}
