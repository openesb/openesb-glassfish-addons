/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-jbi-components.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-jbi-components.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ExportJCAConfigCommand.java
 *
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */
package org.glassfish.openesb.addons.appconfig.commands;

import com.sun.enterprise.cli.framework.CLILogger;
import com.sun.enterprise.cli.framework.CommandException;
import com.sun.enterprise.cli.framework.CommandValidationException;
import org.glassfish.openesb.addons.appconfig.ConfigExporter;
import javax.management.MBeanServerConnection;
import javax.management.ObjectName;

/**
 * @author Sun Microsystems
 *
 */
public class ExportJCAConfigCommand extends ExtSysConfigCommand {

    /* (non-Javadoc)
     * @see com.sun.enterprise.cli.framework.Command#runCommand()
     */
    @Override
    public void runCommand()
            throws CommandException, CommandValidationException {
        validateOptions();

        String host = getHost();
        int port = getPort();
        String user = getUser();
        String passwd = getPassword();
        String configDir = getConfigDirOption();
        boolean overwrite = getOverwriteOption();
        String poolName = getPoolNameOperand();
        
        MBeanServerConnection mbsc = getMBeanServerConnection(host, port, user, passwd);
        try {
            validateConfigDir(configDir);

            //test that the server is running
            // TODO: is there a better way?
            mbsc.invoke(new ObjectName(OBJ_NAME_CUSTOM_RESOURCES), "getCustomResourceNamesList", null, null);

//            EarConfigurator configurator = new EarConfigurator(configDir);
//            configurator.queryCCP(poolName, mbsc);

            ConfigExporter cexp = new ConfigExporter(configDir, mbsc);
            cexp.exportJCAConfig(poolName, overwrite);
 
            CLILogger.getInstance().printDetailMessage(getLocalizedString(
                    "CommandSuccessful",
                    new Object[]{name}));
        } catch (Exception e) {
            handleException(name, e);
        }
    }

    private String getPoolNameOperand()
            throws CommandValidationException {
        String poolName = (String) getOperands().get(0);
        return poolName;
    }
}
