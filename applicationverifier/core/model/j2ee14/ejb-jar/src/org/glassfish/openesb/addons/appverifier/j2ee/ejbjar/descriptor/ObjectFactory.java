/* DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * 
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 * 
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common Development
 * and Distribution License("CDDL") (collectively, the "License").  You
 * may not use this file except in compliance with the License. You can obtain
 * a copy of the License at https://glassfish.dev.java.net/public/CDDL+GPL.html
 * or glassfish/bootstrap/legal/LICENSE.txt.  See the License for the specific
 * language governing permissions and limitations under the License.
 * 
 * When distributing the software, include this License Header Notice in each
 * file and include the License file at glassfish/bootstrap/legal/LICENSE.txt.
 * Sun designates this particular file as subject to the "Classpath" exception
 * as provided by Sun in the GPL Version 2 section of the License file that
 * accompanied this code.  If applicable, add the following below the License
 * Header, with the fields enclosed by brackets [] replaced by your own
 * identifying information: "Portions Copyrighted [year]
 * [name of copyright owner]"
 * 
 * Contributor(s):
 * 
 * If you wish your version of this file to be governed by only the CDDL or
 * only the GPL Version 2, indicate your decision by adding "[Contributor]
 * elects to include this software in this distribution under the [CDDL or GPL
 * Version 2] license."  If you don't indicate a single choice of license, a
 * recipient has the option to distribute your version of this file under
 * either the CDDL, the GPL Version 2 or to extend the choice of license to
 * its licensees as provided above.  However, if you add GPL Version 2 code
 * and therefore, elected the GPL Version 2 license, then the option applies
 * only if the new code is made subject to such option by the copyright
 * holder.
 */

//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vhudson-jaxb-ri-2.1-463 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2007.10.16 at 06:01:28 PM PDT 
//


package org.glassfish.openesb.addons.appverifier.j2ee.ejbjar.descriptor;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.stc.applicationVerifier.j2ee.ejbjar.descriptor package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _EjbJar_QNAME = new QName("http://java.sun.com/xml/ns/j2ee", "ejb-jar");
    private final static QName _EjbRelationTypeEjbRelationshipRole_QNAME = new QName("http://java.sun.com/xml/ns/j2ee", "ejb-relationship-role");
    private final static QName _EjbRelationTypeEjbRelationName_QNAME = new QName("http://java.sun.com/xml/ns/j2ee", "ejb-relation-name");
    private final static QName _EjbRelationTypeDescription_QNAME = new QName("http://java.sun.com/xml/ns/j2ee", "description");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.stc.applicationVerifier.j2ee.ejbjar.descriptor
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link org.glassfish.openesb.addons.appverifier.j2ee.ejbjar.descriptor.TransAttributeType}
     * 
     */
    public TransAttributeType createTransAttributeType() {
        return new TransAttributeType();
    }

    /**
     * Create an instance of {@link org.glassfish.openesb.addons.appverifier.j2ee.ejbjar.descriptor.RemoteType}
     * 
     */
    public RemoteType createRemoteType() {
        return new RemoteType();
    }

    /**
     * Create an instance of {@link org.glassfish.openesb.addons.appverifier.j2ee.ejbjar.descriptor.JavaTypeType}
     * 
     */
    public JavaTypeType createJavaTypeType() {
        return new JavaTypeType();
    }

    /**
     * Create an instance of {@link org.glassfish.openesb.addons.appverifier.j2ee.ejbjar.descriptor.JndiNameType}
     * 
     */
    public JndiNameType createJndiNameType() {
        return new JndiNameType();
    }

    /**
     * Create an instance of {@link org.glassfish.openesb.addons.appverifier.j2ee.ejbjar.descriptor.ResultTypeMappingType}
     * 
     */
    public ResultTypeMappingType createResultTypeMappingType() {
        return new ResultTypeMappingType();
    }

    /**
     * Create an instance of {@link org.glassfish.openesb.addons.appverifier.j2ee.ejbjar.descriptor.CmpFieldType}
     * 
     */
    public CmpFieldType createCmpFieldType() {
        return new CmpFieldType();
    }

    /**
     * Create an instance of {@link org.glassfish.openesb.addons.appverifier.j2ee.ejbjar.descriptor.XsdPositiveIntegerType}
     * 
     */
    public XsdPositiveIntegerType createXsdPositiveIntegerType() {
        return new XsdPositiveIntegerType();
    }

    /**
     * Create an instance of {@link org.glassfish.openesb.addons.appverifier.j2ee.ejbjar.descriptor.ExcludeListType}
     * 
     */
    public ExcludeListType createExcludeListType() {
        return new ExcludeListType();
    }

    /**
     * Create an instance of {@link org.glassfish.openesb.addons.appverifier.j2ee.ejbjar.descriptor.EnvEntryType}
     * 
     */
    public EnvEntryType createEnvEntryType() {
        return new EnvEntryType();
    }

    /**
     * Create an instance of {@link org.glassfish.openesb.addons.appverifier.j2ee.ejbjar.descriptor.LocalType}
     * 
     */
    public LocalType createLocalType() {
        return new LocalType();
    }

    /**
     * Create an instance of {@link org.glassfish.openesb.addons.appverifier.j2ee.ejbjar.descriptor.XsdIntegerType}
     * 
     */
    public XsdIntegerType createXsdIntegerType() {
        return new XsdIntegerType();
    }

    /**
     * Create an instance of {@link org.glassfish.openesb.addons.appverifier.j2ee.ejbjar.descriptor.EjbLinkType}
     * 
     */
    public EjbLinkType createEjbLinkType() {
        return new EjbLinkType();
    }

    /**
     * Create an instance of {@link org.glassfish.openesb.addons.appverifier.j2ee.ejbjar.descriptor.MethodNameType}
     * 
     */
    public MethodNameType createMethodNameType() {
        return new MethodNameType();
    }

    /**
     * Create an instance of {@link org.glassfish.openesb.addons.appverifier.j2ee.ejbjar.descriptor.ActivationConfigPropertyType}
     * 
     */
    public ActivationConfigPropertyType createActivationConfigPropertyType() {
        return new ActivationConfigPropertyType();
    }

    /**
     * Create an instance of {@link org.glassfish.openesb.addons.appverifier.j2ee.ejbjar.descriptor.MultiplicityType}
     * 
     */
    public MultiplicityType createMultiplicityType() {
        return new MultiplicityType();
    }

    /**
     * Create an instance of {@link org.glassfish.openesb.addons.appverifier.j2ee.ejbjar.descriptor.EntityBeanType}
     * 
     */
    public EntityBeanType createEntityBeanType() {
        return new EntityBeanType();
    }

    /**
     * Create an instance of {@link org.glassfish.openesb.addons.appverifier.j2ee.ejbjar.descriptor.AssemblyDescriptorType}
     * 
     */
    public AssemblyDescriptorType createAssemblyDescriptorType() {
        return new AssemblyDescriptorType();
    }

    /**
     * Create an instance of {@link org.glassfish.openesb.addons.appverifier.j2ee.ejbjar.descriptor.ContainerTransactionType}
     * 
     */
    public ContainerTransactionType createContainerTransactionType() {
        return new ContainerTransactionType();
    }

    /**
     * Create an instance of {@link org.glassfish.openesb.addons.appverifier.j2ee.ejbjar.descriptor.MessageDestinationRefType}
     * 
     */
    public MessageDestinationRefType createMessageDestinationRefType() {
        return new MessageDestinationRefType();
    }

    /**
     * Create an instance of {@link org.glassfish.openesb.addons.appverifier.j2ee.ejbjar.descriptor.SecurityRoleType}
     * 
     */
    public SecurityRoleType createSecurityRoleType() {
        return new SecurityRoleType();
    }

    /**
     * Create an instance of {@link org.glassfish.openesb.addons.appverifier.j2ee.ejbjar.descriptor.XsdBooleanType}
     * 
     */
    public XsdBooleanType createXsdBooleanType() {
        return new XsdBooleanType();
    }

    /**
     * Create an instance of {@link org.glassfish.openesb.addons.appverifier.j2ee.ejbjar.descriptor.SessionBeanType}
     * 
     */
    public SessionBeanType createSessionBeanType() {
        return new SessionBeanType();
    }

    /**
     * Create an instance of {@link org.glassfish.openesb.addons.appverifier.j2ee.ejbjar.descriptor.TrueFalseType}
     * 
     */
    public TrueFalseType createTrueFalseType() {
        return new TrueFalseType();
    }

    /**
     * Create an instance of {@link org.glassfish.openesb.addons.appverifier.j2ee.ejbjar.descriptor.CmpVersionType}
     * 
     */
    public CmpVersionType createCmpVersionType() {
        return new CmpVersionType();
    }

    /**
     * Create an instance of {@link org.glassfish.openesb.addons.appverifier.j2ee.ejbjar.descriptor.EjbRefType}
     * 
     */
    public EjbRefType createEjbRefType() {
        return new EjbRefType();
    }

    /**
     * Create an instance of {@link org.glassfish.openesb.addons.appverifier.j2ee.ejbjar.descriptor.IconType}
     * 
     */
    public IconType createIconType() {
        return new IconType();
    }

    /**
     * Create an instance of {@link org.glassfish.openesb.addons.appverifier.j2ee.ejbjar.descriptor.ActivationConfigType}
     * 
     */
    public ActivationConfigType createActivationConfigType() {
        return new ActivationConfigType();
    }

    /**
     * Create an instance of {@link org.glassfish.openesb.addons.appverifier.j2ee.ejbjar.descriptor.PathType}
     * 
     */
    public PathType createPathType() {
        return new PathType();
    }

    /**
     * Create an instance of {@link org.glassfish.openesb.addons.appverifier.j2ee.ejbjar.descriptor.SecurityIdentityType}
     * 
     */
    public SecurityIdentityType createSecurityIdentityType() {
        return new SecurityIdentityType();
    }

    /**
     * Create an instance of {@link org.glassfish.openesb.addons.appverifier.j2ee.ejbjar.descriptor.DisplayNameType}
     * 
     */
    public DisplayNameType createDisplayNameType() {
        return new DisplayNameType();
    }

    /**
     * Create an instance of {@link org.glassfish.openesb.addons.appverifier.j2ee.ejbjar.descriptor.QueryMethodType}
     * 
     */
    public QueryMethodType createQueryMethodType() {
        return new QueryMethodType();
    }

    /**
     * Create an instance of {@link org.glassfish.openesb.addons.appverifier.j2ee.ejbjar.descriptor.SecurityRoleRefType}
     * 
     */
    public SecurityRoleRefType createSecurityRoleRefType() {
        return new SecurityRoleRefType();
    }

    /**
     * Create an instance of {@link org.glassfish.openesb.addons.appverifier.j2ee.ejbjar.descriptor.FullyQualifiedClassType}
     * 
     */
    public FullyQualifiedClassType createFullyQualifiedClassType() {
        return new FullyQualifiedClassType();
    }

    /**
     * Create an instance of {@link org.glassfish.openesb.addons.appverifier.j2ee.ejbjar.descriptor.EmptyType}
     * 
     */
    public EmptyType createEmptyType() {
        return new EmptyType();
    }

    /**
     * Create an instance of {@link org.glassfish.openesb.addons.appverifier.j2ee.ejbjar.descriptor.MethodType}
     * 
     */
    public MethodType createMethodType() {
        return new MethodType();
    }

    /**
     * Create an instance of {@link org.glassfish.openesb.addons.appverifier.j2ee.ejbjar.descriptor.EjbLocalRefType}
     * 
     */
    public EjbLocalRefType createEjbLocalRefType() {
        return new EjbLocalRefType();
    }

    /**
     * Create an instance of {@link org.glassfish.openesb.addons.appverifier.j2ee.ejbjar.descriptor.XsdAnyURIType}
     * 
     */
    public XsdAnyURIType createXsdAnyURIType() {
        return new XsdAnyURIType();
    }

    /**
     * Create an instance of {@link org.glassfish.openesb.addons.appverifier.j2ee.ejbjar.descriptor.ServiceRefType}
     * 
     */
    public ServiceRefType createServiceRefType() {
        return new ServiceRefType();
    }

    /**
     * Create an instance of {@link org.glassfish.openesb.addons.appverifier.j2ee.ejbjar.descriptor.PersistenceTypeType}
     * 
     */
    public PersistenceTypeType createPersistenceTypeType() {
        return new PersistenceTypeType();
    }

    /**
     * Create an instance of {@link org.glassfish.openesb.addons.appverifier.j2ee.ejbjar.descriptor.SessionTypeType}
     * 
     */
    public SessionTypeType createSessionTypeType() {
        return new SessionTypeType();
    }

    /**
     * Create an instance of {@link org.glassfish.openesb.addons.appverifier.j2ee.ejbjar.descriptor.XsdStringType}
     * 
     */
    public XsdStringType createXsdStringType() {
        return new XsdStringType();
    }

    /**
     * Create an instance of {@link org.glassfish.openesb.addons.appverifier.j2ee.ejbjar.descriptor.CmrFieldTypeType}
     * 
     */
    public CmrFieldTypeType createCmrFieldTypeType() {
        return new CmrFieldTypeType();
    }

    /**
     * Create an instance of {@link org.glassfish.openesb.addons.appverifier.j2ee.ejbjar.descriptor.EjbRefNameType}
     * 
     */
    public EjbRefNameType createEjbRefNameType() {
        return new EjbRefNameType();
    }

    /**
     * Create an instance of {@link org.glassfish.openesb.addons.appverifier.j2ee.ejbjar.descriptor.RelationshipsType}
     * 
     */
    public RelationshipsType createRelationshipsType() {
        return new RelationshipsType();
    }

    /**
     * Create an instance of {@link org.glassfish.openesb.addons.appverifier.j2ee.ejbjar.descriptor.ResourceRefType}
     * 
     */
    public ResourceRefType createResourceRefType() {
        return new ResourceRefType();
    }

    /**
     * Create an instance of {@link org.glassfish.openesb.addons.appverifier.j2ee.ejbjar.descriptor.MethodIntfType}
     * 
     */
    public MethodIntfType createMethodIntfType() {
        return new MethodIntfType();
    }

    /**
     * Create an instance of {@link org.glassfish.openesb.addons.appverifier.j2ee.ejbjar.descriptor.EjbNameType}
     * 
     */
    public EjbNameType createEjbNameType() {
        return new EjbNameType();
    }

    /**
     * Create an instance of {@link org.glassfish.openesb.addons.appverifier.j2ee.ejbjar.descriptor.EjbRelationType}
     * 
     */
    public EjbRelationType createEjbRelationType() {
        return new EjbRelationType();
    }

    /**
     * Create an instance of {@link org.glassfish.openesb.addons.appverifier.j2ee.ejbjar.descriptor.RoleNameType}
     * 
     */
    public RoleNameType createRoleNameType() {
        return new RoleNameType();
    }

    /**
     * Create an instance of {@link org.glassfish.openesb.addons.appverifier.j2ee.ejbjar.descriptor.XsdQNameType}
     * 
     */
    public XsdQNameType createXsdQNameType() {
        return new XsdQNameType();
    }

    /**
     * Create an instance of {@link org.glassfish.openesb.addons.appverifier.j2ee.ejbjar.descriptor.RelationshipRoleSourceType}
     * 
     */
    public RelationshipRoleSourceType createRelationshipRoleSourceType() {
        return new RelationshipRoleSourceType();
    }

    /**
     * Create an instance of {@link org.glassfish.openesb.addons.appverifier.j2ee.ejbjar.descriptor.MessageDestinationType}
     * 
     */
    public MessageDestinationType createMessageDestinationType() {
        return new MessageDestinationType();
    }

    /**
     * Create an instance of {@link org.glassfish.openesb.addons.appverifier.j2ee.ejbjar.descriptor.TransactionTypeType}
     * 
     */
    public TransactionTypeType createTransactionTypeType() {
        return new TransactionTypeType();
    }

    /**
     * Create an instance of {@link org.glassfish.openesb.addons.appverifier.j2ee.ejbjar.descriptor.ServiceRefHandlerType}
     * 
     */
    public ServiceRefHandlerType createServiceRefHandlerType() {
        return new ServiceRefHandlerType();
    }

    /**
     * Create an instance of {@link org.glassfish.openesb.addons.appverifier.j2ee.ejbjar.descriptor.String}
     * 
     */
    public String createString() {
        return new String();
    }

    /**
     * Create an instance of {@link org.glassfish.openesb.addons.appverifier.j2ee.ejbjar.descriptor.CmrFieldType}
     * 
     */
    public CmrFieldType createCmrFieldType() {
        return new CmrFieldType();
    }

    /**
     * Create an instance of {@link org.glassfish.openesb.addons.appverifier.j2ee.ejbjar.descriptor.EjbRefTypeType}
     * 
     */
    public EjbRefTypeType createEjbRefTypeType() {
        return new EjbRefTypeType();
    }

    /**
     * Create an instance of {@link org.glassfish.openesb.addons.appverifier.j2ee.ejbjar.descriptor.EnterpriseBeansType}
     * 
     */
    public EnterpriseBeansType createEnterpriseBeansType() {
        return new EnterpriseBeansType();
    }

    /**
     * Create an instance of {@link org.glassfish.openesb.addons.appverifier.j2ee.ejbjar.descriptor.EjbJarType}
     * 
     */
    public EjbJarType createEjbJarType() {
        return new EjbJarType();
    }

    /**
     * Create an instance of {@link org.glassfish.openesb.addons.appverifier.j2ee.ejbjar.descriptor.UrlPatternType}
     * 
     */
    public UrlPatternType createUrlPatternType() {
        return new UrlPatternType();
    }

    /**
     * Create an instance of {@link org.glassfish.openesb.addons.appverifier.j2ee.ejbjar.descriptor.MessageDrivenBeanType}
     * 
     */
    public MessageDrivenBeanType createMessageDrivenBeanType() {
        return new MessageDrivenBeanType();
    }

    /**
     * Create an instance of {@link org.glassfish.openesb.addons.appverifier.j2ee.ejbjar.descriptor.JavaIdentifierType}
     * 
     */
    public JavaIdentifierType createJavaIdentifierType() {
        return new JavaIdentifierType();
    }

    /**
     * Create an instance of {@link org.glassfish.openesb.addons.appverifier.j2ee.ejbjar.descriptor.MethodPermissionType}
     * 
     */
    public MethodPermissionType createMethodPermissionType() {
        return new MethodPermissionType();
    }

    /**
     * Create an instance of {@link org.glassfish.openesb.addons.appverifier.j2ee.ejbjar.descriptor.EjbRelationshipRoleType}
     * 
     */
    public EjbRelationshipRoleType createEjbRelationshipRoleType() {
        return new EjbRelationshipRoleType();
    }

    /**
     * Create an instance of {@link org.glassfish.openesb.addons.appverifier.j2ee.ejbjar.descriptor.GenericBooleanType}
     * 
     */
    public GenericBooleanType createGenericBooleanType() {
        return new GenericBooleanType();
    }

    /**
     * Create an instance of {@link org.glassfish.openesb.addons.appverifier.j2ee.ejbjar.descriptor.MethodParamsType}
     * 
     */
    public MethodParamsType createMethodParamsType() {
        return new MethodParamsType();
    }

    /**
     * Create an instance of {@link org.glassfish.openesb.addons.appverifier.j2ee.ejbjar.descriptor.ParamValueType}
     * 
     */
    public ParamValueType createParamValueType() {
        return new ParamValueType();
    }

    /**
     * Create an instance of {@link org.glassfish.openesb.addons.appverifier.j2ee.ejbjar.descriptor.QueryType}
     * 
     */
    public QueryType createQueryType() {
        return new QueryType();
    }

    /**
     * Create an instance of {@link org.glassfish.openesb.addons.appverifier.j2ee.ejbjar.descriptor.DescriptionType}
     * 
     */
    public DescriptionType createDescriptionType() {
        return new DescriptionType();
    }

    /**
     * Create an instance of {@link org.glassfish.openesb.addons.appverifier.j2ee.ejbjar.descriptor.MessageDestinationUsageType}
     * 
     */
    public MessageDestinationUsageType createMessageDestinationUsageType() {
        return new MessageDestinationUsageType();
    }

    /**
     * Create an instance of {@link org.glassfish.openesb.addons.appverifier.j2ee.ejbjar.descriptor.XsdNonNegativeIntegerType}
     * 
     */
    public XsdNonNegativeIntegerType createXsdNonNegativeIntegerType() {
        return new XsdNonNegativeIntegerType();
    }

    /**
     * Create an instance of {@link org.glassfish.openesb.addons.appverifier.j2ee.ejbjar.descriptor.PortComponentRefType}
     * 
     */
    public PortComponentRefType createPortComponentRefType() {
        return new PortComponentRefType();
    }

    /**
     * Create an instance of {@link org.glassfish.openesb.addons.appverifier.j2ee.ejbjar.descriptor.EnvEntryTypeValuesType}
     * 
     */
    public EnvEntryTypeValuesType createEnvEntryTypeValuesType() {
        return new EnvEntryTypeValuesType();
    }

    /**
     * Create an instance of {@link org.glassfish.openesb.addons.appverifier.j2ee.ejbjar.descriptor.HomeType}
     * 
     */
    public HomeType createHomeType() {
        return new HomeType();
    }

    /**
     * Create an instance of {@link org.glassfish.openesb.addons.appverifier.j2ee.ejbjar.descriptor.ResAuthType}
     * 
     */
    public ResAuthType createResAuthType() {
        return new ResAuthType();
    }

    /**
     * Create an instance of {@link org.glassfish.openesb.addons.appverifier.j2ee.ejbjar.descriptor.XsdNMTOKENType}
     * 
     */
    public XsdNMTOKENType createXsdNMTOKENType() {
        return new XsdNMTOKENType();
    }

    /**
     * Create an instance of {@link org.glassfish.openesb.addons.appverifier.j2ee.ejbjar.descriptor.EjbClassType}
     * 
     */
    public EjbClassType createEjbClassType() {
        return new EjbClassType();
    }

    /**
     * Create an instance of {@link org.glassfish.openesb.addons.appverifier.j2ee.ejbjar.descriptor.LocalHomeType}
     * 
     */
    public LocalHomeType createLocalHomeType() {
        return new LocalHomeType();
    }

    /**
     * Create an instance of {@link org.glassfish.openesb.addons.appverifier.j2ee.ejbjar.descriptor.ResSharingScopeType}
     * 
     */
    public ResSharingScopeType createResSharingScopeType() {
        return new ResSharingScopeType();
    }

    /**
     * Create an instance of {@link org.glassfish.openesb.addons.appverifier.j2ee.ejbjar.descriptor.MessageDestinationTypeType}
     * 
     */
    public MessageDestinationTypeType createMessageDestinationTypeType() {
        return new MessageDestinationTypeType();
    }

    /**
     * Create an instance of {@link org.glassfish.openesb.addons.appverifier.j2ee.ejbjar.descriptor.RunAsType}
     * 
     */
    public RunAsType createRunAsType() {
        return new RunAsType();
    }

    /**
     * Create an instance of {@link org.glassfish.openesb.addons.appverifier.j2ee.ejbjar.descriptor.ResourceEnvRefType}
     * 
     */
    public ResourceEnvRefType createResourceEnvRefType() {
        return new ResourceEnvRefType();
    }

    /**
     * Create an instance of {@link org.glassfish.openesb.addons.appverifier.j2ee.ejbjar.descriptor.MessageDestinationLinkType}
     * 
     */
    public MessageDestinationLinkType createMessageDestinationLinkType() {
        return new MessageDestinationLinkType();
    }

    /**
     * Create an instance of {@link org.glassfish.openesb.addons.appverifier.j2ee.ejbjar.descriptor.ListenerType}
     * 
     */
    public ListenerType createListenerType() {
        return new ListenerType();
    }

    /**
     * Create an instance of {@link JAXBElement} {@code <} {@link org.glassfish.openesb.addons.appverifier.j2ee.ejbjar.descriptor.EjbJarType} {@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://java.sun.com/xml/ns/j2ee", name = "ejb-jar")
    public JAXBElement<EjbJarType> createEjbJar(EjbJarType value) {
        return new JAXBElement<EjbJarType>(_EjbJar_QNAME, EjbJarType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement} {@code <} {@link org.glassfish.openesb.addons.appverifier.j2ee.ejbjar.descriptor.EjbRelationshipRoleType} {@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://java.sun.com/xml/ns/j2ee", name = "ejb-relationship-role", scope = EjbRelationType.class)
    public JAXBElement<EjbRelationshipRoleType> createEjbRelationTypeEjbRelationshipRole(EjbRelationshipRoleType value) {
        return new JAXBElement<EjbRelationshipRoleType>(_EjbRelationTypeEjbRelationshipRole_QNAME, EjbRelationshipRoleType.class, EjbRelationType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement} {@code <} {@link org.glassfish.openesb.addons.appverifier.j2ee.ejbjar.descriptor.String} {@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://java.sun.com/xml/ns/j2ee", name = "ejb-relation-name", scope = EjbRelationType.class)
    public JAXBElement<String> createEjbRelationTypeEjbRelationName(String value) {
        return new JAXBElement<String>(_EjbRelationTypeEjbRelationName_QNAME, String.class, EjbRelationType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement} {@code <} {@link org.glassfish.openesb.addons.appverifier.j2ee.ejbjar.descriptor.DescriptionType} {@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://java.sun.com/xml/ns/j2ee", name = "description", scope = EjbRelationType.class)
    public JAXBElement<DescriptionType> createEjbRelationTypeDescription(DescriptionType value) {
        return new JAXBElement<DescriptionType>(_EjbRelationTypeDescription_QNAME, DescriptionType.class, EjbRelationType.class, value);
    }

}
