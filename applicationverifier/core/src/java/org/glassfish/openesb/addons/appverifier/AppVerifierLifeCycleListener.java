/*
  * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
  *
  * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
  *
  * The contents of this file are subject to the terms of either the GNU
  * General Public License Version 2 only ("GPL") or the Common Development
  * and Distribution License("CDDL") (collectively, the "License").  You
  * may not use this file except in compliance with the License. You can obtain
  * a copy of the License at https://glassfish.dev.java.net/public/CDDL+GPL.html
  * or glassfish/bootstrap/legal/LICENSE.txt.  See the License for the specific
  * language governing permissions and limitations under the License.
  *
  * When distributing the software, include this License Header Notice in each
  * file and include the License file at glassfish/bootstrap/legal/LICENSE.txt.
  * Sun designates this particular file as subject to the "Classpath" exception
  * as provided by Sun in the GPL Version 2 section of the License file that
  * accompanied this code.  If applicable, add the following below the License
  * Header, with the fields enclosed by brackets [] replaced by your own
  * identifying information: "Portions Copyrighted [year]
  * [name of copyright owner]"
  *
  * Contributor(s):
  *
  * If you wish your version of this file to be governed by only the CDDL or
  * only the GPL Version 2, indicate your decision by adding "[Contributor]
  * elects to include this software in this distribution under the [CDDL or GPL
  * Version 2] license."  If you don't indicate a single choice of license, a
  * recipient has the option to distribute your version of this file under
  * either the CDDL, the GPL Version 2 or to extend the choice of license to
  * its licensees as provided above.  However, if you add GPL Version 2 code
  * and therefore, elected the GPL Version 2 license, then the option applies
  * only if the new code is made subject to such option by the copyright
  * holder.
  */

package org.glassfish.openesb.addons.appverifier;

import com.sun.appserv.server.LifecycleListener;
import com.sun.appserv.server.LifecycleEvent;
import com.sun.appserv.server.LifecycleEventContext;
import com.sun.appserv.server.ServerLifecycleException;

import java.util.Properties;

import javax.management.MBeanServer;
import javax.management.ObjectName;

import java.util.logging.Logger;
import java.util.logging.Level;

/**
 *  Implemenation for Lifecycle Listener
 *  @author Sreeni Genipudi
 */
public class

AppVerifierLifeCycleListener implements LifecycleListener {
    private Logger mLog = 
        Logger.getLogger("org.glassfish.openesb.addons.appverifier.AppVerifierLifeCycleListener");
    private ObjectName mObjectName = null;
    private static final String MBEAN_OBJ_NAME = 
        "com.sun.jbi:ServiceName=JavaEEVerifier,ComponentType=System";
    private MBeanServer mbeanServer = null;

    /**
     *  Life cycle event context
     */
    private LifecycleEventContext context;

    public AppVerifierLifeCycleListener() {
        mbeanServer = 
                com.sun.enterprise.admin.common.MBeanServerFactory.getMBeanServer();
    }

    public void handleEvent(LifecycleEvent event) throws ServerLifecycleException {
        mLog.fine("on handleEvent()");
        context = event.getLifecycleEventContext();

        switch (event.getEventType()) {
        case LifecycleEvent.INIT_EVENT:
            break;
        case LifecycleEvent.STARTUP_EVENT:
            initialize();
            break;
        case LifecycleEvent.READY_EVENT:
            initialize();
            break;
        case LifecycleEvent.SHUTDOWN_EVENT:
            shutdown();
            break;
        case LifecycleEvent.TERMINATION_EVENT:
            onTermination();
            break;
        }
    }

    private void initialize() throws ServerLifecycleException {


        try {
            if (mObjectName == null) {
                mObjectName = new ObjectName(MBEAN_OBJ_NAME);
            }

            if (mbeanServer == null) {
                mbeanServer = 
                        com.sun.enterprise.admin.common.MBeanServerFactory.getMBeanServer();
            }
            if (!mbeanServer.isRegistered(mObjectName)) {
                JavaEEVerifierMonitor javaEEMBean = 
                    new JavaEEVerifierMonitor(mbeanServer);
                mbeanServer.registerMBean(javaEEMBean, mObjectName);
                mLog.fine("registerMBean done" + MBEAN_OBJ_NAME);
            }

        } catch (Exception ex) {
            mLog.log(Level.SEVERE, "initialize() ", ex);
            throw new ServerLifecycleException(ex);
        }
    }

    private void shutdown() throws ServerLifecycleException {
        try {
            if (mObjectName != null && mbeanServer.isRegistered(mObjectName)) {
                mbeanServer.unregisterMBean(mObjectName);
                mLog.fine("unregisterMBean done " + MBEAN_OBJ_NAME);
            }
            mObjectName = null;
            mbeanServer = null;
        } catch (Exception ex) {
            //throw new ServerLifecycleException(ex);
            mLog.log(Level.SEVERE, ex.getMessage(), ex);
        }
    }

    private void onTermination() throws ServerLifecycleException {
        try {
            if (mObjectName != null) {
                shutdown();
            }
            mObjectName = null;
            mbeanServer = null;

        } catch (Exception ex) {
            //throw new ServerLifecycleException(ex);
            mLog.log(Level.SEVERE, ex.getMessage(), ex);
        }

    }
}
