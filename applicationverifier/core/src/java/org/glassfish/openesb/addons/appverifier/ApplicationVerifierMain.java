/*
  * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
  *
  * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
  *
  * The contents of this file are subject to the terms of either the GNU
  * General Public License Version 2 only ("GPL") or the Common Development
  * and Distribution License("CDDL") (collectively, the "License").  You
  * may not use this file except in compliance with the License. You can obtain
  * a copy of the License at https://glassfish.dev.java.net/public/CDDL+GPL.html
  * or glassfish/bootstrap/legal/LICENSE.txt.  See the License for the specific
  * language governing permissions and limitations under the License.
  *
  * When distributing the software, include this License Header Notice in each
  * file and include the License file at glassfish/bootstrap/legal/LICENSE.txt.
  * Sun designates this particular file as subject to the "Classpath" exception
  * as provided by Sun in the GPL Version 2 section of the License file that
  * accompanied this code.  If applicable, add the following below the License
  * Header, with the fields enclosed by brackets [] replaced by your own
  * identifying information: "Portions Copyrighted [year]
  * [name of copyright owner]"
  *
  * Contributor(s):
  *
  * If you wish your version of this file to be governed by only the CDDL or
  * only the GPL Version 2, indicate your decision by adding "[Contributor]
  * elects to include this software in this distribution under the [CDDL or GPL
  * Version 2] license."  If you don't indicate a single choice of license, a
  * recipient has the option to distribute your version of this file under
  * either the CDDL, the GPL Version 2 or to extend the choice of license to
  * its licensees as provided above.  However, if you add GPL Version 2 code
  * and therefore, elected the GPL Version 2 license, then the option applies
  * only if the new code is made subject to such option by the copyright
  * holder.
  */

package org.glassfish.openesb.addons.appverifier;

import org.glassfish.openesb.addons.appconfig.Verifier;
import org.glassfish.openesb.addons.appconfig.VerificationResult;
import org.glassfish.openesb.addons.appconfig.VerifierImpl;

import org.glassfish.openesb.addons.appverifier.ejb.ApplicationJ2EEXMLReader;
import org.glassfish.openesb.addons.appverifier.ejb.ApplicationJavaEEReader;
import org.glassfish.openesb.addons.appverifier.ejb.EJBInfo;
import org.glassfish.openesb.addons.appverifier.ejb.EJBReferenceInfo;
import org.glassfish.openesb.addons.appverifier.ejb.EjbJavaEEXMLReader;
import org.glassfish.openesb.addons.appverifier.ejb.EjbReader;
import org.glassfish.openesb.addons.appverifier.ejb.MDBInfo;
import org.glassfish.openesb.addons.appverifier.ejb.ResourceInfo;
import org.glassfish.openesb.addons.appverifier.ejb.SessionBeanInfo;
import org.glassfish.openesb.addons.appverifier.ra.InboundResourceAdapter;
import org.glassfish.openesb.addons.appverifier.ra.OutboundResourceAdapter;
import org.glassfish.openesb.addons.appverifier.ra.ResourceAdapter;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileFilter;

import java.io.FileInputStream;
import java.io.IOException;

import java.io.InputStream;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;

import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.jar.JarInputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

import java.util.zip.ZipEntry;

import javax.management.openmbean.CompositeData;
import javax.management.openmbean.CompositeDataSupport;
import javax.management.openmbean.CompositeType;
import javax.management.openmbean.OpenDataException;
import javax.management.openmbean.SimpleType;

import javax.management.openmbean.TabularData;

import javax.management.openmbean.TabularDataSupport;
import javax.management.openmbean.TabularType;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;


import org.apache.tools.ant.AntClassLoader;


/**
 * Reads EAR file/ EJB jar file and gets information about Resource
 * adapters , EJBs. Validation is done based on ValidationConfig.properties
 *
 * @author Sreeni Genipudi
 */
public class ApplicationVerifierMain {
    /**
     * Constants
     */
    public static final String FOLDER = "FOLDER";
    public static final String METAINF = "meta-inf";
    public static final String APPLICATIONDESCRIPTOR = "application.xml";
    public static final String SUNAPPLICATIONDESCRIPTOR = 
        "sun-application.xml";
    public static final String EJBDESCRIPTOR = "ejb-jar.xml";
    public static final String SUNEJBDESCRIPTOR = "sun-ejb-jar.xml";
    public static final String RADESCRIPTOR = "ra.xml";
    public static final String SUNRADESCRIPTOR = "sun-ra.xml";
    //application reader
    private ApplicationReader mArApplicationXMLReader = null;
    //JAXB Java EE EJB Context
    private JAXBContext mEjbjcJavaee;
    //JAXB Java EE EJB Unmarshaller
    private Unmarshaller mEjbjUmJavaee;
    //JAXB Java EE Sun EJB Context
    private JAXBContext mSunEJBJcJavaee;
    //JAXB Java EE SUN EJB Unmarshaller
    private Unmarshaller mSunEJBUmJavaee;
    //Service Assembly File
    private JarFile mSaFile = null;
    //SU Entry name 
    private JarEntry mSuJarEntry = null;
    //Ear file/EJB path 
    private String mEARFilePath = null;
    //List of inbound resource adapter
    private List<InboundResourceAdapter> mListOfInBoundRAs = 
        new ArrayList<InboundResourceAdapter>();
    //List of outbound resource adapter
    private List<OutboundResourceAdapter> mListOfOutBoundRAs = 
        new ArrayList<OutboundResourceAdapter>();
    //List of EJBs
    private List<EJBInfo> mListOfEJBs = new ArrayList<EJBInfo>();

    private List<VerificationResult> mClassicVerifierResults = null;
    private ResourceBundle mRb = null;
    private static Logger logger = 
        Logger.getLogger("org.glassfish.openesb.addons.appverifier.ApplicationVerifierMain");
    private Verifier mClassicVerifier = null;

    private static final String ERROR_CHECK_APPXML = "ERROR_CHECK_APPXML";
    private static final String RA_NOT_FOUND = "RA_NOT_FOUND";
    private static final String ERROR_READ_EJB_DESCRIPTOR = 
        "ERROR_READ_EJB_DESCRIPTOR";
    private static final String ERROR_WHILE_PROCESSING = 
        "ERROR_WHILE_PROCESSING";
    private static final String ERROR_FOUND_GET_EJB_INFO = 
        "ERROR_FOUND_GET_EJB_INFO";
    private static final String SU_NOT_FOUND = "SU_NOT_FOUND";

    /**
     * Constructor
     * @param earFile EAR FILE instance
     * @param eb ErrorBundle
     * @param config Validation Config.
     */
    public ApplicationVerifierMain(JarFile saFile, String suName, 
                                   String instance, ErrorBundle eb, 
                                   ValidationConfig config) {
        mRb = 
ResourceBundle.getBundle("org.glassfish.openesb.addons.appverifier.Bundle", 
                         Locale.getDefault());
        FileUtil fileUtil = FileUtil.getInstance();
        ClassLoaderUtil classLdrUtil = ClassLoaderUtil.getInstance();
        File applicationXML = null;
        RAHelper raHelper = RAHelper.getInstance(config, eb, instance);

        File mainRootDir = null;
        EjbReader ejbRdr = null;
        InputStream saIs = null;
        JarInputStream saJis = null;
        JarEntry suJe = null;
        //get the SU Jar entry
        if (saFile != null) {
            suJe = saFile.getJarEntry(suName);
            if (suJe == null) {
                String errMsg = this.mRb.getString(SU_NOT_FOUND);
                eb.addError(errMsg);
                throw new RuntimeException(errMsg);
            }
        } else {
            mEARFilePath = suName;
        }
        this.mSaFile = saFile;
        this.mSuJarEntry = suJe;
        String appXML = null;
        try {
            saJis = this.getSuJarIs();
            //Get Application XML
            appXML = fileUtil.getApplicationXML(saJis);
        } catch (IOException e) {
            // TODO
        }
        boolean bJ2EEApplication = false;
        if (appXML != null) {
            if (appXML.indexOf("\"1.4\"") != -1) {
                try {
                    bJ2EEApplication = true;
                    //check if application xml is J2EE or JavaEE
                    mArApplicationXMLReader = 
                            new ApplicationJ2EEXMLReader(appXML);
                } catch (Exception ex) {
                    eb.addError(mRb.getString(ERROR_CHECK_APPXML), ex);
                    //createCompositeData();
                }
            } else if (appXML.indexOf("\"5\"") != -1) {
                try {
                    mArApplicationXMLReader = 
                            new ApplicationJavaEEReader(appXML);
                } catch (Exception ex1) {
                    eb.addError(mRb.getString(ERROR_CHECK_APPXML), ex1);
                }
            }
            try {
                if (mArApplicationXMLReader != null) {
                    //Populate Resource information
                    List<String> listOfRA = 
                        mArApplicationXMLReader.getRAList();
                    String raLocation = null;
                    String sunRALocation = null;

                    File earEntryFile = null;
                    String raDescriptor = null;
                    String sunRADescriptor = null;
                    //First gather local resource adapter information.
                    for (String ra: listOfRA) {
                        InputStream is = 
                            fileUtil.getInputStream(this.getSuJarIs(), ra);

                        String[] raDescArry = fileUtil.readData(is, "ra.xml");
                        if (raDescArry != null) {
                            for (String raDesc: raDescArry) {
                                if (raDesc.indexOf("</sun-connector>") != -1) {
                                    sunRADescriptor = raDesc;
                                } else {
                                    raDescriptor = raDesc;
                                }
                            }
                        }

                        if (!bJ2EEApplication) {
                            if (raDescriptor != null) {
                                ResourceAdapter raWrapper = 
                                    raHelper.getResourceAdapter(getEARFileName(), 
                                                                ra, 
                                                                raDescriptor, 
                                                                sunRADescriptor, 
                                                                ra, config);
                                if (raWrapper instanceof 
                                    InboundResourceAdapter) {
                                    mListOfInBoundRAs.add((InboundResourceAdapter)raWrapper);
                                } else {
                                    this.mListOfOutBoundRAs.add((OutboundResourceAdapter)raWrapper);
                                }
                            } else {
                                eb.addError(mRb.getString(RA_NOT_FOUND) + ra);
                            }
                        }
                        //Nitin: always call classic verifier.
                        //else {
                        //Call Classic verifier pass in ra.xml
                        List<VerificationResult> vrl = 
                            this.getClassicVerifier().verify(raDescriptor);
                        this.mClassicVerifierResults.addAll(vrl);
                        //}
                    }

                    //Next collect EJB Information.
                    List<String> listOfEJB = 
                        mArApplicationXMLReader.getEJBList();
                    File ejbFileLocation = null;


                    for (String ejb: listOfEJB) {
                        String ejbXML = null;
                        String sunEJBXML = null;
                        InputStream is = 
                            fileUtil.getInputStream(this.getSuJarIs(), ejb);
                        String[] ejbDescArry = 
                            fileUtil.readData(is, "ejb-jar.xml");
                        if (ejbDescArry != null) {
                            for (String ejbDesc: ejbDescArry) {
                                if (ejbDesc.indexOf("<sun-ejb-jar") != -1) {
                                    sunEJBXML = ejbDesc;
                                } else {
                                    ejbXML = ejbDesc;
                                }
                            }
                        }

                        //Check if a descriptor is available.
                        //Gather information from descriptor.
                        if (ejbXML != null) {
                            if (ejbXML.indexOf("\"2.1\"") != -1 || 
                                ejbXML.indexOf("ejb-jar_2_0.dtd") != -1) {
                                this.logger.fine(" J2EE Descriptor");
                                //Call Classic verifier pass in ra.xml
                                List<VerificationResult> vrl = 
                                    this.getClassicVerifier().verify(ejbXML);
                                this.mClassicVerifierResults.addAll(vrl);
                            } else {
                                try {
                                    ejbRdr = 
                                            processJavaEEEJBDescriptors(ejbXML, 
                                                                        sunEJBXML, 
                                                                        config, 
                                                                        eb, 
                                                                        instance);
                                } catch (Exception ex) {
                                    EJBInfo ejbInfo = new EJBInfo();
                                    ejbInfo.setEJBName(ejb);
                                    ejbInfo.setStatus(VerifyStatus.ERROR);
                                    ejbInfo.setMessage(mRb.getString(ERROR_READ_EJB_DESCRIPTOR) + 
                                                       ((ex.getMessage() != 
                                                         null) ? 
                                                        ex.getMessage() : ""));
                                    mListOfEJBs.add(ejbInfo);
                                    eb.addError(mRb.getString(ERROR_READ_EJB_DESCRIPTOR));
                                }
                            }
                        } else {
                            //this could be a JAVAEE EJB no descriptor is needed!.
                            JarEntry jarEnt = null;
                            saJis = this.getSuJarIs();
                            while ((jarEnt = saJis.getNextJarEntry()) != 
                                   null) {
                                if (jarEnt.getName().equals(ejb)) {
                                    populateEJBInfoJAVAEE(saJis, jarEnt, 
                                                          config, eb, 
                                                          instance);
                                }
                            }


                        }
                        if (ejbRdr != null) {
                            List<EJBInfo> listOfEjbs = ejbRdr.getEJBList();
                            mListOfEJBs.addAll(listOfEjbs);
                        }
                    }
                }
            } catch (Exception ex) {
                //ex.printStackTrace();

                eb.addError(mRb.getString(ERROR_WHILE_PROCESSING), ex);
            }
        } else {
            //This is definitely a JAVA EE Application. No Application.xml?.
            try {
                //This file may not be a EAR file. It could be a EJB JAR file.
                // Check if the EJB JAR file.
                populateEJBInfoJAVAEEInternal(saJis, this.mSuJarEntry, config, 
                                              eb, instance);

            } catch (Exception e) {
                e.printStackTrace();
                eb.addError(mRb.getString(ERROR_WHILE_PROCESSING), e);
            }
        }

    }

    private JarInputStream getSuJarIs() {
        JarInputStream returnIS = null;
        try {
            //Create jar file instance of EAR file.
            if (this.mEARFilePath == null) {
                InputStream saIs = 
                    this.mSaFile.getInputStream(this.mSuJarEntry);
                JarInputStream saJis = new JarInputStream(saIs);
                returnIS = saJis;
            } else {
                FileInputStream fis = 
                    new FileInputStream(new File(this.mEARFilePath));
                returnIS = new JarInputStream(fis);
            }
        } catch (IOException e) {
            // TODO
        }
        return returnIS;
    }


    /**
     * Populate EJB Information.
     *
     * @param ejbFolderFile
     * @param mainRootDir
     * @param mainClassLoader
     * @param config
     * @param eb
     * @return
     */
    private boolean populateEJBInfoJAVAEEInternal(JarInputStream jis, 
                                                  JarEntry je, 
                                                  ValidationConfig config, 
                                                  ErrorBundle eb, 
                                                  String instance) throws IOException {

        if (je != null && je.getName().endsWith(".ear")) {
            //Enumeration<JarEntry> enum1 = jf.entries();
            JarEntry jarEntry = null;
            while ((jarEntry = jis.getNextJarEntry()) != null) {
                populateEJBInfoJAVAEE(jis, jarEntry, config, eb, instance);
            }
        } else {
            populateEJBInfoJAVAEE(jis, null, config, eb, instance);
        }
        return true;
    }


    /**
     * Populate EJB Information.
     *
     * @param ejbFolderFile
     * @param mainRootDir
     * @param mainClassLoader
     * @param config
     * @param eb
     * @return
     */
    private boolean populateEJBInfoJAVAEE(JarInputStream jis, 
                                          JarEntry jarEntry, 
                                          ValidationConfig config, 
                                          ErrorBundle eb, 
                                          String instance) throws IOException {
        boolean bEJBFound = false;
        InputStream is;
        List<byte[]> classJarBytes = null;
        FileUtil fileUtil = FileUtil.getInstance();
        List<byte[]> ejbJarXMLBytes = null;
        String ejbJarXML = null;
        String sunEjbJarXML = null;
        String tempXML = null;
        EjbReader ejbRdr = null;
        try {
            if (jarEntry != null) {
                is = jis;
                ejbJarXMLBytes = fileUtil.readBytes(is, "ejb-jar.xml");
                is = getInputStream(this.getSuJarIs(), jarEntry);

                classJarBytes = fileUtil.readBytes(is, ".class");
            } else {
                ejbJarXMLBytes = 
                        fileUtil.readAllBytes(this.getSuJarIs(), "ejb-jar.xml");
                JarInputStream jis1 = this.getSuJarIs();
                classJarBytes = fileUtil.readAllBytes(jis1, ".class");

            }
            if (ejbJarXMLBytes != null && ejbJarXMLBytes.size() > 0) {
                if (ejbJarXMLBytes.size() == 1) {
                    ejbJarXML = new String(ejbJarXMLBytes.get(0));
                } else {
                    ejbJarXML = new String(ejbJarXMLBytes.get(0));
                    sunEjbJarXML = new String(ejbJarXMLBytes.get(1));
                    if (ejbJarXML.indexOf("<sun-ejb-jar") != -1) {
                        tempXML = sunEjbJarXML;
                        sunEjbJarXML = ejbJarXML;
                        ejbJarXML = tempXML;
                    }
                }
            }


            // see if descriptor exists.
            if (ejbJarXML != null) {
                if (sunEjbJarXML != null) {
                    ejbRdr = 
                            processJavaEEEJBDescriptors(ejbJarXML, sunEjbJarXML, 
                                                        config, eb, instance);
                    List<EJBInfo> listOfEjbs = ejbRdr.getEJBList();
                    if (listOfEjbs.size() > 0) {
                        bEJBFound = true;
                    }
                    mListOfEJBs.addAll(listOfEjbs);
                }
            }
            // Iterate the folder files one by one and see if it is an EJB
            List<EJBInfo> listOfEJBRefInfo;

            listOfEJBRefInfo = 
                    ClassLoaderUtil.getInstance().getEJBInfo(classJarBytes, 
                                                             ejbRdr, 
                                                             mListOfInBoundRAs, 
                                                             this.mListOfOutBoundRAs, 
                                                             config, eb, 
                                                             instance);
            if (listOfEJBRefInfo != null && listOfEJBRefInfo.size() > 0) {
                bEJBFound = true;
                if (mListOfEJBs != null && mListOfEJBs.size() > 0) {
                    for (EJBInfo ejbInf: listOfEJBRefInfo) {
                        int index = mListOfEJBs.indexOf(ejbInf);
                        if (index >= 0) {
                            EJBInfo descEJBInfo = mListOfEJBs.get(index);
                            List<EJBReferenceInfo> ejbRefList = 
                                descEJBInfo.getEJBReferenceInfo();
                            List<ResourceInfo> resRefList = 
                                descEJBInfo.getResourceReferenceInfo();

                            if (ejbRefList == null || ejbRefList.size() == 0) {
                                descEJBInfo.setEJBReferenceInfo(ejbInf.getEJBReferenceInfo());
                            }
                            if (resRefList == null || resRefList.size() == 0) {
                                descEJBInfo.setResourceReferenceInfo(ejbInf.getResourceReferenceInfo());
                            }
                        } else {
                            mListOfEJBs.add(ejbInf);
                        }
                    }
                } else {
                    mListOfEJBs.addAll(listOfEJBRefInfo);
                }
            }
        } catch (Exception e) {
            eb.addError(mRb.getString(ERROR_FOUND_GET_EJB_INFO));
        }
        return bEJBFound;


    }

    /**
     * Process Java EE EJB descriptor and return EJBReader instance.
     * @param ejbFileLocation
     * @param sunEJBXML
     * @param config
     * @param mainClassLoader
     * @param eb
     * @return
     * @throws Exception
     */
    private EjbReader processJavaEEEJBDescriptors(String ejbXML, 
                                                  String sunEJBXML, 
                                                  ValidationConfig config, 
                                                  ErrorBundle eb, 
                                                  String instance) throws Exception {

        EjbReader ejbRdr = null;
        try {
            if (this.mEjbjcJavaee == null) {
                mEjbjcJavaee = 
                        JAXBContext.newInstance("org.glassfish.openesb.addons.appverifier.javaee.ejb.descriptor");
                mEjbjUmJavaee = mEjbjcJavaee.createUnmarshaller();
            }

            ByteArrayInputStream bis = 
                new ByteArrayInputStream(ejbXML.getBytes());
            javax.xml.bind.JAXBElement je = 
                (javax.xml.bind.JAXBElement)mEjbjUmJavaee.unmarshal(bis);
            org.glassfish.openesb.addons.appverifier.javaee.ejb.descriptor.EjbJarType ej = 
                (org.glassfish.openesb.addons.appverifier.javaee.ejb.descriptor.EjbJarType)je.getValue();
            if (this.mSunEJBJcJavaee == null) {
                mSunEJBJcJavaee = 
                        JAXBContext.newInstance("org.glassfish.openesb.addons.appverifier.javaee.sunejb.descriptor");
                mSunEJBUmJavaee = mSunEJBJcJavaee.createUnmarshaller();
            }
            bis = new ByteArrayInputStream(sunEJBXML.getBytes());
            // je =
            //  (javax.xml.bind.JAXBElement)mSunEJBUmJavaee.unmarshal(sunEJBXML);
            org.glassfish.openesb.addons.appverifier.javaee.sunejb.descriptor.SunEjbJar sej = 
                (org.glassfish.openesb.addons.appverifier.javaee.sunejb.descriptor.SunEjbJar)mSunEJBUmJavaee.unmarshal(bis);
            //            org.glassfish.openesb.addons.appverifier.javaee.sunejb.descriptor.SunEjbJar sej =
            //              (org.glassfish.openesb.addons.appverifier.javaee.sunejb.descriptor.SunEjbJar)je.getValue();
            ejbRdr = 
                    new EjbJavaEEXMLReader(ej, sej, config, null, eb, this.mListOfInBoundRAs, 
                                           this.mListOfOutBoundRAs, instance);
        } catch (Exception e) {
            //                eb.addError("Error while processing Java EE Descriptors = ",e);
            e.printStackTrace();
            throw e;
        }
        return ejbRdr;

    }

    /**
     * Get the list of inbound resource adapter
     * @return List of InboundResourceAdapter
     * @throws Exception
     */
    public List<InboundResourceAdapter> getInboundRAs() throws Exception {
        return this.mListOfInBoundRAs;
    }

    /**
     * Get list of outbound resource adapter.
     * @return list of outbound resource adapter.
     * @throws Exception
     */
    public List<OutboundResourceAdapter> getOutboundRAs() throws Exception {
        return this.mListOfOutBoundRAs;
    }

    /**
     * Get list of Classic verifier results.
     * @return list of Classic EAR verification Results.
     */
    public List<VerificationResult> getClassicVerifierResults() {
        return this.mClassicVerifierResults;
    }

    /**
     * Get List of EJBs
     * @return list of EJBInfo
     */
    public List<EJBInfo> getEJBs() {
        return this.mListOfEJBs;
    }

    private static void displayUsage() {
        // System.out.println("Appverifier <EAR/EJB FILENAME>");
        logger.log(Level.INFO, 
                   "Appverifier <Service assembly jar file with absolute path> <Service unit jar file name>");
    }


    private Verifier getClassicVerifier() {
        if (this.mClassicVerifier == null) {
            mClassicVerifier = new VerifierImpl();
            this.mClassicVerifierResults = new ArrayList<VerificationResult>();
        }
        return this.mClassicVerifier;
    }

    private InputStream getInputStream(JarInputStream jarInputStream, 
                                       JarEntry jarEntry) {
        InputStream is = null;
        try {
            JarEntry je = null;
            while ((je = jarInputStream.getNextJarEntry()) != null) {
                if (je.getName().equals(jarEntry.getName())) {
                    is = jarInputStream;
                    break;
                }
            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
        return is;
    }

    private String getEARFileName() {
        String earFileName = null;
        if (this.mEARFilePath != null) {
            File f = new File(this.mEARFilePath);
            earFileName = f.getName();
        } else {
            earFileName = this.mSuJarEntry.getName();
        }
        return earFileName;
    }
}
