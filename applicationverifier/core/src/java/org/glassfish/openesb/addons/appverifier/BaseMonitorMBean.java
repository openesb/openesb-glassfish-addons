/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common Development
 * and Distribution License("CDDL") (collectively, the "License").  You
 * may not use this file except in compliance with the License. You can obtain
 * a copy of the License at https://glassfish.dev.java.net/public/CDDL+GPL.html
 * or glassfish/bootstrap/legal/LICENSE.txt.  See the License for the specific
 * language governing permissions and limitations under the License.
 *
 * When distributing the software, include this License Header Notice in each
 * file and include the License file at glassfish/bootstrap/legal/LICENSE.txt.
 * Sun designates this particular file as subject to the "Classpath" exception
 * as provided by Sun in the GPL Version 2 section of the License file that
 * accompanied this code.  If applicable, add the following below the License
 * Header, with the fields enclosed by brackets [] replaced by your own
 * identifying information: "Portions Copyrighted [year]
 * [name of copyright owner]"
 *
 * Contributor(s):
 *
 * If you wish your version of this file to be governed by only the CDDL or
 * only the GPL Version 2, indicate your decision by adding "[Contributor]
 * elects to include this software in this distribution under the [CDDL or GPL
 * Version 2] license."  If you don't indicate a single choice of license, a
 * recipient has the option to distribute your version of this file under
 * either the CDDL, the GPL Version 2 or to extend the choice of license to
 * its licensees as provided above.  However, if you add GPL Version 2 code
 * and therefore, elected the GPL Version 2 license, then the option applies
 * only if the new code is made subject to such option by the copyright
 * holder.
 */


package org.glassfish.openesb.addons.appverifier;

import javax.management.AttributeList;
import javax.management.DynamicMBean;
import javax.management.InvalidAttributeValueException;
import javax.management.MBeanException;
import javax.management.ReflectionException;
import javax.management.AttributeNotFoundException;
import javax.management.MBeanInfo;
import javax.management.Attribute;
import javax.management.MBeanAttributeInfo;
import javax.management.MBeanConstructorInfo;
import javax.management.MBeanOperationInfo;
import javax.management.MBeanNotificationInfo;
import javax.management.MBeanRegistration;
import javax.management.ObjectName;
import javax.management.MBeanServer;
import javax.management.RuntimeOperationsException;
import javax.management.NotificationBroadcasterSupport;
import javax.management.Notification;
import javax.management.NotificationListener;
import javax.management.NotificationFilter;
import javax.management.ListenerNotFoundException;

import java.util.ArrayList;
import java.util.Iterator;


import java.util.logging.Logger;
import java.util.logging.Level;


/** The BaseMonitorMBean class provides the basic functionalities for buffering alerts if the
 * Event Management framework is not yet ready for receiving alerts.
 * Each <x>MBean (e.g CollabMonitorMBean) should inherits this base class.
 *
 *
 * @version 1.0
 */
public class BaseMonitorMBean extends NotificationBroadcasterSupport implements DynamicMBean, 
                                                                                MBeanRegistration {


    /**
     * MBean attributes
     */
    protected ArrayList mAttributesInfo = new ArrayList();

    /**
     * MBean constructors
     */
    protected ArrayList mConstructorsInfo = new ArrayList();

    /**
     * MBean operations
     */
    protected ArrayList mOperationsInfo = new ArrayList();

    /**
     * MBean notifications
     */
    protected ArrayList mNotificationsInfo = new ArrayList();

    /**
     * MBean info
     */
    protected MBeanInfo mMBeanInfo = null;

    /**
     * the MBeanServer
     */
    private MBeanServer mMBeanServer = null;

    private static Logger mLogger = 
        Logger.getLogger("org.glassfish.openesb.addons.appverifier" + "." + 
                         BaseMonitorMBean.class.getName());


    private int listenersCount = 0;
    private ArrayList notificationsList = new ArrayList();

    public static final String NOTIFICATION_ALERT = "Notification.Alert";


    /**
     * default constructor
     */
    public BaseMonitorMBean() {
        buildDefaultDynamicMBeanInfo();
    }


    /**
     * Build the protected MBeanInfo field,
     * which represents the management interface exposed by the MBean;
     * i.e., the set of attributes, constructors, operations and notifications
     * which are available for management.
     *
     * A reference to the MBeanInfo object is returned by the getMBeanInfo()
     * method of the DynamicMBean interface. Note that, once constructed,
     * an MBeanInfo object is immutable.
     */
    protected void buildDefaultDynamicMBeanInfo() {

        mConstructorsInfo.add(new MBeanConstructorInfo("BaseMonitorMBean Constructor", 
                                                       getClass().getConstructors()[0]));

        String[] notifTypes = { NOTIFICATION_ALERT };
        mNotificationsInfo.add(new MBeanNotificationInfo(notifTypes, "Alert", 
                                                         "Notification alerts type"));

        mMBeanInfo = 
                new MBeanInfo(getClass().getName(), "BaseMonitor DynamicMBean", 
                              (MBeanAttributeInfo[])mAttributesInfo.toArray(new MBeanAttributeInfo[mAttributesInfo.size()]), 
                              (MBeanConstructorInfo[])mConstructorsInfo.toArray(new MBeanConstructorInfo[mConstructorsInfo.size()]), 
                              (MBeanOperationInfo[])mOperationsInfo.toArray(new MBeanOperationInfo[mOperationsInfo.size()]), 
                              (MBeanNotificationInfo[])mNotificationsInfo.toArray(new MBeanNotificationInfo[mNotificationsInfo.size()]));

    }

    /**
     */
    public Object getAttribute(String aName) throws AttributeNotFoundException, 
                                                    MBeanException, 
                                                    ReflectionException {

        return new Object();
    }

    /**
     */
    public void setAttribute(Attribute aAttribute) throws AttributeNotFoundException, 
                                                          InvalidAttributeValueException, 
                                                          MBeanException, 
                                                          ReflectionException {
    }

    /**
     * Retrieves the value of specified attributes of the Dynamic MBean
     * @param aNames aNames of the attributes
     * @return AttributeList list of attribute aNames and values
     */
    public AttributeList getAttributes(String[] aNames) {
        AttributeList attributes = new AttributeList(aNames.length);
        return attributes;
    }

    /**
     * Sets the value of specified aAttributes of the Dynamic MBean.
     * @param aAttributes list of attribute names and values.
     * @return AttributeList list of attribute names and values
     */
    public AttributeList setAttributes(AttributeList aAttributes) {
        for (Iterator it = aAttributes.iterator(); it.hasNext(); ) {
            Attribute attribute = (Attribute)it.next();
            try {
                setAttribute(attribute);
            } catch (Exception e) {
                continue;
            }
        }
        return aAttributes;
    }


    /**
     * Invokes an operation on the Dynamic MBean.
     * @param operationName The name of the action to be invoked
     * @param params An array containing the parameters to be set when the
     * action is invoked
     * @param signature An array containing the aSignature of the action.
     * The class objects will be loaded through the same class loader as
     * the one used for loading the MBean on which the action is invoked.
     * @return The object returned by the action, which represents the result
     * of invoking the action on the MBean specified
     * @throws MBeanException MBeanException
     * @throws ReflectionException ReflectionException
     */
    public Object invoke(String operationName, Object[] params, 
                         String[] signature) throws MBeanException, 
                                                    ReflectionException {

        // Check operationName is not null to avoid NullPointerException later on
        if (operationName == null) {
            throw new RuntimeOperationsException(new IllegalArgumentException("Operation name cannot be null"), 
                                                 "Cannot invoke a null operation in " + 
                                                 getClass().getName());
        }
        return null;
    }


    /**
     */
    public MBeanInfo getMBeanInfo() {
        if (mMBeanInfo == null) {
            buildDefaultDynamicMBeanInfo();
            buildDynamicMBeanInfo();
        }
        return mMBeanInfo;
    }

    protected void buildDynamicMBeanInfo() {
    }


    public void postDeregister() {
    }

    public void postRegister(Boolean registrationDone) {
    }

    public void preDeregister() {
    }

    public ObjectName preRegister(MBeanServer server, ObjectName name) {
        mLogger.fine("preRegistering: mbeanserver=" + server.toString() + 
                     " objName=" + name);
        mMBeanServer = server;
        return name;
    }


    /**
     * intercepts the addNotificationListener and increment listeners counter
     */
    public void addNotificationListener(NotificationListener listener, 
                                        NotificationFilter filter, 
                                        java.lang.Object handback) {

        listenersCount++;
        // delegates it to superclass
        super.addNotificationListener(listener, filter, handback);

        // check to see if we have any buffered notifications, and sent them out if any
        mLogger.fine("Notification listner added, send buffered notifications");
        sendBufferedNotifications();
    }


    private void sendBufferedNotifications() {
        if (notificationsList.size() > 0) {
            for (int i = 0; i < notificationsList.size(); i++) {
                Notification n = (Notification)notificationsList.get(i);
                mLogger.fine("Send buffered notification out msg:" + n);
                super.sendNotification(n);
            }
            // clear out notifications cache
            notificationsList.clear();
        }
    }

    /**
     * intercepts the removeNotificationListener and decrement listeners counter
     */
    public void removeNotificationListener(NotificationListener listener) throws ListenerNotFoundException {
        listenersCount--;
        // delegates it to superclass
        super.removeNotificationListener(listener);
    }


    /**
     * send out a jmx notification. We need to do buffering if there's currently no
     * listeners.
     */
    public void sendNotification(Notification n) {

        // is there any listeners registered with us ???
        if (listenersCount < 1) {
            mLogger.fine("Buffering notification");
            // buffer it
            notificationsList.add(n);
        } else {
            mLogger.fine("Send notification out msg:" + n);
            // ok, just sent it along
            super.sendNotification(n);
        }
    }

}
