/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common Development
 * and Distribution License("CDDL") (collectively, the "License").  You
 * may not use this file except in compliance with the License. You can obtain
 * a copy of the License at https://glassfish.dev.java.net/public/CDDL+GPL.html
 * or glassfish/bootstrap/legal/LICENSE.txt.  See the License for the specific
 * language governing permissions and limitations under the License.
 *
 * When distributing the software, include this License Header Notice in each
 * file and include the License file at glassfish/bootstrap/legal/LICENSE.txt.
 * Sun designates this particular file as subject to the "Classpath" exception
 * as provided by Sun in the GPL Version 2 section of the License file that
 * accompanied this code.  If applicable, add the following below the License
 * Header, with the fields enclosed by brackets [] replaced by your own
 * identifying information: "Portions Copyrighted [year]
 * [name of copyright owner]"
 *
 * Contributor(s):
 *
 * If you wish your version of this file to be governed by only the CDDL or
 * only the GPL Version 2, indicate your decision by adding "[Contributor]
 * elects to include this software in this distribution under the [CDDL or GPL
 * Version 2] license."  If you don't indicate a single choice of license, a
 * recipient has the option to distribute your version of this file under
 * either the CDDL, the GPL Version 2 or to extend the choice of license to
 * its licensees as provided above.  However, if you add GPL Version 2 code
 * and therefore, elected the GPL Version 2 license, then the option applies
 * only if the new code is made subject to such option by the copyright
 * holder.
 */


package org.glassfish.openesb.addons.appverifier;

import org.glassfish.openesb.addons.appverifier.ejb.EJBInfo;
import org.glassfish.openesb.addons.appverifier.ejb.EJBReferenceInfo;

import org.glassfish.openesb.addons.appverifier.ejb.EjbReader;

import org.glassfish.openesb.addons.appverifier.javaee.ejb.annotations.AnnotationsReader;
import org.glassfish.openesb.addons.appverifier.javaee.ejb.annotations.EJBAnnotationsHandler;
import org.glassfish.openesb.addons.appverifier.javaee.ejb.annotations.mdb.MDBAnnotationHandler;
import org.glassfish.openesb.addons.appverifier.javaee.ejb.annotations.sessionorentity.StatefulSessionHandler;
import org.glassfish.openesb.addons.appverifier.javaee.ejb.annotations.sessionorentity.StatelessSessionHandler;
import org.glassfish.openesb.addons.appverifier.ra.InboundResourceAdapter;

import org.glassfish.openesb.addons.appverifier.ra.OutboundResourceAdapter;


import java.io.ByteArrayInputStream;
import java.io.File;

import java.io.InputStream;

import java.net.MalformedURLException;
import java.net.URL;

import java.net.URLClassLoader;

import java.net.URLDecoder;

import java.util.ArrayList;

import java.util.List;

import java.util.Map;

import java.util.logging.Logger;

import org.apache.tools.ant.AntClassLoader;
import org.apache.tools.ant.types.FileSet;
import org.apache.tools.ant.types.Path;

import org.netbeans.modules.classfile.ClassFile;

import org.netbeans.modules.classfile.ClassName;

/**
 * Utility for verifier.
 * @author Sreeni Genipudi
 */
public class ClassLoaderUtil {
    //Constants
    public static final String PATH_SEPARATOR = "path.separator";
    private static final String MDB_EJB_TYPE = "javax/ejb/MessageDriven";
    private static final String SFSB_EJB_TYPE = "javax/ejb/Stateful";
    private static final String STSB_EJB_TYPE = "javax/ejb/Stateless";
    public static final String CLASS_EXT = ".class";
    private static final String INVALID_CHARS = 
        " %+,.;@[]{}()*!&~`\"\'-#$:/\\";

    // Classloader utility instance
    private static ClassLoaderUtil mInstance = null;
    
    private static Logger logger = 
        Logger.getLogger("org.glassfish.openesb.addons.appverifier.ClassLoadeUtil");

    /**
     * Convert package "." to path notation
     * @param packagename
     * @return
     */
    public String convertPkgToPath(String packagename) {
        return convertPkgToPath(packagename, File.separator);
    }

    /**
     * Convert package "." to path notation
     * @param packagename
     * @return
     */
    public String convertPkgToResPath(String packagename) {
        return convertPkgToPath(packagename, "/");
    }

    // TODO turn it public

    private String convertPkgToPath(String packagename, String replaceToken) {
        String res;

        if ((packagename == null) || packagename.trim().equals("")) {
            res = "";
        } else if (packagename.indexOf(".") == -1) {
            res = packagename;
        } else {
            StringBuffer buffer = new StringBuffer();
            java.util.StringTokenizer stk = 
                new java.util.StringTokenizer(packagename, ".", true);

            while (stk.hasMoreTokens()) {
                String token = stk.nextToken();
                if (token.equals(".")) {
                    token = replaceToken;
                }

                buffer.append(token);
            }

            res = buffer.toString();
        }

        return res;
    }

    /**
     * Get the jar file from classloader    
     * @param classLoader
     * @param clazz
     * @return File instance of Jar file.
     */
    public File getJar(ClassLoader classLoader, String clazz) {

        if (!clazz.toLowerCase().endsWith(".xml") && 
            !clazz.toLowerCase().endsWith(".mf") && 
            !clazz.toLowerCase().endsWith(".jar")) {
            clazz = convertPkgToResPath(clazz) + CLASS_EXT;
            ;
        }
        // System.out.println(" LOOKING IN CLASSLOADER = "+clazz);
        URL url = classLoader.getResource(clazz);


        String urlString = url.toString();
        int pos = urlString.indexOf("file:/");
        int end = urlString.indexOf("!");
        int till = 6;
        if (pos == -1) {
            pos = urlString.indexOf("file:");
            till = 5;
        }
        urlString = urlString.substring(pos + till, end);
        try {
            urlString = URLDecoder.decode(urlString, "US-ASCII");
        } catch (Exception ex) {
            return null;
        }
        File urlStringFile = new File(urlString);
        if (!urlStringFile.exists()) {
            urlStringFile = new File(File.separator + urlStringFile);
        }
        return urlStringFile;

    }

    /**
     * Get the jar file from classloader    
     * @param classLoader
     * @param url
     * @return
     */
    public File getJar(ClassLoader classLoader, URL url) {

        String urlString = url.toString();
        try {
            urlString = URLDecoder.decode(urlString, "US-ASCII");
        } catch (Exception ex) {
            return null;
        }
        int pos = urlString.indexOf("file:/");
        int end = urlString.indexOf("!");
        int till = 6;
        if (pos == -1) {
            pos = urlString.indexOf("file:");
            till = 5;
        }
        urlString = urlString.substring(pos + till, end);
        File urlStringFile = new File(urlString);
        if (!urlStringFile.exists()) {
            urlStringFile = new File(File.separator + urlStringFile);
        }
        return urlStringFile;
    }

    /**
     * Get the jar file from classloader    
     * @param classLoader
     * @param clazz
     * @return
     */
    public File getJar(ClassLoader classLoader, Class clazz) {
        return getJar(classLoader, clazz.getName());
    }

    /**
     * Constructor
     */
    private ClassLoaderUtil() {
    }

    /**
     * Get instance
     * @return
     */
    public static ClassLoaderUtil getInstance() {
        if (mInstance == null) {
            mInstance = new ClassLoaderUtil();
        }
        return mInstance;
    }

    /**
     * Create classloader
     * @param archiveFile
     * @param destinationDir
     * @param filterList
     * @param extraCp
     * @return
     */
    public AntClassLoader createClassLoader(File archiveFile, 
                                            File destinationDir, 
                                            String[] filterList, 
                                            String extraCp) {
        AntClassLoader loader = new AntClassLoader();
        if (archiveFile != null) {
            FileUtil.getInstance().unzip(archiveFile, destinationDir);
        }

        org.apache.tools.ant.Project proj1 = 
            new org.apache.tools.ant.Project();
        proj1.init();

        Path p1 = new Path(proj1);
        FileSet fs = null;
        Path dirPath = null;

        fs = new FileSet();
        fs.setDir(destinationDir);
        dirPath = new Path(proj1);
        dirPath.setLocation(destinationDir);
        p1.add(dirPath);
        for (String filter: filterList) {
            fs.setIncludes(filter);
        }
        p1.addFileset(fs);
        if (extraCp != null) {
            dirPath = new Path(proj1);
            dirPath.setPath(extraCp);
            p1.add(dirPath);
        }

        loader.setClassPath(p1);
        return loader;

    }
    
    /**
     * Get the list of files.
     * @param directory
     * @param filterList
     * @return
     */
    public File[] getFiles(File directory, List<String> filterList) {

        org.apache.tools.ant.Project proj1 = 
            new org.apache.tools.ant.Project();
        proj1.init();

        Path p1 = new Path(proj1);
        FileSet fs = null;
        Path dirPath = null;

        fs = new FileSet();
        fs.setDir(directory);
        for (String filter: filterList) {
            fs.setIncludes(filter);
        }

        String[] fileNameList = 
            fs.getDirectoryScanner(proj1).getIncludedFiles();
        File[] fileList = new File[fileNameList.length];
        int index = 0;
        for (String fileName: fileNameList) {
            fileList[index++] = new File(fileName);
        }
        return fileList;

    }

    /**
     * Get resource
     * @param archive
     * @param resource
     * @return
     * @throws Exception
     */
    public URL getResource(File archive, String resource) throws Exception {
        URLClassLoader urlCll;
        urlCll = new URLClassLoader(new URL[] { archive.toURL() });
        return urlCll.findResource(resource);
    }

    /**
     * Get resource as file
     * @param archive
     * @param resource
     * @return
     * @throws Exception
     */
    public File getResourceAsFile(File archive, 
                                  String resource) throws Exception {
        File tempFile = 
            File.createTempFile(archive.getName() + resource.hashCode(), 
                                "RAR");
        if (tempFile.exists()) {
            tempFile.delete();
        }
        tempFile.mkdirs();
        FileUtil.getInstance().unzip(archive, tempFile);
        File resourceFile = new File(tempFile, resource);
        if (resourceFile.exists()) {
            return resourceFile;
        } else {
            return null;
        }
    }
    /**
     * Get EJB Reference information
     * @param ejbJarFile
     * @param mainDirectory
     * @return
     */
    public List<EJBReferenceInfo> getEJBReferenceInfo(File ejbJarFile, 
                                                      File mainDirectory) {
        //todo - implement.
        return null;
    }

    /**
     * Get EJB Information
     * @param classByteArry
     * @param ejbRdr
     * @param inRaList
     * @param outRaList
     * @param vc
     * @param eb
     * @param instance
     * @return
     * @throws Exception
     */
    public List<EJBInfo> getEJBInfo(List<byte[]> classByteArry, 
                                    EjbReader ejbRdr, 
                                    List<InboundResourceAdapter> inRaList, 
                                    List<OutboundResourceAdapter> outRaList, 
                                    ValidationConfig vc, ErrorBundle eb, 
                                    String instance) throws Exception {

        AnnotationsReader anReader = AnnotationsReader.getInstance();
        ClassName mdbClassName = ClassName.getClassName(this.MDB_EJB_TYPE);
        ClassName slsbClassName = ClassName.getClassName(this.STSB_EJB_TYPE);
        ClassName sfsbClassName = ClassName.getClassName(this.SFSB_EJB_TYPE);
        List<EJBInfo> listOfEJBInfo = new ArrayList<EJBInfo>();
        EJBAnnotationsHandler ejbAnnHndlr = null;
        String ejbFilePath = null;
        ClassFile ejbClassFile = null;
        EJBInfo ejbInfo = null;
        String ejbName = null;
        AnnotationsReader anr = AnnotationsReader.getInstance();
        for (byte[] classByte: classByteArry) {
            InputStream inStr = null;
            ejbAnnHndlr = null;
            inStr = new ByteArrayInputStream(classByte);
            ejbClassFile = new ClassFile(inStr);
            if (anReader.contains(ejbClassFile, mdbClassName)) {
                ejbAnnHndlr = new MDBAnnotationHandler();
            } else if (anReader.contains(ejbClassFile, sfsbClassName)) {
                ejbAnnHndlr = new StatefulSessionHandler();
            } else if (anReader.contains(ejbClassFile, slsbClassName)) {
                ejbAnnHndlr = new StatelessSessionHandler();
            }
            if (ejbAnnHndlr != null) {
                Map<String, Map<String, Object>> map = ejbAnnHndlr.createMap();
                String mdbAnnClassName = anr.getValue(ejbClassFile, map, eb);
                ejbName = ejbClassFile.getName().getExternalName();
                ejbInfo = 
                        ejbAnnHndlr.process(map, inRaList, outRaList, null, vc, 
                                            null, ejbName, ejbClassFile, eb, 
                                            instance);
                listOfEJBInfo.add(ejbInfo);
            }
        }
        return listOfEJBInfo;
    }

    private String formatForClassName(String classname) {
        int index = classname.lastIndexOf(".");
        if (index >= 0) {
            classname = classname.substring(0, index);
        }
        classname = classname.replace('\\', '/');
        return classname;

    }

    private EJBInfo createEJBInfo(Map<String, Map<String, Object>> map) {
        return null;
    }
}
