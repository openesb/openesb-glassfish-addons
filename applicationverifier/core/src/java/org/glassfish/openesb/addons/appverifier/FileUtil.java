/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common Development
 * and Distribution License("CDDL") (collectively, the "License").  You
 * may not use this file except in compliance with the License. You can obtain
 * a copy of the License at https://glassfish.dev.java.net/public/CDDL+GPL.html
 * or glassfish/bootstrap/legal/LICENSE.txt.  See the License for the specific
 * language governing permissions and limitations under the License.
 *
 * When distributing the software, include this License Header Notice in each
 * file and include the License file at glassfish/bootstrap/legal/LICENSE.txt.
 * Sun designates this particular file as subject to the "Classpath" exception
 * as provided by Sun in the GPL Version 2 section of the License file that
 * accompanied this code.  If applicable, add the following below the License
 * Header, with the fields enclosed by brackets [] replaced by your own
 * identifying information: "Portions Copyrighted [year]
 * [name of copyright owner]"
 *
 * Contributor(s):
 *
 * If you wish your version of this file to be governed by only the CDDL or
 * only the GPL Version 2, indicate your decision by adding "[Contributor]
 * elects to include this software in this distribution under the [CDDL or GPL
 * Version 2] license."  If you don't indicate a single choice of license, a
 * recipient has the option to distribute your version of this file under
 * either the CDDL, the GPL Version 2 or to extend the choice of license to
 * its licensees as provided above.  However, if you add GPL Version 2 code
 * and therefore, elected the GPL Version 2 license, then the option applies
 * only if the new code is made subject to such option by the copyright
 * holder.
 */


package org.glassfish.openesb.addons.appverifier;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;

import java.io.FilenameFilter;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;

import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.jar.JarInputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;
import java.util.zip.ZipFile;

import org.apache.tools.ant.Project;
import org.apache.tools.ant.Target;
import org.apache.tools.ant.taskdefs.Delete;
import org.apache.tools.ant.taskdefs.Expand;
import org.apache.tools.ant.types.FileSet;
import org.apache.tools.ant.types.Path;

import org.netbeans.modules.classfile.AnnotationComponent;
import org.netbeans.modules.classfile.ClassFile;

/**
 *
 * @author Sreeni Genipudi
 */
public class FileUtil {
    private static FileUtil mInstance = null;

    private FileUtil() {
    }

    public static FileUtil getInstance() {
        if (mInstance == null) {
            mInstance = new FileUtil();
        }
        return mInstance;
    }

    public File[] getDirectories(File rootDir, int level) {
        if (rootDir.isDirectory()) {
            List<File> listOfFiles = new ArrayList<File>();
            getDirectories(listOfFiles, rootDir, level, 0);
            return listOfFiles.toArray(new File[0]);
        }
        return null;
    }

    private void getDirectories(List<File> listOfFiles, File directory, 
                                int maxLevel, int level) {
        if (directory.isDirectory()) {
            if (level <= maxLevel) {
                listOfFiles.add(directory);
                level++;
                File[] subDirList = directory.listFiles();
                for (File subDirFile: subDirList) {
                    getDirectories(listOfFiles, subDirFile, maxLevel, level);
                }
            }
        }
    }

    public File[] getClasses(File directory) {
        org.apache.tools.ant.Project proj1 = 
            new org.apache.tools.ant.Project();
        proj1.init();

        Path p1 = new Path(proj1);
        FileSet fs = null;
        fs = new FileSet();
        fs.setDir(directory);
        fs.setIncludes("**/*.class");
        String[] listOffileNames = 
            fs.getDirectoryScanner(proj1).getIncludedFiles();
        File[] listOfFiles = new File[listOffileNames.length];
        int index = 0;
        for (String fileName: listOffileNames) {
            listOfFiles[index++] = new File(fileName);
        }
        return listOfFiles;

    }


    public File createTempDirectory() throws Exception {
        File tempFIle = File.createTempFile("EAR", "DIRECTORY");
        File tempDir = new File(tempFIle.getParent(), "EARFILE");
        if (tempDir.exists()) {
            org.apache.tools.ant.Project proj1 = 
                new org.apache.tools.ant.Project();
            proj1.init();
            Delete delTask = new Delete();
            delTask.setProject(proj1);
            delTask.setDir(tempDir);
            delTask.execute();
        }
        tempDir.mkdirs();
        return tempDir;
    }

    /**
     * A utility based on Ant tasks to unzip any jar based file (zip, rar, etc.)
     *
     * @param zipFilePath The directory path to the jar file
     * @param targetDir The name of directory where the the jar will be extracted
     *
     */
    public void unzip(File zipFilePath, File targetDir) {

        final class MyUnzip extends Expand {
            public MyUnzip() {
                setProject(new Project());
                getProject().init();
                setTaskType("unzip");
                setTaskName("unzip");
                setOwningTarget(new Target());
            }
        }

        MyUnzip expander = new MyUnzip();
        expander.setSrc(zipFilePath);
        expander.setDest(targetDir);
        expander.execute();
    }


    public List<ResourceAdapterInfo> getResourceAdapterInfo(File mainDirectory, 
                                                            String raDescriptor, 
                                                            String sunRADescriptor) throws Exception {
        File[] listOfFiles = mainDirectory.listFiles();
        File tempFile = null;
        List<ResourceAdapterInfo> matchingFiles = null;
        ResourceAdapterInfo raInfo = null;
        File sunRAXML = null;
        File destinationDirectory = null;
        for (File file: listOfFiles) {
            if (!file.isDirectory() && file.getName().endsWith(".jar")) {
                destinationDirectory = 
                        new File(file.getAbsolutePath() + ApplicationVerifierMain.FOLDER);
                destinationDirectory.mkdirs();
                this.unzip(file, destinationDirectory);
                tempFile = new File(destinationDirectory, raDescriptor);
                if (tempFile.exists()) {
                    raInfo = new ResourceAdapterInfo();

                    raInfo.setRAXML(tempFile);
                    raInfo.setSunArchiveRA(file);
                    sunRAXML = new File(file, sunRADescriptor);
                    if (sunRAXML.exists()) {
                        raInfo.setSunRAXML(sunRAXML);
                    }
                    if (matchingFiles == null) {
                        matchingFiles = new ArrayList<ResourceAdapterInfo>();
                    }
                    matchingFiles.add(raInfo);
                }
            }
        }

        return matchingFiles;
    }


    String getString(InputStream is) throws IOException {
        BufferedReader in = new BufferedReader(new InputStreamReader(is));

        StringBuffer buffer = new StringBuffer();
        String line = null;
        while ((line = in.readLine()) != null) {
            buffer.append(line);
        }
        return buffer.toString();
    }


    public void extractFile() throws ZipException, IOException {
        File file = 
            new File("C:\\ICAN513\\ican513_20070815-1804_Thu_REL\\edesigner\\builds\\Deployment2Project1\\LogicalHost1\\SunJavaSystemApplicationServer1\\Deployment2Project1.ear");
        ZipFile zipFile = new ZipFile(file);
        Enumeration<? extends ZipEntry> enumeration = zipFile.entries();
        while (enumeration.hasMoreElements()) {
            ZipEntry zipEntry = (ZipEntry)enumeration.nextElement();
            String zipFileName = zipEntry.getName();
            System.out.println("zip file name = " + zipFileName);
        }
    }

    /**
     * Read the input stream and retrieve Application.xml if found else return 
     * null
     * @param jis JarInput stream for Service unit.
     * @return Application.xml as String if found else null
     * @throws IOException
     */
    public String getApplicationXML(JarInputStream jis) throws IOException {
        String appXML = null;
        ZipEntry zipEntry = null;
        String zipFileName = null;

        while ((zipEntry = jis.getNextEntry()) != null) {
            zipFileName = zipEntry.getName();
            if (zipFileName.endsWith("/application.xml")) {
                try {
                    appXML = getString(jis);
                    break;
                } catch (IOException e) {
                    // TODO
                    e.printStackTrace();
                }
            }
        }
        return appXML;
    }

    /**
     * Reads input stream tries to match the input pattern and collects them in 
     * List of byte array.
     * @param is Input stream
     * @param desc matching pattern.
     * @return List of byte array.
     * @throws IOException
     */
    public List<byte[]> readBytes(InputStream is, 
                                  String desc) throws IOException {
        return this.readBytes(is, desc, false);
    }

    /**
     * Reads input stream tries to match the input pattern and collects them in 
     * List of byte array.
     * @param is Input stream
     * @param desc matching pattern.
     * @param isSingleFile if the inputstream contains one single non archive file.
     * @return List of byte array.
     * @throws IOException
     */
    public List<byte[]> readBytes(InputStream is, String desc, 
                                  boolean isSingleFile) throws IOException {
        JarInputStream jis = new JarInputStream(is);
        JarEntry je = null;
        ArrayList<byte[]> listOfByteArray = new ArrayList<byte[]>();
        if (!isSingleFile) {
            while ((je = jis.getNextJarEntry()) != null) {
                if (je.getName().endsWith(desc)) {
                    listOfByteArray.add(convert(jis));
                }
            }
        } else {
            listOfByteArray.add(convert(is));
        }
        return listOfByteArray;

    }

    public List<byte[]> readBytes(JarFile jf, String desc) throws IOException {

        ArrayList<byte[]> listOfByteArray = new ArrayList<byte[]>();
        Enumeration<JarEntry> enu = jf.entries();
        InputStream is = null;
        String jarEntryType = null;
        while (enu.hasMoreElements()) {
            JarEntry je = enu.nextElement();
            jarEntryType = je.getName();
            if (jarEntryType.endsWith(desc)) {
                is = jf.getInputStream(je);
                listOfByteArray.add(convert(is));
            }
        }
        return listOfByteArray;

    }


    public List<byte[]> readAllBytes(JarInputStream jIs, 
                                     String desc) throws IOException {

        ArrayList<byte[]> listOfByteArray = new ArrayList<byte[]>();

        //Enumeration<JarEntry> enu = jf.entries();
        JarEntry je = null;
        InputStream is = null;
        String jarEntryType = null;
        while ((je = jIs.getNextJarEntry()) != null) {
            jarEntryType = je.getName();
            if (jarEntryType.endsWith(desc)) {
                is = jIs;
                listOfByteArray.add(convert(is));
            }
        }
        return listOfByteArray;

    }

    public String[] readData(InputStream is, String desc) throws IOException {
        ArrayList<String> listOfData = new ArrayList<String>();
        List<byte[]> byteList = this.readBytes(is, desc);
        for (Iterator itr = byteList.iterator(); itr.hasNext(); ) {
            byte[] byteArray = (byte[])itr.next();
            listOfData.add(new String(byteArray));
        }
        return listOfData.toArray(new String[0]);

    }

    public byte[] convert(InputStream is) {
        byte[] byArr = new byte[1];
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        try {
            while (is.read(byArr) != -1) {
                bos.write(byArr);
            }
            bos.flush();
            byArr = bos.toByteArray();
            bos.close();
        } catch (IOException e) {
            // TODO
        }

        return byArr;
    }


    public InputStream getInputStream(JarInputStream jis, 
                                      String ra) throws IOException {
        JarEntry je = null;
        while ((je = jis.getNextJarEntry()) != null) {
            if (je.getName().endsWith(ra)) {
                return jis;
            }
        }
        return null;
    }
}
