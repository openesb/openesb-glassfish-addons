/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common Development
 * and Distribution License("CDDL") (collectively, the "License").  You
 * may not use this file except in compliance with the License. You can obtain
 * a copy of the License at https://glassfish.dev.java.net/public/CDDL+GPL.html
 * or glassfish/bootstrap/legal/LICENSE.txt.  See the License for the specific
 * language governing permissions and limitations under the License.
 *
 * When distributing the software, include this License Header Notice in each
 * file and include the License file at glassfish/bootstrap/legal/LICENSE.txt.
 * Sun designates this particular file as subject to the "Classpath" exception
 * as provided by Sun in the GPL Version 2 section of the License file that
 * accompanied this code.  If applicable, add the following below the License
 * Header, with the fields enclosed by brackets [] replaced by your own
 * identifying information: "Portions Copyrighted [year]
 * [name of copyright owner]"
 *
 * Contributor(s):
 *
 * If you wish your version of this file to be governed by only the CDDL or
 * only the GPL Version 2, indicate your decision by adding "[Contributor]
 * elects to include this software in this distribution under the [CDDL or GPL
 * Version 2] license."  If you don't indicate a single choice of license, a
 * recipient has the option to distribute your version of this file under
 * either the CDDL, the GPL Version 2 or to extend the choice of license to
 * its licensees as provided above.  However, if you add GPL Version 2 code
 * and therefore, elected the GPL Version 2 license, then the option applies
 * only if the new code is made subject to such option by the copyright
 * holder.
 */


package org.glassfish.openesb.addons.appverifier;


import org.glassfish.openesb.addons.appconfig.Verifier;
import org.glassfish.openesb.addons.appconfig.VerificationResult;
import org.glassfish.openesb.addons.appconfig.VerifierImpl;
import org.glassfish.openesb.addons.appverifier.ejb.EJBInfo;

import org.glassfish.openesb.addons.appverifier.ejb.EJBReferenceInfo;
import org.glassfish.openesb.addons.appverifier.ejb.MDBInfo;
import org.glassfish.openesb.addons.appverifier.ejb.ResourceInfo;
import org.glassfish.openesb.addons.appverifier.ejb.SessionBeanInfo;

import java.io.File;

import java.io.FilenameFilter;

import java.lang.reflect.Constructor;

import java.net.InetAddress;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.Vector;
import java.util.jar.JarFile;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.management.Attribute;

import javax.management.AttributeChangeNotification;
import javax.management.AttributeList;
import javax.management.AttributeNotFoundException;
import javax.management.InstanceAlreadyExistsException;
import javax.management.InstanceNotFoundException;
import javax.management.InvalidAttributeValueException;
import javax.management.MBeanAttributeInfo;
import javax.management.MBeanConstructorInfo;
import javax.management.MBeanException;
import javax.management.MBeanInfo;
import javax.management.MBeanNotificationInfo;
import javax.management.MBeanOperationInfo;
import javax.management.MBeanParameterInfo;
import javax.management.MBeanRegistration;
import javax.management.MBeanRegistrationException;
import javax.management.MBeanServer;
import javax.management.MBeanServerFactory;
import javax.management.MalformedObjectNameException;
import javax.management.NotCompliantMBeanException;
import javax.management.Notification;
import javax.management.NotificationListener;
import javax.management.ObjectName;
import javax.management.ReflectionException;
import javax.management.RuntimeOperationsException;
import javax.management.openmbean.CompositeData;
import javax.management.openmbean.CompositeDataSupport;
import javax.management.openmbean.CompositeType;
import javax.management.openmbean.OpenDataException;
import javax.management.openmbean.SimpleType;
import javax.management.openmbean.TabularData;
import javax.management.openmbean.TabularDataSupport;
import javax.management.openmbean.TabularType;

/**
 *  MBean Implementation for Verifier.
 *  @author Sreenivasan Genipudi
 */
public class JavaEEVerifierMonitor extends BaseMonitorMBean implements JavaEEVerifierMBean, 
                                                                       MBeanRegistration, 
                                                                       NotificationListener {

    /*
     *  VERIFY STATUS - OK
     */
    public static final int OK = 0;
    /*
     *  VERIFY STATUS - WARNING
     */
    public static final int WARNING = 0;
    /*
     *  VERIFY STATUS - ERROR
     */
    public static final int ERROR = 0;
    /*
     *  Logger
     */
    private Logger logger = Logger.getLogger(getClass().getName());

    //MBean Server instance
    private MBeanServer mbeanServer;
    //Object Name
    private ObjectName mobjectName = null;
    //empty attribute list
    private static final AttributeList mAttrList = new AttributeList();
    private ResourceBundle mRb = null;

    private static final String TRANSFORM_LDAP_URL_ERROR = 
        "TRANSFORM_LDAP_URL_ERROR";
    private static final String OP_NAME_CANNOT_NULL = "OP_NAME_CANNOT_NULL";

    private static final String REF_CLASS = "_referencerclass";
    private static final String REF_BY = "_referencer";
    private static final String JNDI_NAME = "_jndiname";


    private static final String EAR_FILE_NAME = "_filename";
    private static final String JNDI_CLASS_TYPE = "_jndinameClass";
    private static final String MESSAGE = "_message";
    private static final String STATUS = "_status";


    private static final String DISP_REF_CLASS = "Disp_Referrence_Class";
    private static final String DISP_REF_BY = "Disp_Referrence_By";
    private static final String DISP_JNDI_NAME = "Disp_JNDI_Name";


    private static final String DISP_EAR_FILE_NAME = "Disp_Ear_Filename";
    private static final String DISP_JNDI_CLASS_TYPE = "Disp_JNDI_Class_Type";
    private static final String DISP_MESSAGE = "Disp_Message";
    private static final String DISP_STATUS = "Disp_Status";

    private static final String WARNING_STATUS = "WARNING_STATUS";
    private static final String ERROR_STATUS = "ERROR_STATUS";
    private static final String OK_STATUS = "OK_STATUS";

    private static final String PATH_TO_SU_FOLDER = "PATH_TO_SU_FOLDER";
    private static final String TARGET_SRVR_NAME = "TARGET_SRVR_NAME";
    private static final String CANNOT_INVK_NULL_OPN = "CANNOT_INVK_NULL_OPN";
    private static final String CANNOT_ACCEPT_NULL = "CANNOT_ACCEPT_NULL";
    private static final String EXPECT_TWO_PARAM = "EXPECT_TWO_PARAM";
    private static final String INVALID_OPN = "INVALID_OPN";
    private static final String CANNOT_INVOKE = "CANNOT_INVOKE";

    private static final String EAR_FILE_NAME_MSG = "EAR_FILE_NAME_MSG";
    private static final String COMP_REFER_JNDI = "COMP_REFER_JNDI";
    private static final String CLASS_REFER_JNDI = "CLASS_REFER_JNDI";
    private static final String JNDI_NAME_MSG = "JNDI_NAME_MSG";
    private static final String JNDI_CLASS_TYPE_MSG = "JNDI_CLASS_TYPE_MSG";
    private static final String MESSAGE_MSG = "MESSAGE_MSG";
    private static final String STATUS_MSG = "STATUS_MSG";
    private static final String WARNING_MSG = "WARNING_MSG";
    private static final String OK_MSG = "OK_MSG";
    private static final String PATH_TO_SA_JAR = "PATH_TO_SA_JAR";
    private static final String ZIPFILEENTRY_SU = "ZIPFILEENTRY_SU";

    /** Creates a new instance of ConfigurationMonitor */
    public JavaEEVerifierMonitor() throws Exception {
        mRb = 
ResourceBundle.getBundle("org.glassfish.openesb.addons.appverifier.Bundle", 
                         Locale.getDefault());

        buildDynamicMBeanInfo();
        //   mbeanServer =

        ArrayList mbeanServers = MBeanServerFactory.findMBeanServer(null);
        if (mbeanServers.size() > 0) {
            // get the first mbean server instance
            mbeanServer = (MBeanServer)mbeanServers.get(0);
        }

    }


    /** Creates a new instance of ConfigurationMonitor */
    public JavaEEVerifierMonitor(MBeanServer mbeanSrvr) throws Exception {
        mRb = 
ResourceBundle.getBundle("org.glassfish.openesb.addons.appverifier.Bundle", 
                         Locale.getDefault());
        buildDynamicMBeanInfo();
        //   mbeanServer =

        mbeanServer = mbeanSrvr;


    }

    /**
     * Build the protected MBeanInfo field,
     * which represents the management interface exposed by the MBean;
     * i.e., the set of attributes, constructors, operations and notifications
     * which are available for management.
     *
     * A reference to the MBeanInfo object is returned by the getMBeanInfo()
     * method of the DynamicMBean interface. Note that, once constructed,
     * an MBeanInfo object is immutable.
     */
    protected void buildDynamicMBeanInfo() {
        // first clear out base class contructors
        mConstructorsInfo.clear();

        mConstructorsInfo.add(new MBeanConstructorInfo("JavaEEVerifier Constructor", 
                                                       getClass().getConstructors()[0]));
        mOperationsInfo.add(new MBeanOperationInfo("verifyServiceUnit", 
                                                   mRb.getString(TRANSFORM_LDAP_URL_ERROR), 
                                                   new MBeanParameterInfo[] { new MBeanParameterInfo("serviceAssemblyFileName", 
                                                                                                     "java.lang.String", 
                                                                                                     mRb.getString(PATH_TO_SA_JAR)), 
                                                                              new MBeanParameterInfo("ZipEntryFileName", 
                                                                                                     "java.lang.String", 
                                                                                                     mRb.getString(ZIPFILEENTRY_SU)), 
                                                                              new MBeanParameterInfo("target", 
                                                                                                     "java.lang.String", 
                                                                                                     mRb.getString(TARGET_SRVR_NAME)) }, 
                                                   "java.lang.String", 
                                                   MBeanOperationInfo.ACTION));

        mMBeanInfo = 
                new MBeanInfo(getClass().getName(), getClass().getName() + " DynamicMBean", 
                              null, 
                              (MBeanConstructorInfo[])mConstructorsInfo.toArray(new MBeanConstructorInfo[mConstructorsInfo.size()]), 
                              (MBeanOperationInfo[])mOperationsInfo.toArray(new MBeanOperationInfo[mOperationsInfo.size()]), 
                              (MBeanNotificationInfo[])mNotificationsInfo.toArray(new MBeanNotificationInfo[mNotificationsInfo.size()]));
    }

    public Object invoke(String operationName, Object[] params, 
                         String[] signature) throws MBeanException, 
                                                    ReflectionException {

        // Check operationName is not null to avoid NullPointerException later on
        try {
            logger.fine("operation name:" + operationName);

            if (operationName == null) {
                throw new RuntimeOperationsException(new IllegalArgumentException(mRb.getString(OP_NAME_CANNOT_NULL)), 
                                                     mRb.getString(CANNOT_INVK_NULL_OPN) + 
                                                     getClass().getName());
            } else if (operationName.equalsIgnoreCase("verifyServiceUnit")) {
                if (params == null) {
                    throw new RuntimeOperationsException(new IllegalArgumentException("verifyServiceUnit " + 
                                                                                      mRb.getString(CANNOT_ACCEPT_NULL)), 
                                                         getClass().getName());
                }
                if (params.length != 3) {
                    throw new RuntimeOperationsException(new IllegalArgumentException("verifyServiceUnit " + 
                                                                                      mRb.getString(EXPECT_TWO_PARAM) + 
                                                                                      params.length), 
                                                         getClass().getName());

                }
                return verifyServiceUnit((String)params[0], (String)params[1], 
                                         (String)params[2]);
            } else {
                logger.severe(mRb.getString(INVALID_OPN) + operationName + 
                              mRb.getString(CANNOT_INVOKE));
                return null;
            }
        } catch (Exception e) {
            logger.severe(e.toString());
        }
        return null;
    }

    public AttributeList getAttributes(String[] aNames) {
        return mAttrList;
    }

    public Object getAttribute(String aName) throws AttributeNotFoundException, 
                                                    MBeanException, 
                                                    ReflectionException {
        return null;
    }

    public void setAttribute(Attribute attribute) throws AttributeNotFoundException, 
                                                         InvalidAttributeValueException, 
                                                         MBeanException, 
                                                         ReflectionException {
    }

    public void handleNotification(javax.management.Notification aNotification, 
                                   Object obj) {
        try {
            if (aNotification instanceof AttributeChangeNotification) {
                AttributeChangeNotification changeNotification = 
                    (AttributeChangeNotification)aNotification;
                logger.fine("Received AttributeChange notification " + 
                            changeNotification.toString() + " new value:" + 
                            changeNotification.getNewValue().toString());
                /*
              if (changeNotification.getNewValue().toString().equals("0")){
                  // when the application ear file is removed from the IS
                  // MbeanServer, its state changes to 0
                  // we remove this monitor mbean
                  if (mbeanServer.isRegistered(mobjectName)) {
                      logger.fine("Container removed. Removing "
                                   + mobjectName.getCanonicalName());
                      mbeanServer.unregisterMBean(mobjectName);
                  }
              }
              */
            }
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception thrown:", ex);
        }
    }
    // Methods from MBeanRegistration interface

    /**
     * <code>MBeanRegistration</code> implementation method
     * @param aServer <code>MBeanServer</code>
     * @param aName MBean's <code>ObjectName</code>
     * @return MBean's <code>ObjectName</code>
     * @throws Exception  This exception should be caught by the MBean server
     * and re-thrown as an <code>MBeanRegistrationException</code>
     */
    public ObjectName preRegister(MBeanServer aServer, ObjectName aName) {
        this.mbeanServer = aServer;
        this.mobjectName = aName;

        return aName;
    }

    /**
     * <code>MBeanRegistration</code> implementation method
     * @param registrationDone Indicates whether or not the MBean has been
     * successfully registered in the MBean server.
     * The value false means that the registration phase has failed
     */
    public void postRegister(Boolean registrationDone) {
        try {
            logger.fine("adding notification listener to " + 
                        mobjectName.getCanonicalName());
            this.mbeanServer.addNotificationListener(mobjectName, this, null, 
                                                     null);
        } catch (Exception ex) {
            logger.log(Level.SEVERE, 
                       "Exception encountered trying to add and register mirror MBeans. ", 
                       ex);
        }
    }

    /***
     *  Verify  a service unit targeted to the JavaEE SE. This operation verifies the javaee/j2ee
     *  application and returns a CompositeData which has the verification status i.e. information
     *  related to the resources referenced by the application but are missing.
     *
     *  @param serviceAssemblyFileName - Service assembly file name.
     *  @param ZipEntryFileName - Zip Entry file name for Service unit.  The other possibility is this
     *          can be an EAR file path or EJB Jar file path, if so serviceAssemblyFileName should be null.
     *  @param target - identifies the target for the service unit verification. This could
     *                           be the DAS server instance "server", a standalone or clustered instance name
     *                           or cluster.
     *  @return a TabularData with the verification result. The CompositeType of TabularData
     *       The column names in composite data are - "Ear Filename", "Referrence By","Referrence Class", "JNDI Name", "JNDI Class Type", "Message", "Status"
     *
     *       Column names:                        SimpleType.STRING
     *       Ear Filename     *       Referrence By
     *       Referrence Class
     *       JNDI Name
     *       JNDI Class Type           *       Message
     *
     *       Column name:             *           Status                         SimpleType.INTEGER
     *      Key - { "Referrence Class","Referrence By","JNDI Name"}
     */
    public TabularData verifyServiceUnit(String serviceAssemblyFileName, 
                                         String ZipEntryFileName, 
                                         String target) throws MBeanException {
        ErrorBundle eb = new ErrorBundle();
        ValidationConfig cfg = new ValidationConfig();
        RAHelper.getInstance(cfg, eb, this.mbeanServer, target);
        JarFile saJarFile = null;
        try {
            if (serviceAssemblyFileName != null && 
                (!serviceAssemblyFileName.trim().equals(""))) {
                saJarFile = new JarFile(serviceAssemblyFileName);
            }
        } catch (Exception e) {
            throw new MBeanException(e);
        }

        File[] earFile = null;

        List<EJBInfo> ejbListColl = new ArrayList<EJBInfo>();
        List<VerificationResult> vRes = new ArrayList<VerificationResult>();
        ApplicationVerifierMain appMain = null;
        List<EJBInfo> ejbInfoList = null;
        List<VerificationResult> classicVerifierResult = null;

        try {

            if (serviceAssemblyFileName != null && 
                (!serviceAssemblyFileName.trim().equals(""))) {
                appMain = 
                        new ApplicationVerifierMain(saJarFile, ZipEntryFileName, 
                                                    target, eb, cfg);
            } else {
                appMain = 
                        new ApplicationVerifierMain(saJarFile, ZipEntryFileName, 
                                                    target, eb, cfg);
            }
            ejbInfoList = appMain.getEJBs();
            if (ejbInfoList != null && ejbInfoList.size() > 0) {
                ejbListColl.addAll(ejbInfoList);
            }
            classicVerifierResult = appMain.getClassicVerifierResults();
            if (classicVerifierResult != null && 
                classicVerifierResult.size() > 0) {
                vRes.addAll(classicVerifierResult);
            }
            return this.createTabData(ZipEntryFileName, ejbListColl, vRes);
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "VerifySU", ex);
            throw new MBeanException(ex);
        }

        // return createSuccessMessage(ZipEntryFileName, target);
    }


    private CompositeData createCompositeData(File earFileName, 
                                              List<EJBInfo> ejbInfoList) throws OpenDataException {

        int numberOfEJBs = ejbInfoList.size();
        int refSize = 0;
        for (EJBInfo ejbInf: ejbInfoList) {
            refSize = refSize + ejbInf.getResourceReferenceInfo().size();
        }
        int maxSpan = 7;
        int maxSize = 
            maxSpan * (numberOfEJBs == 0 ? 1 : numberOfEJBs) * (refSize == 0 ? 
                                                                1 : refSize);


        String[] names = new String[maxSize];
        SimpleType[] types = new SimpleType[maxSize];
        String[] descr = new String[maxSize];
        Object[] values = new Object[maxSize];

        int j = 0;
        int ejbIndex = 0;
        int resIndex = 0;
        EJBInfo ejbInf = null;
        ResourceInfo resRef = null;
        boolean bContinue = true;
        while (j < maxSize) {
            List<ResourceInfo> resList = null;
            if (ejbInfoList.size() > 0) {
                ejbInf = ejbInfoList.get(ejbIndex);
                resList = ejbInf.getResourceReferenceInfo();
            }
            String earFilePath = earFileName.getName();
            resIndex = 0;
            resRef = null;
            bContinue = true;

            while (bContinue) {
                names[j] = this.EAR_FILE_NAME;
                types[j] = SimpleType.STRING;
                descr[j] = mRb.getString(EAR_FILE_NAME_MSG);
                values[j++] = earFilePath;

                names[j] = this.REF_BY;
                types[j] = SimpleType.STRING;
                descr[j] = mRb.getString(COMP_REFER_JNDI);
                if (ejbInf != null) {
                    values[j++] = ejbInf.getEJBName();
                } else {
                    values[j++] = "";
                }

                names[j] = this.REF_CLASS;
                types[j] = SimpleType.STRING;
                descr[j] = mRb.getString(CLASS_REFER_JNDI);
                if (ejbInf != null) {
                    values[j++] = ejbInf.getEJBClassName();
                } else {
                    values[j++] = "";
                }

                if (resList != null && resList.size() > 0) {
                    if (resIndex < resList.size()) {
                        resRef = resList.get(resIndex);
                    } else {
                        bContinue = false;
                        continue;
                    }
                }


                names[j] = this.JNDI_NAME;
                types[j] = SimpleType.STRING;
                descr[j] = mRb.getString(JNDI_NAME_MSG);
                if (resRef != null) {
                    values[j++] = resRef.getResJNDIName();
                } else {
                    values[j++] = "";
                }

                names[j] = this.JNDI_CLASS_TYPE;
                types[j] = SimpleType.STRING;
                descr[j] = mRb.getString(JNDI_CLASS_TYPE_MSG);
                if (resRef != null) {
                    values[j++] = resRef.getResourceType();
                } else {
                    values[j++] = "";
                }

                names[j] = this.MESSAGE;
                types[j] = SimpleType.STRING;
                descr[j] = mRb.getString(MESSAGE_MSG);
                if (resRef != null) {
                    values[j++] = resRef.getMessage();
                } else {
                    values[j++] = "";
                }

                names[j] = this.STATUS;
                types[j] = SimpleType.INTEGER;
                descr[j] = mRb.getString(STATUS_MSG);
                if (resRef != null) {
                    if (resRef.getStatus() == VerifyStatus.OK) {
                        values[j++] = 0;
                    } else if (resRef.getStatus() == VerifyStatus.WARNING) {
                        values[j++] = 1;
                    } else {
                        values[j++] = 2;
                    }
                } else {
                    values[j++] = 2;
                }
                if (resRef == null) {
                    bContinue = false;
                }
            }
        }

        CompositeType typex;
        typex = 
                new CompositeType("Application verifier", "Application verifier", 
                                  names, descr, types);
        CompositeData ret = new CompositeDataSupport(typex, names, values);
        return ret;

    }

    private TabularData createSuccessMessage(String serviceUnitPath, 
                                             String target) {
        TabularData t = null;
        CompositeData ret = null;
        try {
            String[] names = 
                new String[] { this.EAR_FILE_NAME, this.REF_BY, this.REF_CLASS, 
                               this.JNDI_NAME, this.JNDI_CLASS_TYPE, 
                               this.MESSAGE, this.STATUS };
            SimpleType[] types = 
                new SimpleType[] { SimpleType.STRING, SimpleType.STRING, 
                                   SimpleType.STRING, SimpleType.STRING, 
                                   SimpleType.STRING, SimpleType.STRING, 
                                   SimpleType.INTEGER };
            String[] descrs = 
                new String[] { mRb.getString(EAR_FILE_NAME_MSG), mRb.getString(COMP_REFER_JNDI), 
                               mRb.getString(CLASS_REFER_JNDI), 
                               mRb.getString(JNDI_NAME_MSG), 
                               mRb.getString(JNDI_CLASS_TYPE_MSG), 
                               mRb.getString(MESSAGE_MSG), 
                               mRb.getString(STATUS_MSG) };
            CompositeType type = 
                new CompositeType("Application verifier", "Application verifier", 
                                  names, descrs, types);
            Object[] values = 
                new Object[] { serviceUnitPath, "", "", "", "", "", this.OK };
            ret = new CompositeDataSupport(type, names, values);
            TabularType tabletype = 
                new TabularType("Application verifier", "Application verifier", 
                                type, 
                                new String[] { "Referrence Class", "Referrence By", 
                                               "JNDI Name" });

            t = new TabularDataSupport(tabletype);
            t.put(ret);
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "createSuccessMessage()", ex);
        }

        return t;
    }
    
    /**
     * Create tabular data
     * @param earFileName
     * @param ejbInfoList
     * @param vResultList
     * @return tabular data
     * @throws OpenDataException
     */
    private TabularData createTabData(String earFileName, 
                                      List<EJBInfo> ejbInfoList, 
                                      List<VerificationResult> vResultList) throws OpenDataException {

        int numberOfEJBs = ejbInfoList.size();
        int refSize = 0;
        List<ResourceInfo> ejbInfResRefInfo = null;
        for (EJBInfo ejbInf: ejbInfoList) {
            ejbInfResRefInfo = ejbInf.getResourceReferenceInfo();
            if (ejbInfResRefInfo != null) {
                refSize = refSize + ejbInfResRefInfo.size();
            }
        }
        int maxSize = (numberOfEJBs == 0 ? 1 : numberOfEJBs);
        String descrTab = "Application Verifier";
        TabularData tab = null;
        CompositeData ret = null;
        List<CompositeData> listComp = new ArrayList<CompositeData>();
        String[] names =

            new String[] { this.EAR_FILE_NAME, this.REF_BY, this.REF_CLASS, 
                           this.JNDI_NAME, this.JNDI_CLASS_TYPE, this.MESSAGE, 
                           this.STATUS, mRb.getString(this.DISP_EAR_FILE_NAME), 
                           mRb.getString(this.DISP_REF_BY), 
                           mRb.getString(this.DISP_REF_CLASS), 
                           mRb.getString(this.DISP_JNDI_NAME), 
                           mRb.getString(this.DISP_JNDI_CLASS_TYPE), 
                           mRb.getString(this.DISP_MESSAGE), 
                           mRb.getString(this.DISP_STATUS) };
        SimpleType[] types = 
            new SimpleType[] { SimpleType.STRING, SimpleType.STRING, 
                               SimpleType.STRING, SimpleType.STRING, 
                               SimpleType.STRING, SimpleType.STRING, 
                               SimpleType.INTEGER, SimpleType.STRING, 
                               SimpleType.STRING, SimpleType.STRING, 
                               SimpleType.STRING, SimpleType.STRING, 
                               SimpleType.STRING, SimpleType.STRING };
        String[] descrs = 
            new String[] { mRb.getString(EAR_FILE_NAME_MSG), mRb.getString(COMP_REFER_JNDI), 
                           mRb.getString(CLASS_REFER_JNDI), 
                           mRb.getString(JNDI_NAME_MSG), 
                           mRb.getString(JNDI_CLASS_TYPE_MSG), 
                           mRb.getString(MESSAGE_MSG), 
                           mRb.getString(STATUS_MSG), 
                           mRb.getString(EAR_FILE_NAME_MSG), 
                           mRb.getString(COMP_REFER_JNDI), 
                           mRb.getString(CLASS_REFER_JNDI), 
                           mRb.getString(JNDI_NAME_MSG), 
                           mRb.getString(JNDI_CLASS_TYPE_MSG), 
                           mRb.getString(MESSAGE_MSG), 
                           mRb.getString(STATUS_MSG) };
        CompositeType type = 
            new CompositeType(descrTab, descrTab, names, descrs, types);
        Object[] objData = null;
        int ejbIndex = 0;
        int resIndex = 0;
        boolean bContinue = true;
        List<ResourceInfo> resList = null;

        if (vResultList != null) {
            Set<String> jndiNames = new HashSet<String>();
            for (VerificationResult vResult: vResultList) {
                objData = new Object[14];
                resList = null;
                EJBInfo ejbInf = null;
                bContinue = true;
                ResourceInfo resInf = null;
                resIndex = 0;

                if (vResult.getStatus() != VerificationResult.NA && 
                    (!jndiNames.contains(vResult.getJndiName()))) {

                    jndiNames.add(vResult.getJndiName());

                    objData[0] = earFileName;
                    objData[1] = 
                            vResult.getReferencedBy() != null ? vResult.getReferencedBy() : 
                            "";
                    objData[2] = 
                            vResult.getReferencedClass() != null ? vResult.getReferencedClass() : 
                            "";

                    objData[3] = 
                            vResult.getJndiName() != null ? vResult.getJndiName() : 
                            "";
                    objData[4] = 
                            vResult.getJndiClassType() != null ? vResult.getJndiClassType() : 
                            "";
                    objData[5] = mRb.getString(WARNING_MSG);
                    objData[13] = mRb.getString(this.ERROR_STATUS);
                    if (vResult.getStatus() == VerificationResult.OK) {
                        objData[5] = mRb.getString(OK_MSG);
                        objData[6] = 0;
                        objData[13] = mRb.getString(this.OK_STATUS);
                    } else if (vResult.getStatus() == 
                               VerificationResult.WARNING) {
                        objData[6] = 1;
                        objData[13] = mRb.getString(this.WARNING_STATUS);
                    }

                    objData[7] = objData[0];
                    objData[8] = objData[1];
                    objData[9] = objData[2];
                    objData[10] = objData[3];
                    objData[11] = objData[4];
                    objData[12] = objData[5];

                    CompositeType typex;
                    typex = 
                            new CompositeType(descrTab, descrTab, names, descrs, 
                                              types);
                    ret = new CompositeDataSupport(typex, names, objData);
                    listComp.add(ret);
                }

            }
        }

        if (listComp.size() != 0 && ejbInfoList.size() == 0) {
            maxSize = 0;
        }

        if (listComp.size() == 0 && ejbInfoList.size() == 0) {
            maxSize = 1;
        }

        for (int ind = 0; ind < maxSize; ind++) {
            objData = new Object[14];
            resList = null;
            EJBInfo ejbInf = null;
            bContinue = true;
            ResourceInfo resInf = null;
            resIndex = 0;
            if (ejbInfoList.size() > 0) {
                ejbInf = ejbInfoList.get(ejbIndex++);
                resList = ejbInf.getResourceReferenceInfo();
            }
            boolean bResourceExists = resList != null && resList.size() > 0;
            while (ejbInf != null && bContinue) {
                // When there are no resource references in this EJB
                // and if the status is ok don't add that to result.
                if (!bResourceExists && 
                    ejbInf.getStatus() == VerifyStatus.OK) {
                    break;
                }
                objData[0] = earFileName;
                objData[1] = (ejbInf != null ? ejbInf.getEJBName() : "");
                objData[2] = (ejbInf != null ? ejbInf.getEJBClassName() : "");

                if (bResourceExists && resIndex < resList.size()) {
                    resInf = resList.get(resIndex++);
                }

                objData[3] = (resInf != null ? resInf.getResJNDIName() : "");
                objData[4] = (resInf != null ? resInf.getResourceType() : "");
                objData[5] = 
                        (resInf != null ? resInf.getMessage() : ejbInf.getMessage());

                if (resInf != null) {
                    objData[6] = getStatus(resInf.getStatus());
                } else {
                    objData[6] = getStatus(ejbInf.getStatus());
                }


                objData[13] = mRb.getString(this.ERROR_STATUS);
                if (objData[6].equals(0)) {
                    objData[13] = mRb.getString(this.OK_STATUS);
                } else if (objData[6].equals(1)) {
                    objData[13] = mRb.getString(this.WARNING_STATUS);
                } else {
                    objData[13] = mRb.getString(this.ERROR_STATUS);
                }

                objData[7] = objData[0];
                objData[8] = objData[1];
                objData[9] = objData[2];
                objData[10] = objData[3];
                objData[11] = objData[4];
                objData[12] = objData[5];

                if (resList == null || resList.size() == 0 || 
                    resIndex >= resList.size()) {
                    if (resInf != null && 
                        ejbInf.getStatus() == VerifyStatus.ERROR) {
                        resInf = null;
                    } else {
                        bContinue = false;
                    }
                }

                CompositeType typex;
                typex = 
                        new CompositeType(descrTab, descrTab, names, descrs, types);
                ret = new CompositeDataSupport(typex, names, objData);
                listComp.add(ret);
            }

        }


        TabularType tabletype = 
            new TabularType(descrTab, descrTab, type, new String[] { this.REF_CLASS, 
                                                                     this.REF_BY, 
                                                                     this.JNDI_NAME });

        TabularData t = new TabularDataSupport(tabletype);
        t.putAll(listComp.toArray(new CompositeData[0]));
        return t;


    }


    private int getStatus(VerifyStatus vs) {
        int retStatus = 0;
        if (vs == VerifyStatus.WARNING) {
            retStatus = 1;
        } else if (vs == VerifyStatus.ERROR) {
            retStatus = 2;
        }
        return retStatus;

    }


}
