/* DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common Development
 * and Distribution License("CDDL") (collectively, the "License").  You
 * may not use this file except in compliance with the License. You can obtain
 * a copy of the License at https://glassfish.dev.java.net/public/CDDL+GPL.html
 * or glassfish/bootstrap/legal/LICENSE.txt.  See the License for the specific
 * language governing permissions and limitations under the License.
 *
 * When distributing the software, include this License Header Notice in each
 * file and include the License file at glassfish/bootstrap/legal/LICENSE.txt.
 * Sun designates this particular file as subject to the "Classpath" exception
 * as provided by Sun in the GPL Version 2 section of the License file that
 * accompanied this code.  If applicable, add the following below the License
 * Header, with the fields enclosed by brackets [] replaced by your own
 * identifying information: "Portions Copyrighted [year]
 * [name of copyright owner]"
 *
 * Contributor(s):
 *
 * If you wish your version of this file to be governed by only the CDDL or
 * only the GPL Version 2, indicate your decision by adding "[Contributor]
 * elects to include this software in this distribution under the [CDDL or GPL
 * Version 2] license."  If you don't indicate a single choice of license, a
 * recipient has the option to distribute your version of this file under
 * either the CDDL, the GPL Version 2 or to extend the choice of license to
 * its licensees as provided above.  However, if you add GPL Version 2 code
 * and therefore, elected the GPL Version 2 license, then the option applies
 * only if the new code is made subject to such option by the copyright
 * holder.
 */


package org.glassfish.openesb.addons.appverifier;

import java.io.File;

import java.net.URL;
import java.net.URLDecoder;

import java.nio.charset.Charset;

/**
 * Helper class
 * @author Sreeni Genipudi
 */
public class ResourceFinder {
    private static ResourceFinder mInstance = null;

    private ResourceFinder() {
    }

    public static ResourceFinder getInstance() {
        if (mInstance == null) {
            mInstance = new ResourceFinder();
        }
        return mInstance;
    }

    /* (non-Javadoc)
         * @see com.stc.codegen.framework.model.util.CodeGenHelper#getJar(java.lang.ClassLoader, java.lang.Class)
         */

    public File getJar(ClassLoader classLoader, String clazz) {

        URL url = 
            classLoader.getResource(convertPkgToResPath(clazz) + ".class");
        return getJar(classLoader, url);
    }

    public File getJar(ClassLoader classLoader, Class clazz) {
        return getJar(classLoader, clazz.getName());
    }

    public File getJar(ClassLoader classLoader, URL url) {
        if (url == null) {
            return null;
        }
        String urlString = url.toString();
        int pos = urlString.indexOf("file:/");
        int end = urlString.indexOf("!");
        urlString = urlString.substring(pos + 6, end);
        File fileName = new File(urlString);
        if (urlString.indexOf("%") != -1 && !fileName.exists()) {
            try {
                fileName = 
                        new File(URLDecoder.decode(urlString, Charset.defaultCharset().name()));
            } catch (java.io.UnsupportedEncodingException ue) {

            }
        }
        return fileName;
    }


    public File getJarForFile(ClassLoader classLoader, String fileName) {
        String convertedPath = convertPkgToResPath(fileName);
        convertedPath = convertedPath.replace('%', '.');
        URL url = classLoader.getResource(convertedPath);
        return getJar(classLoader, url);
    }


    public String convertPkgToPath(String packagename) {
        return convertPkgToPath(packagename, File.separator);
    }

    public String convertPkgToResPath(String packagename) {
        return convertPkgToPath(packagename, "/");
    }

    // TODO turn it public

    private String convertPkgToPath(String packagename, String replaceToken) {
        String res;

        if ((packagename == null) || packagename.trim().equals("")) {
            res = "";
        } else if (packagename.indexOf(".") == -1) {
            res = packagename;
        } else {
            StringBuffer buffer = new StringBuffer();
            java.util.StringTokenizer stk = 
                new java.util.StringTokenizer(packagename, ".", true);

            while (stk.hasMoreTokens()) {
                String token = stk.nextToken();
                if (token.equals(".")) {
                    token = replaceToken;
                }

                buffer.append(token);
            }

            res = buffer.toString();
        }

        return res;
    }


}
