/* DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common Development
 * and Distribution License("CDDL") (collectively, the "License").  You
 * may not use this file except in compliance with the License. You can obtain
 * a copy of the License at https://glassfish.dev.java.net/public/CDDL+GPL.html
 * or glassfish/bootstrap/legal/LICENSE.txt.  See the License for the specific
 * language governing permissions and limitations under the License.
 *
 * When distributing the software, include this License Header Notice in each
 * file and include the License file at glassfish/bootstrap/legal/LICENSE.txt.
 * Sun designates this particular file as subject to the "Classpath" exception
 * as provided by Sun in the GPL Version 2 section of the License file that
 * accompanied this code.  If applicable, add the following below the License
 * Header, with the fields enclosed by brackets [] replaced by your own
 * identifying information: "Portions Copyrighted [year]
 * [name of copyright owner]"
 *
 * Contributor(s):
 *
 * If you wish your version of this file to be governed by only the CDDL or
 * only the GPL Version 2, indicate your decision by adding "[Contributor]
 * elects to include this software in this distribution under the [CDDL or GPL
 * Version 2] license."  If you don't indicate a single choice of license, a
 * recipient has the option to distribute your version of this file under
 * either the CDDL, the GPL Version 2 or to extend the choice of license to
 * its licensees as provided above.  However, if you add GPL Version 2 code
 * and therefore, elected the GPL Version 2 license, then the option applies
 * only if the new code is made subject to such option by the copyright
 * holder.
 */

package org.glassfish.openesb.addons.appverifier;

import java.util.Locale;
import java.util.ResourceBundle;

/**
 * This class contains data from ValidationConfig.properties.
 * @author Sreeni Genipudi
 */
public class ValidationConfig {

    private static final String CHECK_CLASS_EXISTENCE = 
        "CHECK_CLASS_EXISTENCE";
    private static final String CHECK_SET_CLASS_PROPERTIES = 
        "CHECK_SET_CLASS_PROPERTIES";
    private static final String HOSTNAME = "HOSTNAME";
    private static final String JMXPORT = "JMXPORT";
    private static final String USERNAME = "USERNAME";
    private static final String PASSWORD = "PASSWORD";
    private static final String CHECK_VALID_GLOBAL_RESOURCE = 
        "CHECK_VALID_GLOBAL_RESOURCE";


    private ResourceBundle mRb = null;

    private boolean mbCheckClassExistence = false;
    private boolean mCheckGRValidity = false;
    private boolean mbCheckPropeties = false;
    private String mHostName = null;
    private String mJMXPort = null;
    private String mUser = null;
    private String mPassword = null;

    public ValidationConfig() {
        mRb = 
ResourceBundle.getBundle("org.glassfish.openesb.addons.appverifier.ValidationConfig");
        String bundleProperty;
        try {

            bundleProperty = mRb.getString(CHECK_CLASS_EXISTENCE);
            if (bundleProperty != null && 
                bundleProperty.equalsIgnoreCase("TRUE")) {
                mbCheckClassExistence = true;
            }
        } catch (java.util.MissingResourceException me) {

        }

        try {
            bundleProperty = mRb.getString(CHECK_SET_CLASS_PROPERTIES);
            if (bundleProperty != null && 
                bundleProperty.equalsIgnoreCase("TRUE")) {
                mbCheckPropeties = true;
            }
        } catch (java.util.MissingResourceException me) {

        }

        try {
            bundleProperty = mRb.getString(CHECK_VALID_GLOBAL_RESOURCE);
            if (bundleProperty != null && 
                bundleProperty.equalsIgnoreCase("TRUE")) {
                mCheckGRValidity = true;
            }
        } catch (java.util.MissingResourceException me) {

        }

        try {
            bundleProperty = mRb.getString(HOSTNAME);
            if (bundleProperty != null) {
                mHostName = bundleProperty;
            }
            bundleProperty = mRb.getString(JMXPORT);
            if (bundleProperty != null) {
                mJMXPort = bundleProperty;
            }
            bundleProperty = mRb.getString(USERNAME);
            if (bundleProperty != null) {
                mUser = bundleProperty;
            }
            bundleProperty = mRb.getString(PASSWORD);
            if (bundleProperty != null) {
                mPassword = bundleProperty;
            }
        } catch (java.util.MissingResourceException me) {

        }


    }

    public boolean checkClassExistence() {
        return mbCheckClassExistence;
    }

    public boolean checkSettingClassProperties() {
        return mbCheckPropeties;
    }

    private void setHostName(String mHostName) {
        this.mHostName = mHostName;
    }

    public String getHostName() {
        return mHostName;
    }

    private void setJMXPort(String mJMXPort) {
        this.mJMXPort = mJMXPort;
    }

    public String getJMXPort() {
        return mJMXPort;
    }

    private void setUser(String mUser) {
        this.mUser = mUser;
    }

    public String getUser() {
        return mUser;
    }

    private void setPassword(String mPassword) {
        this.mPassword = mPassword;
    }

    String getPassword() {
        return mPassword;
    }


    public boolean checkValidGlobalRAR() {
        return mCheckGRValidity;
    }


}
