/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common Development
 * and Distribution License("CDDL") (collectively, the "License").  You
 * may not use this file except in compliance with the License. You can obtain
 * a copy of the License at https://glassfish.dev.java.net/public/CDDL+GPL.html
 * or glassfish/bootstrap/legal/LICENSE.txt.  See the License for the specific
 * language governing permissions and limitations under the License.
 *
 * When distributing the software, include this License Header Notice in each
 * file and include the License file at glassfish/bootstrap/legal/LICENSE.txt.
 * Sun designates this particular file as subject to the "Classpath" exception
 * as provided by Sun in the GPL Version 2 section of the License file that
 * accompanied this code.  If applicable, add the following below the License
 * Header, with the fields enclosed by brackets [] replaced by your own
 * identifying information: "Portions Copyrighted [year]
 * [name of copyright owner]"
 *
 * Contributor(s):
 *
 * If you wish your version of this file to be governed by only the CDDL or
 * only the GPL Version 2, indicate your decision by adding "[Contributor]
 * elects to include this software in this distribution under the [CDDL or GPL
 * Version 2] license."  If you don't indicate a single choice of license, a
 * recipient has the option to distribute your version of this file under
 * either the CDDL, the GPL Version 2 or to extend the choice of license to
 * its licensees as provided above.  However, if you add GPL Version 2 code
 * and therefore, elected the GPL Version 2 license, then the option applies
 * only if the new code is made subject to such option by the copyright
 * holder.
 */
package org.glassfish.openesb.addons.appverifier.ejb;

import org.glassfish.openesb.addons.appverifier.ErrorBundle;

//import org.glassfish.openesb.addons.appverifier.Validation.ValidationHelper;

import java.util.List;

import org.glassfish.openesb.addons.appverifier.ValidationConfig;
import org.glassfish.openesb.addons.appverifier.VerifyStatus;
/**
 * Class instance holds the meta data for EJB
 * @author Sreeni Genipudi
 */
/**
 *  EJB Information required for application verifier is represented in this class
 */
public class EJBInfo {

    /**
     *  EJB Name
     */
    private String mEJBName;
    //EJB Class name
    private String mEJBClassName;
    //EJB JNDI name
    private String mEJBJndiName;
    //List of EJB references
    private List<EJBReferenceInfo> mEJBRefereceInfo;
    //List of resource references
    private List<ResourceInfo> mResRefInfo;

    protected ValidationConfig mValidConfig;
    protected ErrorBundle mErrorBundle = null;
    protected VerifyStatus mStatus = VerifyStatus.OK;
    protected String mMessage = "";
    protected ClassLoader mLoader = null;

    /**
     *  Get EJB Name
     */
    public String getEJBName() {
        return mEJBName;
    }

    /**
     *  Set EJB Name
     */
    public void setEJBName(String name) {
        this.mEJBName = name;
    }

    /**
     * Set EJB Class name
     */
    public void setEJBClassName(String name) {
        mEJBClassName = name;
    }

    /**
     * Get EJB Class name
     * @return EJB Class name
     */
    public String getEJBClassName() {
        return mEJBClassName;
    }
    //constructor

    public EJBInfo() {
    }
    //constructor

    public EJBInfo(ValidationConfig config, ClassLoader loader, 
                   ErrorBundle bundle) {
        mValidConfig = config;
        mErrorBundle = bundle;
        mLoader = loader;
    }

    /**
     * Get Resource reference information
     * @return list of resource information metadata.
     */
    public List<ResourceInfo> getResourceReferenceInfo() {
        return mResRefInfo;
    }

    /**
     * Get EJB reference information
     * @return list of EJB information metadata.
     */
    public List<EJBReferenceInfo> getEJBReferenceInfo() {
        return this.mEJBRefereceInfo;
    }

    /**
     * Set Resource reference information
     */
    public void setResourceReferenceInfo(List<ResourceInfo> resourceInfo) {
        mResRefInfo = resourceInfo;
    }

    /**
     * Set EJB reference information
     */
    public void setEJBReferenceInfo(List<EJBReferenceInfo> ejbRef) {
        mEJBRefereceInfo = ejbRef;
    }

    /**
     * Set JNDI name
     */
    public void setJNDIName(String jndi) {
        this.mEJBJndiName = jndi;
    }

    /**
     * Get JNDI name
     */
    public String getJNDIName() {
        return this.mEJBJndiName;
    }

    public boolean equals(Object obj) {
        if (obj != null && obj instanceof EJBInfo) {
            EJBInfo ejbObj = (EJBInfo)obj;
            String ejbClassName = ejbObj.getEJBClassName();
            String ejbName = ejbObj.getEJBName();
            if (ejbClassName != null && ejbClassName.equals(mEJBClassName) && 
                ejbName != null && ejbName.equals(this.mEJBName)) {
                return true;
            }
        }
        return false;
    }

    public int hashCode() {
        return mEJBClassName.hashCode();
    }

    /**
     * Set message information
     */
    public void setMessage(String message) {
        this.mMessage = message;
    }

    /**
     * Get message information
     */
    public String getMessage() {
        return mMessage;
    }

    /**
     * Set verification status
     * @param status
     */
    public void setStatus(VerifyStatus status) {
        this.mStatus = status;
    }

    /**
     * Get verification status
     * @return
     */
    public VerifyStatus getStatus() {
        return this.mStatus;
    }
}
