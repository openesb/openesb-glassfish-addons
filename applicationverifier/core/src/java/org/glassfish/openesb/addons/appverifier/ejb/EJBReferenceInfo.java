/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common Development
 * and Distribution License("CDDL") (collectively, the "License").  You
 * may not use this file except in compliance with the License. You can obtain
 * a copy of the License at https://glassfish.dev.java.net/public/CDDL+GPL.html
 * or glassfish/bootstrap/legal/LICENSE.txt.  See the License for the specific
 * language governing permissions and limitations under the License.
 *
 * When distributing the software, include this License Header Notice in each
 * file and include the License file at glassfish/bootstrap/legal/LICENSE.txt.
 * Sun designates this particular file as subject to the "Classpath" exception
 * as provided by Sun in the GPL Version 2 section of the License file that
 * accompanied this code.  If applicable, add the following below the License
 * Header, with the fields enclosed by brackets [] replaced by your own
 * identifying information: "Portions Copyrighted [year]
 * [name of copyright owner]"
 *
 * Contributor(s):
 *
 * If you wish your version of this file to be governed by only the CDDL or
 * only the GPL Version 2, indicate your decision by adding "[Contributor]
 * elects to include this software in this distribution under the [CDDL or GPL
 * Version 2] license."  If you don't indicate a single choice of license, a
 * recipient has the option to distribute your version of this file under
 * either the CDDL, the GPL Version 2 or to extend the choice of license to
 * its licensees as provided above.  However, if you add GPL Version 2 code
 * and therefore, elected the GPL Version 2 license, then the option applies
 * only if the new code is made subject to such option by the copyright
 * holder.
 */

package org.glassfish.openesb.addons.appverifier.ejb;

/**
 * Class instance hold information about EJB reference.
 * @author Sreeni Genipudi
 */
public class EJBReferenceInfo {
    //EJB Reference name
    private String mEJBRefName;
    //EJB reference type
    private String mEJBRefType;
    //EJB Local name
    private String mEJBLocal;
    //EJB Local Home Name
    private String mEJBLocalHome;
    //EJB Remote name
    private String mEJBRemote;
    //EJB Remote home name
    private String mEJBRemoteHome;
    //EJB Link
    private String mEJBLink;
    //EJB JNDI name
    private String mJNDIName;

    /**
     * Set EJB Reference name.
     * @param ejb reference name
     */
    public void setEJBRefName(String name) {
        mEJBRefName = name;
    }

    /**
     * Set EJB Reference type
     * @param reference type
     */
    public void setEJBRefType(String type) {
        mEJBRefType = type;
    }

    /**
     * Set EJB Local interface name
     * @param EJB Local interface name
     */
    public void setEJBLocal(String local) {
        mEJBLocal = local;
    }

    /**
     * Set EJBReference JNDI name
     * @param jndi name
     */
    public void setJNDIName(String jndi) {
        this.mJNDIName = jndi;
    }

    /**
     * Set EJB Local home name
     * @param EJB Local home name
     */
    public void setEJBLocalHome(String home) {
        mEJBLocalHome = home;
    }

    /**
     * Set EJB Remote
     * @param EJB Remote
     */
    public void setEJBRemote(String rem) {
        mEJBRemote = rem;
    }

    /**
     * Set EJB Remote home name
     * @param EJB Remote home name
     */
    public

    void setEJBRemoteHome(String remHome) {
        mEJBRemoteHome = remHome;
    }

    /**
     * Set EJB Link
     * @param EJB Link
     */
    public

    void setEJBLink(String link) {
        mEJBLink = link;
    }

    /**
     * Get EJB Reference name
     * @param EJB Reference name
     */
    public String getEJBRefName() {
        return mEJBRefName;
    }

    /**
     * Get EJB local name
     * @param EJB local name
     */
    public String getEJBLocal() {
        return mEJBLocal;
    }

    /**
     * Get EJB Local home name
     * @param EJB Local home name
     */
    public String getEJBLocalHome() {
        return mEJBLocalHome;
    }

    /**
     * Get EJB remote name
     * @param EJB remote name
     */
    public String getEJBRemote() {
        return mEJBRemote;
    }

    /**
     * Get JNDIName
     * @return  EJB Remote home
     */
    public String getJNDIName() {
        return this.mJNDIName;
    }

    /**
     * Get EJB Remote home
     * @return  EJB Remote home
     */
    public String getEJBRemoteHome() {
        return mEJBRemoteHome;
    }

    /**
     * Get EJB Reference type
     * @return  EJB Reference type
     */
    public String getEJBRefType() {
        return mEJBRefType;
    }

    /**
     * Get EJB Link
     * @return EJB Link
     */
    public String getEJBLink() {
        return this.mEJBLink;
    }


}
