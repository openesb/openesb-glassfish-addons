/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common Development
 * and Distribution License("CDDL") (collectively, the "License").  You
 * may not use this file except in compliance with the License. You can obtain
 * a copy of the License at https://glassfish.dev.java.net/public/CDDL+GPL.html
 * or glassfish/bootstrap/legal/LICENSE.txt.  See the License for the specific
 * language governing permissions and limitations under the License.
 *
 * When distributing the software, include this License Header Notice in each
 * file and include the License file at glassfish/bootstrap/legal/LICENSE.txt.
 * Sun designates this particular file as subject to the "Classpath" exception
 * as provided by Sun in the GPL Version 2 section of the License file that
 * accompanied this code.  If applicable, add the following below the License
 * Header, with the fields enclosed by brackets [] replaced by your own
 * identifying information: "Portions Copyrighted [year]
 * [name of copyright owner]"
 *
 * Contributor(s):
 *
 * If you wish your version of this file to be governed by only the CDDL or
 * only the GPL Version 2, indicate your decision by adding "[Contributor]
 * elects to include this software in this distribution under the [CDDL or GPL
 * Version 2] license."  If you don't indicate a single choice of license, a
 * recipient has the option to distribute your version of this file under
 * either the CDDL, the GPL Version 2 or to extend the choice of license to
 * its licensees as provided above.  However, if you add GPL Version 2 code
 * and therefore, elected the GPL Version 2 license, then the option applies
 * only if the new code is made subject to such option by the copyright
 * holder.
 */


package org.glassfish.openesb.addons.appverifier.ejb;

import org.glassfish.openesb.addons.appverifier.ErrorBundle;
import org.glassfish.openesb.addons.appverifier.InboundRARValidator;
import org.glassfish.openesb.addons.appverifier.OutboundRARValidator;
import org.glassfish.openesb.addons.appverifier.RAHelper;
import org.glassfish.openesb.addons.appverifier.ValidationConfig;

import org.glassfish.openesb.addons.appverifier.VerifyStatus;

import java.io.File;

import org.glassfish.openesb.addons.appverifier.javaee.ejb.descriptor.EjbJarType;

import org.glassfish.openesb.addons.appverifier.javaee.ejb.descriptor.EnterpriseBeansType;

import org.glassfish.openesb.addons.appverifier.javaee.ejb.descriptor.HomeType;
import org.glassfish.openesb.addons.appverifier.javaee.ejb.descriptor.MessageDrivenBeanType;
import org.glassfish.openesb.addons.appverifier.javaee.ejb.descriptor.RemoteType;
import org.glassfish.openesb.addons.appverifier.javaee.ejb.descriptor.SessionBeanType;
import org.glassfish.openesb.addons.appverifier.javaee.ejb.descriptor.SessionTypeType;

import org.glassfish.openesb.addons.appverifier.javaee.sunejb.descriptor.SunEjbJar;
import org.glassfish.openesb.addons.appverifier.ra.InboundResourceAdapter;

import org.glassfish.openesb.addons.appverifier.ra.OutboundResourceAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import java.util.Locale;
import java.util.Map;

import java.util.Properties;

import java.util.ResourceBundle;
import java.util.logging.Logger;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

/**
 * XMLReader for javaee/ejb-jar xml.
 * @author Sreeni Genipudi
 */
public class EjbJavaEEXMLReader implements EjbReader {
    //ejb jar type
    private EjbJarType mejbType = null;
    //sun ejb jar 
    private SunEjbJar mSunejb = null;
    private ClassLoader mResourceLoader = null;
    //List of EJBs in descriptor
    private List<EJBInfo> mEJBList = new ArrayList<EJBInfo>();
    //Mapping EJB Name with resource reference
    private HashMap<String, Properties> mMapEjbNameResourceMap = 
        new HashMap<String, Properties>();
    //Mapping EJBName with EJB Reference map
    private HashMap<String, Properties> mMapEjbNameEjbRefMap = 
        new HashMap<String, Properties>();
    //Map EJB Name with EJB JNDI name map
    private HashMap<String, String> mMapEjbJNDINameMap = 
        new HashMap<String, String>();
    private Properties mMapEjbNameResourceMID = new Properties();
    private RAHelper mRAHlpr = null;
    private ResourceBundle mRb = null;

    public static final String RESOURCEMID_NOT_FOUND_EJB = 
        "RESOURCEMID_NOT_FOUND_EJB";
    public static final String RESOURCEMID_NOT_FOUND = "RESOURCEMID_NOT_FOUND";
    public static final String REFFERRED_BY = "REFFERRED_BY";
    public static final String FOR_RESOURCE_NAME = "FOR_RESOURCE_NAME";
    public static final String GLOBAL_RESOURCEMID_NOT_FOUND = 
        "GLOBAL_RESOURCEMID_NOT_FOUND";
    public static final String MISSING_RESOURCE_SUN_EJB_JAR = 
        "MISSING_RESOURCE_SUN_EJB_JAR";
    public static final String MISSING_RESOURCE_JNDI_SUN_EJB_JAR = 
        "MISSING_RESOURCE_JNDI_SUN_EJB_JAR";
    public static final String ERROR_INVALID_JNDI_NAME = 
        "ERROR_INVALID_JNDI_NAME";
    public static final String RESOURCE_JNDI_NOT_FOUND = 
        "RESOURCE_JNDI_NOT_FOUND";
    public static final String GLOBAL_RESOURCE_JNDI_NAME = 
        "GLOBAL_RESOURCE_JNDI_NAME";
    public static final String NOT_FOUND = "NOT_FOUND";
    public static final String ERROR_EJB_REFERENCE_NOT_FOUND = 
        "ERROR_EJB_REFERENCE_NOT_FOUND";
    public static final String CANNOT_RESOLVE_LOCAL_RESOURCE = 
        "CANNOT_RESOLVE_LOCAL_RESOURCE";
    public static final String RESOURCE_TYPE = "RESOURCE_TYPE";
    public static final String CANNOT_RESOLVE_GLOBAL_RESOURCE = 
        "CANNOT_RESOLVE_GLOBAL_RESOURCE";


    private static Logger logger = 
        Logger.getLogger("org.glassfish.openesb.addons.appverifier.ejb.EjbJavaEEXMLReader");

    public EjbJavaEEXMLReader(EjbJarType et, SunEjbJar st, 
                              ValidationConfig config, ClassLoader loader, 
                              ErrorBundle eb, 
                              List<InboundResourceAdapter> inboundRARlist, 
                              List<OutboundResourceAdapter> outboundRARList, 
                              String instance) throws Exception {
        mRb = 
ResourceBundle.getBundle("org.glassfish.openesb.addons.appverifier.ejb.Bundle", 
                         Locale.getDefault());
        mejbType = et;
        mSunejb = st;
        mResourceLoader = loader;
        mRAHlpr = RAHelper.getInstance(config, eb, instance);
        populateSunejbInfo(st);
        //Read sun-ejb-jar.xml first.
        //TODO GET ejbTYPE AS PARAMETER ALSO SAME FOR SUN EJB JAR


        //Read ejb-jar.xml
        /*    JAXBContext jc =
                   JAXBContext.newInstance("com.stc.applicationVerifier.javaee.ejb.descriptor");
        Unmarshaller um = jc.createUnmarshaller();
        ejbType ej = (ejbType)
       javax.xml.bind.JAXBElement je =
          (javax.xml.bind.JAXBElement)um.unmarshal(mEjbXML);
        ejbType et = (ejbType)je.getValue();*/

        EnterpriseBeansType enterpriseType = et.getEnterpriseBeans();
        java.util.List<java.lang.Object> listOfEntityOrSession = 
            enterpriseType.getSessionOrEntityOrMessageDriven();
        MDBInfo mdbInfo = null;
        HashMap<String, String> activationConfig = null;
        for (java.lang.Object obj: listOfEntityOrSession) {

            if (obj instanceof MessageDrivenBeanType) {
                MessageDrivenBeanType mbt = (MessageDrivenBeanType)obj;
                populateMDBInfo(mbt, config, loader, eb, inboundRARlist, 
                                outboundRARList, instance);
            } else {
                if (obj instanceof SessionBeanType) {
                    SessionBeanType sbt = (SessionBeanType)obj;
                    populateSBInfo(sbt, config, loader, eb, inboundRARlist, 
                                   outboundRARList, instance);


                }
            }


        }

    }


    private void populateMDBInfo(MessageDrivenBeanType mbt, 
                                 ValidationConfig config, ClassLoader loader, 
                                 ErrorBundle eb, 
                                 List<InboundResourceAdapter> inboundRARlist, 
                                 List<OutboundResourceAdapter> outboundRARList, 
                                 String instance) {
        InboundRARValidator inbValidator = InboundRARValidator.getInstance();
        MDBInfo mdbInfo = new MDBInfo(config, loader, eb);
        OutboundRARValidator outRARValidator = 
            OutboundRARValidator.getInstance();
        RAHelper raHlpr = RAHelper.getInstance(config, eb, instance);

        mEJBList.add(mdbInfo);
        String ejbName = mbt.getEjbName().getValue();
        mdbInfo.setEJBName(ejbName);
        mdbInfo.setJNDIName(mMapEjbJNDINameMap.get(ejbName));
        String messagingType = mbt.getMessagingType().getValue();
        mdbInfo.setEJBClassName(mbt.getEjbClass().getValue());
        mdbInfo.setMessagingType(messagingType);
        String resourceMID = this.mMapEjbNameResourceMID.getProperty(ejbName);
        String errMsg = null;
        if (resourceMID == null) {
            errMsg = mRb.getString(RESOURCEMID_NOT_FOUND_EJB) + ejbName;
            eb.addError(errMsg);
            mdbInfo.setMessage(errMsg);
            mdbInfo.setStatus(VerifyStatus.ERROR);
        }

        if (inbValidator.isResourceMIDLocal(resourceMID)) {
            if (!inbValidator.isValidLocalResource(resourceMID, messagingType, 
                                                   inboundRARlist, eb)) {
                errMsg = 
                        mRb.getString(RESOURCEMID_NOT_FOUND) + resourceMID + " " + 
                        mRb.getString(REFFERRED_BY) + ejbName;
                eb.addError(errMsg);
                mdbInfo.setMessage(errMsg);
                mdbInfo.setStatus(VerifyStatus.ERROR);
            }
        } else if (config.checkValidGlobalRAR() && 
                   !mRAHlpr.isGlobalResourceMID(resourceMID)) {
            errMsg = 
                    mRb.getString(GLOBAL_RESOURCEMID_NOT_FOUND) + resourceMID + 
                    " " + mRb.getString(REFFERRED_BY) + ejbName;
            eb.addError(errMsg);
            mdbInfo.setMessage(errMsg);
            mdbInfo.setStatus(VerifyStatus.ERROR);
        }

        org.glassfish.openesb.addons.appverifier.javaee.ejb.descriptor.ActivationConfigType at = 
            mbt.getActivationConfig();
        HashMap activationConfig = new HashMap<String, String>();
        java.util.List<org.glassfish.openesb.addons.appverifier.javaee.ejb.descriptor.ActivationConfigPropertyType> acList = 
            at.getActivationConfigProperty();
        for (org.glassfish.openesb.addons.appverifier.javaee.ejb.descriptor.ActivationConfigPropertyType atconfig: 
             acList) {
            activationConfig.put(atconfig.getActivationConfigPropertyName().getValue().toString(), 
                                 atconfig.getActivationConfigPropertyValue().getValue().toString());
        }
        mdbInfo.setActivationConfig(activationConfig);
        java.util.List<org.glassfish.openesb.addons.appverifier.javaee.ejb.descriptor.EjbLocalRefType> ejbRefList = 
            mbt.getEjbLocalRef();
        List<EJBReferenceInfo> listOfEJBRefs = 
            new ArrayList<EJBReferenceInfo>();
        EJBReferenceInfo ejbRefInfo = null;
        if (ejbRefList != null && ejbRefList.size() > 0) {
            ejbRefInfo = new EJBReferenceInfo();
            listOfEJBRefs.add(ejbRefInfo);
            for (org.glassfish.openesb.addons.appverifier.javaee.ejb.descriptor.EjbLocalRefType ejbRef: 
                 ejbRefList) {
                ejbRefInfo.setEJBRefName(ejbRef.getEjbRefName().getValue().toString());
                ejbRefInfo.setEJBRefType(ejbRef.getEjbRefType().getValue().toString());
                org.glassfish.openesb.addons.appverifier.javaee.ejb.descriptor.LocalType lt = 
                    ejbRef.getLocal();
                if (lt != null) {
                    ejbRefInfo.setEJBLocal(lt.getValue().toString());
                }
                org.glassfish.openesb.addons.appverifier.javaee.ejb.descriptor.LocalHomeType lht = 
                    ejbRef.getLocalHome();
                if (lht != null) {
                    ejbRefInfo.setEJBLocalHome(lt.getValue().toString());

                }
                org.glassfish.openesb.addons.appverifier.javaee.ejb.descriptor.EjbLinkType ejblt = 
                    ejbRef.getEjbLink();
                if (ejblt != null) {
                    ejbRefInfo.setEJBLink(lt.getValue().toString());
                }
            }
        }
        List<ResourceInfo> resInfoList = new ArrayList<ResourceInfo>();
        java.util.List<org.glassfish.openesb.addons.appverifier.javaee.ejb.descriptor.ResourceRefType> mbtRRefList = 
            mbt.getResourceRef();

        if (mbtRRefList != null && mbtRRefList.size() > 0) {
            Properties resMapProperties = 
                this.mMapEjbNameResourceMap.get(ejbName);
            if (resMapProperties == null || resMapProperties.size() == 0) {
                eb.addError(mRb.getString(MISSING_RESOURCE_SUN_EJB_JAR));
            } else {
                String resJNDIName = null;
                String resRefName = null;
                String resRefType = null;
                ResourceInfo resRef = null;
                for (org.glassfish.openesb.addons.appverifier.javaee.ejb.descriptor.ResourceRefType mbtRRef: 
                     mbtRRefList) {
                    resRef = new ResourceInfo();
                    resInfoList.add(resRef);
                    resRefName = mbtRRef.getResRefName().getValue().toString();
                    resRefType = mbtRRef.getResType().getValue().toString();
                    resRef.setResourceRefName(resRefName);
                    resRef.setResourceType(resRefType);
                    resJNDIName = resMapProperties.getProperty(resRefName);
                    if (resJNDIName == null) {
                        eb.addError(mRb.getString(MISSING_RESOURCE_SUN_EJB_JAR) + 
                                    mRb.getString(FOR_RESOURCE_NAME) + 
                                    resRefName);
                        resRef.setMessage(mRb.getString(MISSING_RESOURCE_JNDI_SUN_EJB_JAR));
                        resRef.setStatus(VerifyStatus.WARNING);

                    } else {
                        if (outRARValidator.isResourceLocal(resJNDIName)) {
                            if (!outRARValidator.isValidLocalResource(resJNDIName, 
                                                                      resRefType, 
                                                                      outboundRARList, 
                                                                      eb)) {
                                eb.addError(mRb.getString(ERROR_INVALID_JNDI_NAME) + 
                                            resJNDIName + 
                                            mRb.getString(RESOURCE_JNDI_NOT_FOUND) + 
                                            resRefName);
                                resRef.setMessage(mRb.getString(ERROR_INVALID_JNDI_NAME) + 
                                                  resJNDIName + 
                                                  mRb.getString(RESOURCE_JNDI_NOT_FOUND) + 
                                                  resRefName);
                                resRef.setStatus(VerifyStatus.ERROR);
                                continue;
                            }
                        } else {
                            if (!raHlpr.isGlobalResource(resJNDIName, 
                                                         resRefType, eb)) {
                                eb.addError(mRb.getString(GLOBAL_RESOURCE_JNDI_NAME) + 
                                            resJNDIName + 
                                            mRb.getString(NOT_FOUND));
                                resRef.setMessage(mRb.getString(GLOBAL_RESOURCE_JNDI_NAME) + 
                                                  resJNDIName + 
                                                  mRb.getString(NOT_FOUND));
                                resRef.setStatus(VerifyStatus.ERROR);
                                continue;
                            }
                        }
                        resRef.setResourceJNDIName(resJNDIName);
                    }
                }
            }
        }

        java.util.List<org.glassfish.openesb.addons.appverifier.javaee.ejb.descriptor.ResourceEnvRefType> mbtREnvRefList = 
            mbt.getResourceEnvRef();

        if (mbtRRefList != null && mbtRRefList.size() > 0) {
            Properties resMapProperties = 
                this.mMapEjbNameResourceMap.get(ejbName);
            if (resMapProperties == null || resMapProperties.size() == 0) {
                eb.addError(mRb.getString(MISSING_RESOURCE_SUN_EJB_JAR));
            } else {
                String resJNDIName = null;
                String resRefName = null;
                String resRefType = null;
                ResourceInfo resRef = null;

                for (org.glassfish.openesb.addons.appverifier.javaee.ejb.descriptor.ResourceEnvRefType mbtRRef: 
                     mbtREnvRefList) {
                    resRef = new ResourceInfo();
                    resInfoList.add(resRef);
                    resRefName = 
                            mbtRRef.getResourceEnvRefName().getValue().toString();
                    resRef.setResourceRefName(resRefName);
                    resRefType = mbtRRef.getResourceEnvRefType().getValue();
                    resJNDIName = resMapProperties.getProperty(resRefName);
                    resRef.setResourceType(resRefType);
                    if (resJNDIName == null) {
                        eb.addError(mRb.getString(MISSING_RESOURCE_SUN_EJB_JAR) + 
                                    resRefName);

                    } else {

                        if (outRARValidator.isResourceLocal(resJNDIName)) {
                            if (!outRARValidator.isValidLocalResource(resJNDIName, 
                                                                      resRefType, 
                                                                      outboundRARList, 
                                                                      eb)) {
                                eb.addError(mRb.getString(ERROR_INVALID_JNDI_NAME) + 
                                            resJNDIName + 
                                            mRb.getString(RESOURCE_JNDI_NOT_FOUND) + 
                                            resRefName);
                                resRef.setMessage(mRb.getString(ERROR_INVALID_JNDI_NAME) + 
                                                  resJNDIName + 
                                                  mRb.getString(RESOURCE_JNDI_NOT_FOUND) + 
                                                  resRefName);
                                resRef.setStatus(VerifyStatus.ERROR);
                                continue;
                            }
                        } else {
                            if (!raHlpr.isGlobalResource(resJNDIName, 
                                                         resRefType, eb)) {
                                resRef.setMessage((GLOBAL_RESOURCE_JNDI_NAME) + 
                                                  resJNDIName + 
                                                  mRb.getString(NOT_FOUND));
                                resRef.setStatus(VerifyStatus.ERROR);
                                continue;
                            }
                        }

                    }
                    resRef.setResourceJNDIName(resJNDIName);
                }
            }
        }
        mdbInfo.setResourceReferenceInfo(resInfoList);

        // mdbInfo.setEJBReferenceInfo();
        List<EJBReferenceInfo> ejbRefINfoList = 
            new ArrayList<EJBReferenceInfo>();
        java.util.List<org.glassfish.openesb.addons.appverifier.javaee.ejb.descriptor.EjbLocalRefType> listOfEJBLocalRefs = 
            mbt.getEjbLocalRef();
        EJBReferenceInfo ejbLocalRefInfo;
        ejbLocalRefInfo = null;
        String ejbLinkName = null;
        String ejbRefName = null;
        String ejbJNDIName = null;
        if (listOfEJBLocalRefs != null && listOfEJBLocalRefs.size() > 0) {
            mdbInfo.setEJBReferenceInfo(ejbRefINfoList);
        }
        for (org.glassfish.openesb.addons.appverifier.javaee.ejb.descriptor.EjbLocalRefType ejbLocalRef: 
             listOfEJBLocalRefs) {
            ejbLocalRefInfo = new EJBReferenceInfo();
            ejbRefINfoList.add(ejbLocalRefInfo);
            ejbLinkName = ejbLocalRef.getEjbLink().getValue();
            ejbLocalRefInfo.setEJBLink(ejbLinkName);
            ejbRefName = ejbLocalRef.getEjbRefName().getValue();
            ejbRefInfo.setEJBRefName(ejbRefName);
            org.glassfish.openesb.addons.appverifier.javaee.ejb.descriptor.LocalType lt = 
                ejbLocalRef.getLocal();
            if (lt != null) {
                ejbRefInfo.setEJBLocal(lt.getValue());
            }
            org.glassfish.openesb.addons.appverifier.javaee.ejb.descriptor.LocalHomeType lht = 
                ejbLocalRef.getLocalHome();
            if (lht != null) {
                ejbRefInfo.setEJBLocalHome(lht.getValue());
            }
            if (ejbLinkName == null) {
                Properties ejRefProperties = 
                    this.mMapEjbNameEjbRefMap.get(ejbName);
                ejbJNDIName = ejRefProperties.getProperty(ejbRefName);
                if (ejbJNDIName == null) {
                    eb.addError(mRb.getString(ERROR_EJB_REFERENCE_NOT_FOUND) + 
                                ejbRefName);
                    continue;
                }


            }
        }
    }


    private void populateSBInfo(SessionBeanType sbt, ValidationConfig config, 
                                ClassLoader loader, ErrorBundle eb, 
                                List<InboundResourceAdapter> inboundRARlist, 
                                List<OutboundResourceAdapter> outboundRARList, 
                                String instance) {
        logger.fine(" Session Bean Class = " + sbt.getEjbClass().getValue());
        logger.fine(" Session Bean Name = " + sbt.getEjbName().getValue());
        SessionBeanInfo sbInfo;
        String ejbName = sbt.getEjbName().getValue().toString();
        RAHelper raHlpr = RAHelper.getInstance(config, eb, instance);
        sbInfo = new SessionBeanInfo(config, loader, eb);
        sbInfo.setEJBName(ejbName);
        sbInfo.setJNDIName(mMapEjbJNDINameMap.get(ejbName));
        sbInfo.setEJBClassName(sbt.getEjbClass().getValue().toString());
        mEJBList.add(sbInfo);

        org.glassfish.openesb.addons.appverifier.javaee.ejb.descriptor.LocalHomeType localHomeType = 
            sbt.getLocalHome();
        if (localHomeType != null) {
            sbInfo.setLocalHome(localHomeType.getValue());
        }
        org.glassfish.openesb.addons.appverifier.javaee.ejb.descriptor.LocalType localType = 
            sbt.getLocal();
        if (localType != null) {
            sbInfo.setLocal(localType.getValue());
        }
        RemoteType rt = sbt.getRemote();
        if (rt != null) {
            sbInfo.setRemote(rt.getValue());
        }

        HomeType ht = sbt.getHome();
        if (ht != null) {
            sbInfo.setRemoteHome(ht.getValue());
        }

        java.util.List<org.glassfish.openesb.addons.appverifier.javaee.ejb.descriptor.EjbLocalRefType> ejbRefList = 
            sbt.getEjbLocalRef();
        List<EJBReferenceInfo> ejbReferenceInfoList = 
            new ArrayList<EJBReferenceInfo>();
        EJBReferenceInfo ejbRefInfo = null;
        String ejbLink = null;
        String ejbRefName = null;
        String ejbJNDIName = null;

        for (org.glassfish.openesb.addons.appverifier.javaee.ejb.descriptor.EjbLocalRefType ejbRef: 
             ejbRefList) {
            ejbRefInfo = new EJBReferenceInfo();
            ejbReferenceInfoList.add(ejbRefInfo);
            ejbLink = ejbRef.getEjbLink().getValue();
            ejbRefName = ejbRef.getEjbRefName().getValue();
            ejbRefInfo.setEJBLink(ejbLink);
            ejbRefInfo.setEJBLocal(ejbRef.getLocal().getValue());
            ejbRefInfo.setEJBLocalHome(ejbRef.getLocalHome().getValue());
            if (ejbLink == null) {
                Properties ejbProperties = 
                    this.mMapEjbNameEjbRefMap.get(ejbName);
                if (ejbProperties == null) {
                    eb.addError(" Error - sun-ejb-jar.xml is not found for ejb-jar.xml EJBName = " + 
                                ejbName);
                    continue;
                }
                ejbJNDIName = ejbProperties.getProperty(ejbRefName);
                if (ejbJNDIName == null) {
                    eb.addError(" Error - JNDI Name not found for ejb-jar.xml EJBName = " + 
                                ejbName);
                    continue;
                }
                ejbRefInfo.setJNDIName(ejbJNDIName);
            }
        }

        sbInfo.setEJBReferenceInfo(ejbReferenceInfoList);

        java.util.List<org.glassfish.openesb.addons.appverifier.javaee.ejb.descriptor.ResourceEnvRefType> resEnvRefTypeList = 
            sbt.getResourceEnvRef();
        List<ResourceInfo> listOfResInfo = new ArrayList<ResourceInfo>();
        Properties resourceProperties = 
            this.mMapEjbNameResourceMap.get(ejbName);
        String resName = null;
        String resType = null;
        String resJNDIName = null;
        OutboundRARValidator outRARValidator = 
            OutboundRARValidator.getInstance();
        ResourceInfo resInfo = null;
        for (org.glassfish.openesb.addons.appverifier.javaee.ejb.descriptor.ResourceEnvRefType resEnvRefType: 
             resEnvRefTypeList) {
            resInfo = new ResourceInfo();
            resType = resEnvRefType.getResourceEnvRefType().getValue();
            resName = resEnvRefType.getResourceEnvRefName().getValue();
            resJNDIName = resourceProperties.getProperty(resName);
            if (resJNDIName == null) {
                eb.addError(mRb.getString(RESOURCE_JNDI_NOT_FOUND) + resName);
                resInfo.setMessage(mRb.getString(RESOURCE_JNDI_NOT_FOUND) + 
                                   resName);
                resInfo.setStatus(VerifyStatus.ERROR);
            }


            if (outRARValidator.isResourceLocal(resJNDIName)) {
                if (!outRARValidator.isValidLocalResource(resJNDIName, resType, 
                                                          outboundRARList, 
                                                          eb)) {
                    eb.addError(mRb.getString(ERROR_INVALID_JNDI_NAME) + 
                                resJNDIName + 
                                mRb.getString(RESOURCE_JNDI_NOT_FOUND) + 
                                resName);
                    resInfo.setMessage(mRb.getString(ERROR_INVALID_JNDI_NAME) + 
                                       resJNDIName + 
                                       mRb.getString(RESOURCE_JNDI_NOT_FOUND) + 
                                       resName);
                    resInfo.setStatus(VerifyStatus.ERROR);
                }
            } else {
                if (!raHlpr.isGlobalResource(resJNDIName, resType, eb)) {
                    resInfo.setStatus(VerifyStatus.ERROR);
                    eb.addError(mRb.getString(GLOBAL_RESOURCE_JNDI_NAME) + 
                                resJNDIName + mRb.getString(NOT_FOUND));
                    resInfo.setMessage(mRb.getString(GLOBAL_RESOURCE_JNDI_NAME) + 
                                       resJNDIName + mRb.getString(NOT_FOUND));
                }
            }

            resInfo.setResourceJNDIName(resJNDIName);
            resInfo.setResourceRefName(resName);
            resInfo.setResourceType(resType);
            listOfResInfo.add(resInfo);

        }

        java.util.List<org.glassfish.openesb.addons.appverifier.javaee.ejb.descriptor.ResourceRefType> resRefTypeList = 
            sbt.getResourceRef();
        for (org.glassfish.openesb.addons.appverifier.javaee.ejb.descriptor.ResourceRefType resRefType: 
             resRefTypeList) {
            resInfo = new ResourceInfo();
            resType = resRefType.getResType().getValue();
            resName = resRefType.getResRefName().getValue();
            resJNDIName = resourceProperties.getProperty(resName);
            if (resJNDIName == null) {
                resInfo.setMessage(mRb.getString(RESOURCE_JNDI_NOT_FOUND) + 
                                   resName);
                resInfo.setStatus(VerifyStatus.ERROR);
                eb.addError(mRb.getString(RESOURCE_JNDI_NOT_FOUND) + resName);
            }

            if (outRARValidator.isResourceLocal(resJNDIName)) {
                if (!outRARValidator.isValidLocalResource(resJNDIName, resType, 
                                                          outboundRARList, 
                                                          eb)) {

                    resInfo.setMessage(mRb.getString(ERROR_INVALID_JNDI_NAME) + 
                                       resJNDIName + 
                                       mRb.getString(RESOURCE_JNDI_NOT_FOUND) + 
                                       resName);
                    resInfo.setStatus(VerifyStatus.ERROR);
                    eb.addError(mRb.getString(ERROR_INVALID_JNDI_NAME) + 
                                resJNDIName + 
                                mRb.getString(RESOURCE_JNDI_NOT_FOUND) + 
                                resName);
                }
            } else {
                if (!raHlpr.isGlobalResource(resJNDIName, resType, eb)) {
                    resInfo.setMessage(mRb.getString(GLOBAL_RESOURCE_JNDI_NAME) + 
                                       resJNDIName + mRb.getString(NOT_FOUND));
                    resInfo.setStatus(VerifyStatus.ERROR);
                    eb.addError(mRb.getString(GLOBAL_RESOURCE_JNDI_NAME) + 
                                resJNDIName + mRb.getString(NOT_FOUND));
                }
            }
            resInfo.setResourceJNDIName(resJNDIName);
            resInfo.setResourceRefName(resName);
            resInfo.setResourceType(resType);
            listOfResInfo.add(resInfo);

        }
        sbInfo.setResourceReferenceInfo(listOfResInfo);

    }


    public List<EJBInfo> getEJBList() {
        return this.mEJBList;
    }


    private void populateSunejbInfo(SunEjbJar sunejb) {
        org.glassfish.openesb.addons.appverifier.javaee.sunejb.descriptor.EnterpriseBeans entBeans = 
            sunejb.getEnterpriseBeans();
        java.util.List<org.glassfish.openesb.addons.appverifier.javaee.sunejb.descriptor.Ejb> listOfEJB = 
            entBeans.getEjb();
        String ejbName = null;
        Properties jndiMapProperties = null;
        for (org.glassfish.openesb.addons.appverifier.javaee.sunejb.descriptor.Ejb ejb: 
             listOfEJB) {
            ejbName = ejb.getEjbName();
            jndiMapProperties = new Properties();
            this.mMapEjbNameResourceMap.put(ejbName, jndiMapProperties);
            mMapEjbJNDINameMap.put(ejbName, ejb.getJndiName());
            java.util.List<org.glassfish.openesb.addons.appverifier.javaee.sunejb.descriptor.ResourceEnvRef> resEnvRefList = 
                ejb.getResourceEnvRef();

            for (org.glassfish.openesb.addons.appverifier.javaee.sunejb.descriptor.ResourceEnvRef resEnvRef: 
                 resEnvRefList) {
                jndiMapProperties.setProperty(resEnvRef.getResourceEnvRefName(), 
                                              resEnvRef.getJndiName());
            }
            java.util.List<org.glassfish.openesb.addons.appverifier.javaee.sunejb.descriptor.ResourceRef> resRefList = 
                ejb.getResourceRef();
            for (org.glassfish.openesb.addons.appverifier.javaee.sunejb.descriptor.ResourceRef resRef: 
                 resRefList) {
                jndiMapProperties.setProperty(resRef.getResRefName(), 
                                              resRef.getJndiName());
            }
            org.glassfish.openesb.addons.appverifier.javaee.sunejb.descriptor.MdbResourceAdapter mdbResAdapter = 
                ejb.getMdbResourceAdapter();
            if (mdbResAdapter != null) {
                String resourceMID = mdbResAdapter.getResourceAdapterMid();
                if (resourceMID != null) {
                    this.mMapEjbNameResourceMID.setProperty(ejbName, 
                                                            resourceMID);
                }
            }

            java.util.List<org.glassfish.openesb.addons.appverifier.javaee.sunejb.descriptor.EjbRef> ejbRefList = 
                ejb.getEjbRef();
            if (ejbRefList.size() > 0) {
                Properties propEjbRef = new Properties();
                this.mMapEjbNameEjbRefMap.put(ejbName, propEjbRef);
                for (org.glassfish.openesb.addons.appverifier.javaee.sunejb.descriptor.EjbRef ejbRef: 
                     ejbRefList) {
                    propEjbRef.setProperty(ejbRef.getEjbRefName(), 
                                           ejbRef.getJndiName());
                }
            }


        }
    }
}
