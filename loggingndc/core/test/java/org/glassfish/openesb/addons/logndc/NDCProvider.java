/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * 
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 * 
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common Development
 * and Distribution License("CDDL") (collectively, the "License").  You
 * may not use this file except in compliance with the License. You can obtain
 * a copy of the License at https://glassfish.dev.java.net/public/CDDL+GPL.html
 * or glassfish/bootstrap/legal/LICENSE.txt.  See the License for the specific
 * language governing permissions and limitations under the License.
 * 
 * When distributing the software, include this License Header Notice in each
 * file and include the License file at glassfish/bootstrap/legal/LICENSE.txt.
 * Sun designates this particular file as subject to the "Classpath" exception
 * as provided by Sun in the GPL Version 2 section of the License file that
 * accompanied this code.  If applicable, add the following below the License
 * Header, with the fields enclosed by brackets [] replaced by your own
 * identifying information: "Portions Copyrighted [year]
 * [name of copyright owner]"
 * 
 * Contributor(s):
 * 
 * If you wish your version of this file to be governed by only the CDDL or
 * only the GPL Version 2, indicate your decision by adding "[Contributor]
 * elects to include this software in this distribution under the [CDDL or GPL
 * Version 2] license."  If you don't indicate a single choice of license, a
 * recipient has the option to distribute your version of this file under
 * either the CDDL, the GPL Version 2 or to extend the choice of license to
 * its licensees as provided above.  However, if you add GPL Version 2 code
 * and therefore, elected the GPL Version 2 license, then the option applies
 * only if the new code is made subject to such option by the copyright
 * holder.
 */

package org.glassfish.openesb.addons.logndc;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.LogRecord;
import java.util.logging.Logger;

/**
 * A Glassfish lifecycle listener that extends the logging system with log4j style
 * Nested Diagnostics Context functionality (NDC).
 * 
 * @author fkieviet
 */
public class NDCProvider {
    public static final String ENTERCONTEXT = "com.sun.EnterContext";
    public static final String EXITCONTEXT = "com.sun.ExitContext";
    public static final String CONTEXT = "Context";
    
    private ThreadLocal<org.glassfish.openesb.addons.logndc.NDCProvider.NDCStack> mThreadContexts = new ThreadLocal<org.glassfish.openesb.addons.logndc.NDCProvider.NDCStack>();
    private ContextInserter mKVHandler;


    // ===================================================================================

    /**
     * Logger that delegates all logging methods to a process() method
     * 
     * @author fkieviet
     */
    public abstract class NDCLogger extends Logger {
        protected NDCLogger(String name, String resourceBundleName) {
            super(name, resourceBundleName);
        }

        /**
         * @param o Object to push or pop
         * @return error string
         */
        public abstract String process(Object o);

        @Override
        public void log(Level level, String msg, Object param1) {
            String error = process(param1);
            if (error != null) {
                LogRecord lr = new LogRecord(Level.WARNING, error);
                lr.setLoggerName(getName());
                lr.setThrown(new Exception(error));
                log(lr);
            }
        }
        
        @Override
        public void log(Level level, String msg) {
            log(level, msg, (Object) msg);
        }

        @Override
        public void log(Level level, String msg, Object[] params) {
            log(level, msg, (Object) params);
        }

        @Override
        public void config(String msg) {
            log(Level.CONFIG, msg, msg);
        }

        @Override
        public void fine(String msg) {
            log(Level.FINE, msg, msg);
        }

        @Override
        public void finer(String msg) {
            log(Level.FINER, msg, msg);
        }

        @Override
        public void finest(String msg) {
            log(Level.FINEST, msg, msg);
        }

        @Override
        public void info(String msg) {
            log(Level.INFO, msg, msg);
        }

        @Override
        public void severe(String msg) {
            log(Level.SEVERE, msg, msg);
        }

        @Override
        public void warning(String msg) {
            log(Level.WARNING, msg, msg);
        }
    }

    
    // ===================================================================================
    
    /**
     * Specialized logger that pushes a context onto the context stack
     * 
     * @author fkieviet
     */
    public class NDCEntryLogger extends NDCLogger {
        protected NDCEntryLogger(String name, String resourceBundleName) {
            super(name, resourceBundleName);
        }
        
        @Override
        public String process(Object o) {
            NDCStack ndc = getNDCStack();
            return ndc.push(o);
        }
    }

    
    // ===================================================================================
    
    /**
     * Specialized logger that pops a context off the context stack
     * 
     * @author fkieviet
     */
    public class NDCExitLogger extends NDCLogger {
        protected NDCExitLogger(String name, String resourceBundleName) {
            super(name, resourceBundleName);
        }

        @Override
        public String process(Object o) {
            NDCStack ndc = getNDCStack();
            return ndc.pop(o);
        }
    }

    
    // ===================================================================================

    /**
     * A java.util.logging.Handler that intercepts log messages and changes them: it 
     * adds the context as a Map as an extra argument to the parameters field of the 
     * log record.
     * 
     * @author fkieviet
     */
    private class ContextInserter extends Handler {
        @Override
        public void close() throws SecurityException {
        }

        @Override
        public void flush() {
        }

        @Override
        public void publish(LogRecord record) {
            NDCStack contexts = getNDCStack();
            Map contextlist = contexts.get();
            if (contextlist != null) {
                Object[] old = record.getParameters();
                Object[] nw;
                if (old == null) {
                    nw = new Object[] { contextlist };
                } else {
                    nw = new Object[old.length + 1];
                    System.arraycopy(old, 0, nw, 0, old.length);
                    nw[old.length] = contextlist;
                }
                record.setParameters(nw);
            }
        }
    }

    
    // ===================================================================================

    /**
     * A fixed size stack of contexts
     *
     * Care must be taken to correctly pushes and pops. If unbalanced, overflows
     * or underflows are likely. The following protection and detection are built in:
     * when popping a context, it will be checked to see that the context is infact
     * equal to the current context. If not, the message that will be logged will
     * contain an error indicating this.
     */
    private static class NDCStack {
        private static final int N = 100;
        private FlexMap[] mStack = new FlexMap[N]; 
        private int mSize;
        
        private String pretty(Object o) {
            if (o instanceof String) {
                return o.toString();
            } else if (o instanceof Object[]) {
                Object[] values = (Object[]) o;
                StringBuffer ret = new StringBuffer();
                for (int i = 0; i < values.length; i += 2) {
                    if (ret.length() != 0) {
                        ret.append(", ");
                    }
                    ret.append(values[i]).append("=").append(values[i + 1]);
                }
                return ret.toString();
            } else if (o instanceof Map) {
                Map m = (Map) o;
                StringBuffer ret = new StringBuffer();
                for (Iterator iter = m.entrySet().iterator(); iter.hasNext();) {
                    Map.Entry v = (Map.Entry) iter.next();
                    if (ret.length() != 0) {
                        ret.append(", ");
                    }
                    ret.append(v.getKey()).append("=").append(v.getValue());
                }
                return ret.toString();
            } else {
                return o.toString();
            }
        }
        
        /**
         * Pushes a new context
         *
         * @param c Context
         */
        public final String push(Object c) {
            if (c == null) {
                throw new RuntimeException("Cannot push null object");
            }
            
            String ret = null;
            if (mSize < N) {
                if (mStack[mSize] == null) {
                    mStack[mSize] = new FlexMap();
                }
                
                // Parameter validation happens here; exception may occur
                mStack[mSize].set(c);
            } else {
                // Stack overflow... ignore
                ret = "Context error: number of contexts (" + mSize
                    + ") exceeds maximum (" + N + "); the context [" + pretty(c)
                    + "] will not be set";
            }
            mSize++;
            return ret;
        }
        
        /**
         * Unsets the current context; unbalanced pops are ignored
         *
         * @param c String must be the same as the context that was pushed.
         * @return error
         */
        public final String pop(Object c) {
            String error = null;
            if (mSize == 0) {
                // Error: unbalanced pops (too many pops)
                error = "Context error: the stack is empty; context [" + pretty(c)
                + "] cannot be popped off the context stack.";
            } else {
                mSize--;
                if (mSize < N) {
                    if (mStack[mSize].is(c)) {
                        // Normal pop
                        mStack[mSize].set(null);
                    } else {
                        // Find matching entry (if any)
                        int iMatch = -1;
                        for (int i = mSize; i >= 0; i--) {
                            if (mStack[i].is(c)) {
                                iMatch = i;
                                break;
                            }
                        }

                        if (iMatch < 0) {
                            // No match found
                            error = "Context error: context [" + pretty(c)
                            + "] cannot be popped off the context stack because it is " 
                            + "not present on the the stack";
                            // Undo pop
                            mSize++;
                        } else {
                            // Match found: report
                            StringBuffer others = new StringBuffer();
                            for (int i = mSize; i > iMatch; i--) {
                                if (i != mSize) {
                                    others.append(", ");
                                }
                                others.append(pretty(mStack[i]));
                            }
                            error = "Context warning: unbalanced context stack: there are " 
                                + "contexts [" + others.toString() + "] that were left on "
                                + "the stack; these will be removed by "
                                + "context [" + pretty(c) + "]";
                            // Reset stack
                            for (int i = mSize; i > iMatch; i--) {
                                mStack[i].set(null);
                            }
                            mSize = iMatch;
                        }
                    }
                }
            }
            return error;
        }

        /**
         * Gets the current context
         *
         * @return String null if no context
         */
        public final Map get() {
            if (mSize == 0) {
                return null;
            }
            return mStack[mSize < N ? mSize - 1 : N - 1];
        }
    }

    
    // ===================================================================================

    /**
     * A Map that takes a String, Object[] or Map as its input. The purpose for this is
     * to avoid object creation of Map objects. In case the input is a String or 
     * Object[], a map will be created if the map is accessed. This mirrors standard 
     * usage where contexts are pushed and popped but not logged which results in no
     * object creation; only in case of actual logging, a map is created.
     * 
     *  This class has some characteristics of an old-fashioned union; the non-value
     *  indicates the type.
     */
    private static class FlexMap implements Map {
        private Object[] mValues;
        private Object mValue;
        private Map mDelegate;
        
        /**
         * Associates an object value with this union
         * 
         * @param value object, either Object[], Map, or Object
         */
        public void set(Object value) {
            if (value instanceof Object[]) {
                mDelegate = null;
                Object[] v = (Object[]) value;
                if (v.length == 0) {
                    throw new RuntimeException("Cannot set null array");
                }
                if (v.length % 2 != 0) {
                    throw new RuntimeException("Array length must be a multiple of two");
                }
                mValues = v;
                mValue = null;
            } else if (value instanceof Map) {
                mDelegate = (Map) value;
                mValues = null;
                mValue = null;
            } else {
                mDelegate = null;
                mValues = null;
                mValue = value;
            }
        }

        /**
         * Checks to see if the specified object is equivalent to this object: the
         * types and values must match
         * 
         * @param o object
         * @return true if equal
         */
        public boolean is(Object o) {
            if (o instanceof Object[]) {
                if (o == mValues) {
                    return true;
                }
                return Arrays.equals((Object[]) o, mValues);
            } else if (o instanceof Map) {
                if (o == mDelegate) {
                    return true;
                }
                return o.equals(mDelegate);
            } else {
                return o.equals(mValue);
            }
        }
        
        public void clear() {
            throw new IllegalStateException("Read only");
        }
        
        @SuppressWarnings("unchecked")
        private Map getMap() {
            if (mDelegate == null) {
                mDelegate = new HashMap<String, String>();
                if (mValue != null) {
                    mDelegate.put(CONTEXT, mValue.toString());
                } else {
                    for (int i = 0; i < mValues.length; i += 2) {
                        mDelegate.put(mValues[i].toString(), mValues[i + 1].toString());
                    }
                }
            }
            return mDelegate;
        }

        public boolean containsKey(Object key) {
            return getMap().containsKey(key);
        }

        public boolean containsValue(Object value) {
            return getMap().containsValue(value);
        }

        public Set entrySet() {
            return getMap().entrySet();
        }

        public Object get(Object key) {
            return getMap().get(key);
        }

        public boolean isEmpty() {
            return getMap().isEmpty();
        }

        public Set keySet() {
            return getMap().keySet();
        }

        public Object put(Object key, Object value) {
            throw new IllegalStateException("Read only");
        }

        public void putAll(Map t) {
            throw new IllegalStateException("Read only");
        }

        public Object remove(Object key) {
            throw new IllegalStateException("Read only");
        }

        public int size() {
            return getMap().size();
        }

        public Collection values() {
            return getMap().values();
        }

        @Override
        public String toString() {
            if (mValue != null) {
                return mValue.toString();
            } else if (mValues != null) {
                StringBuffer ret = new StringBuffer();
                for (int i = 0; i < mValues.length; i += 2) {
                    if (ret.length() != 0) {
                        ret.append(", ");
                    }
                    ret.append(mValues[i]).append("=").append(mValues[i + 1]);
                }
                return ret.toString();
            } else {
                StringBuffer ret = new StringBuffer();
                for (Iterator iter = mDelegate.entrySet().iterator(); iter.hasNext();) {
                    Map.Entry v = (Map.Entry) iter.next();
                    if (ret.length() != 0) {
                        ret.append(", ");
                    }
                    ret.append(v.getKey()).append("=").append(v.getValue());
                }
                return ret.toString();
            }
        }
    }

    
    // ===================================================================================
    
    /**
     * Obsolete default constructor
     */
    public NDCProvider() {
    }

    /**
     * Initializes the contexts system 
     */
    public NDCProvider(boolean noop) {
        if (noop) {
            return;
        }
        
        // Add appender
        Logger root = Logger.getLogger("");
        mKVHandler = new ContextInserter();
        Handler h = mKVHandler;
        synchronized (root) {
            Handler[] roothandlers = root.getHandlers();
            for (int i = 0; i < roothandlers.length; i++) {
                root.removeHandler(roothandlers[i]);
            }
            root.addHandler(h);
            for (int i = 0; i < roothandlers.length; i++) {
                root.addHandler(roothandlers[i]);
            }
        }
        
        // Create entry and exit loggers.
        // Sync on Logger.class to avoid deadlock: the server log manager locks on 
        // _unInitializedLoggers and then tries to call Logger.getLogger() which locks
        // Logger.class; other calls to Logger.getLogger() will lock this first, and then
        // try to lock _unInitializedLoggers when Logger calls addLogger(), resulting in 
        // a classic deadlock of thread 1 locks A, then B, while thread 2 locks B then A.
        synchronized (Logger.class) {
            LogManager.getLogManager().addLogger(new NDCEntryLogger(ENTERCONTEXT, null));
            LogManager.getLogManager().addLogger(new NDCEntryLogger("com.stc.EnterContext", null));
            LogManager.getLogManager().addLogger(new NDCExitLogger(EXITCONTEXT, null));
            LogManager.getLogManager().addLogger(new NDCExitLogger("com.stc.ExitContext", null));
        }
    }

    private NDCStack getNDCStack() {
        NDCStack ndc = (NDCStack) mThreadContexts.get();
        if (ndc == null) {
            ndc = new NDCStack();
            mThreadContexts.set(ndc);
        }
        return ndc;
    }

    /**
     * Ensures that the handler is still connected to the root logger (mainly used for
     * testing)
     */
    public void checkHandler() {
        Logger root = Logger.getLogger("");
        synchronized (root) {
            Handler[] roothandlers = root.getHandlers();
            for (int i = 0; i < roothandlers.length; i++) {
                root.removeHandler(roothandlers[i]);
            }
            root.addHandler(mKVHandler);
            for (int i = 0; i < roothandlers.length; i++) {
                if (roothandlers[i] != mKVHandler) {
                    root.addHandler(roothandlers[i]);
                }
            }
        }
    }
}
