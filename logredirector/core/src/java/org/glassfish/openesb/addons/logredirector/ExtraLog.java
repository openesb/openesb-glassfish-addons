/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * 
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 * 
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common Development
 * and Distribution License("CDDL") (collectively, the "License").  You
 * may not use this file except in compliance with the License. You can obtain
 * a copy of the License at https://glassfish.dev.java.net/public/CDDL+GPL.html
 * or glassfish/bootstrap/legal/LICENSE.txt.  See the License for the specific
 * language governing permissions and limitations under the License.
 * 
 * When distributing the software, include this License Header Notice in each
 * file and include the License file at glassfish/bootstrap/legal/LICENSE.txt.
 * Sun designates this particular file as subject to the "Classpath" exception
 * as provided by Sun in the GPL Version 2 section of the License file that
 * accompanied this code.  If applicable, add the following below the License
 * Header, with the fields enclosed by brackets [] replaced by your own
 * identifying information: "Portions Copyrighted [year]
 * [name of copyright owner]"
 * 
 * Contributor(s):
 * 
 * If you wish your version of this file to be governed by only the CDDL or
 * only the GPL Version 2, indicate your decision by adding "[Contributor]
 * elects to include this software in this distribution under the [CDDL or GPL
 * Version 2] license."  If you don't indicate a single choice of license, a
 * recipient has the option to distribute your version of this file under
 * either the CDDL, the GPL Version 2 or to extend the choice of license to
 * its licensees as provided above.  However, if you add GPL Version 2 code
 * and therefore, elected the GPL Version 2 license, then the option applies
 * only if the new code is made subject to such option by the copyright
 * holder.
 */

package org.glassfish.openesb.addons.logredirector;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;

/**
 * A log handler that uses a modified GlassFish log handler and adds an advanced filter
 * that uses a SQL syntax.
 * 
 * <h3>Configuration properties</h3>
 * <ul>
 *   <li>filename: the filename of the log. The file will be written to the same directory
 *       as server.log. Default: extra.log
 *   <li>stacktrace: valid values are 0 or 1. Default: 1. A value of 0 indicates that 
 *        stacktraces should not be written to the log.
 *   <li>selector: see below
 *   <li>maxfilesize: the maximum size of a log file in bytes. Default: 2,000,000 bytes
 *   <li>maxtime: maximum file rotation time in seconds. 0 means no time 
 *   based log file rotation. Default: 0
 *   <li>level: cut-off log level. Log messages with a level lower than the specified level
 *   will not be logged. The value should be one of 
 *    <ul>
 *    <li>SEVERE (highest value)
 *    <li>WARNING
 *    <li>INFO
 *    <li>CONFIG
 *    <li>FINE
 *    <li>FINER
 *    <li>FINEST  (lowest value)
 *    </ul>
 *    Default: CONFIG
 * </ul>
 * 
 * The selector is modeled after the javax.jms message selector. The comments below are 
 * a copy of those, modified where appropriate
 * 
 * 
  * <H3>Selector</H3>
  *
  * <P>A log message has a number of fields that can be used in the selector. A 
  * selector matches a log message if the selector evaluates to 
  * true when the log message's field values are 
  * substituted for their corresponding identifiers in the selector.
  *
  * <P>The syntax of a selector is based on a 
  * subset of 
  * the SQL92 conditional expression syntax. 
  *
  * <P>The order of evaluation of a selector is from left to right 
  * within precedence level. Parentheses can be used to change this order.
  *
  * <P>Predefined selector literals and operator names are shown here in 
  * uppercase; however, they are case insensitive.
  *
  * <P>A selector can contain:
  *
  * <UL>
  *   <LI>Literals:
  *   <UL>
  *     <LI>A string literal is enclosed in single quotes, with a single quote 
  *         represented by doubled single quote; for example, 
  *         <CODE>'literal'</CODE> and <CODE>'literal''s'</CODE>. Like 
  *         string literals in the Java programming language, these use the 
  *         Unicode character encoding.
  *     <LI>An exact numeric literal is a numeric value without a decimal 
  *         point, such as <CODE>57</CODE>, <CODE>-957</CODE>, and  
  *         <CODE>+62</CODE>; numbers in the range of <CODE>long</CODE> are 
  *         supported. Exact numeric literals use the integer literal 
  *         syntax of the Java programming language.
  *     <LI>An approximate numeric literal is a numeric value in scientific 
  *         notation, such as <CODE>7E3</CODE> and <CODE>-57.9E2</CODE>, or a 
  *         numeric value with a decimal, such as <CODE>7.</CODE>, 
  *         <CODE>-95.7</CODE>, and <CODE>+6.2</CODE>; numbers in the range of 
  *         <CODE>double</CODE> are supported. Approximate literals use the 
  *         floating-point literal syntax of the Java programming language.
  *     <LI>The boolean literals <CODE>TRUE</CODE> and <CODE>FALSE</CODE>.
  *   </UL>
  *   <LI>Identifiers:
  *   <UL>
  *     <LI>For a list of defined identifiers, see below.
  *     <LI>An identifier is an unlimited-length sequence of letters 
  *         and digits, the first of which must be a letter. A letter is any 
  *         character for which the method <CODE>Character.isJavaLetter</CODE>
  *         returns true. This includes <CODE>'_'</CODE> and <CODE>'$'</CODE>.
  *         A letter or digit is any character for which the method 
  *         <CODE>Character.isJavaLetterOrDigit</CODE> returns true.
  *     <LI>Identifiers cannot be the names <CODE>NULL</CODE>, 
  *         <CODE>TRUE</CODE>, and <CODE>FALSE</CODE>.
  *     <LI>Identifiers cannot be <CODE>NOT</CODE>, <CODE>AND</CODE>, 
  *         <CODE>OR</CODE>, <CODE>BETWEEN</CODE>, <CODE>LIKE</CODE>, 
  *         <CODE>IN</CODE>, <CODE>IS</CODE>, or <CODE>ESCAPE</CODE>.
  *     <LI>Identifiers are case-sensitive.
  *   </UL>
  *   <LI>White space is the same as that defined for the Java programming
  *       language: space, horizontal tab, form feed, and line terminator.
  *   <LI>Expressions: 
  *   <UL>
  *     <LI>A selector is a conditional expression; a selector that evaluates 
  *         to <CODE>true</CODE> matches; a selector that evaluates to 
  *         <CODE>false</CODE> or unknown does not match.
  *     <LI>Arithmetic expressions are composed of themselves, arithmetic 
  *         operations, identifiers (whose value is treated as a numeric 
  *         literal), and numeric literals.
  *     <LI>Conditional expressions are composed of themselves, comparison 
  *         operations, and logical operations.
  *   </UL>
  *   <LI>Standard bracketing <CODE>()</CODE> for ordering expression evaluation
  *      is supported.
  *   <LI>Logical operators in precedence order: <CODE>NOT</CODE>, 
  *       <CODE>AND</CODE>, <CODE>OR</CODE>
  *   <LI>Comparison operators: <CODE>=</CODE>, <CODE>></CODE>, <CODE>>=</CODE>,
  *       <CODE><</CODE>, <CODE><=</CODE>, <CODE><></CODE> (not equal)
  *   <UL>
  *     <LI>Only like type values can be compared. One exception is that it 
  *         is valid to compare exact numeric values and approximate numeric 
  *         values; the type conversion required is defined by the rules of 
  *         numeric promotion in the Java programming language. If the 
  *         comparison of non-like type values is attempted, the value of the 
  *         operation is false. If either of the type values evaluates to 
  *         <CODE>NULL</CODE>, the value of the expression is unknown.   
  *     <LI>String and boolean comparison is restricted to <CODE>=</CODE> and 
  *         <CODE><></CODE>. Two strings are equal 
  *         if and only if they contain the same sequence of characters.
  *   </UL>
  *   <LI>Arithmetic operators in precedence order:
  *   <UL>
  *     <LI><CODE>+</CODE>, <CODE>-</CODE> (unary)
  *     <LI><CODE>*</CODE>, <CODE>/</CODE> (multiplication and division)
  *     <LI><CODE>+</CODE>, <CODE>-</CODE> (addition and subtraction)
  *     <LI>Arithmetic operations must use numeric promotion in the Java 
  *         programming language.
  *   </UL>
  *   <LI><CODE><I>arithmetic-expr1</I> [NOT] BETWEEN <I>arithmetic-expr2</I> 
  *       AND <I>arithmetic-expr3</I></CODE> (comparison operator)
  *   <UL>
  *     <LI><CODE>"age&nbsp;BETWEEN&nbsp;15&nbsp;AND&nbsp;19"</CODE> is 
  *         equivalent to 
  *         <CODE>"age&nbsp;>=&nbsp;15&nbsp;AND&nbsp;age&nbsp;<=&nbsp;19"</CODE>
  *     <LI><CODE>"age&nbsp;NOT&nbsp;BETWEEN&nbsp;15&nbsp;AND&nbsp;19"</CODE> 
  *         is equivalent to 
  *         <CODE>"age&nbsp;<&nbsp;15&nbsp;OR&nbsp;age&nbsp;>&nbsp;19"</CODE>
  *   </UL>
  *   <LI><CODE><I>identifier</I> [NOT] IN (<I>string-literal1</I>, 
  *       <I>string-literal2</I>,...)</CODE> (comparison operator where 
  *       <CODE><I>identifier</I></CODE> has a <CODE>String</CODE> or 
  *       <CODE>NULL</CODE> value)
  *   <UL>
  *     <LI><CODE>"Country&nbsp;IN&nbsp;('&nbsp;UK',&nbsp;'US',&nbsp;'France')"</CODE>
  *         is true for 
  *         <CODE>'UK'</CODE> and false for <CODE>'Peru'</CODE>; it is 
  *         equivalent to the expression 
  *         <CODE>"(Country&nbsp;=&nbsp;'&nbsp;UK')&nbsp;OR&nbsp;(Country&nbsp;=&nbsp;'&nbsp;US')&nbsp;OR&nbsp;(Country&nbsp;=&nbsp;'&nbsp;France')"</CODE>
  *     <LI><CODE>"Country&nbsp;NOT&nbsp;IN&nbsp;('&nbsp;UK',&nbsp;'US',&nbsp;'France')"</CODE> 
  *         is false for <CODE>'UK'</CODE> and true for <CODE>'Peru'</CODE>; it 
  *         is equivalent to the expression 
  *         <CODE>"NOT&nbsp;((Country&nbsp;=&nbsp;'&nbsp;UK')&nbsp;OR&nbsp;(Country&nbsp;=&nbsp;'&nbsp;US')&nbsp;OR&nbsp;(Country&nbsp;=&nbsp;'&nbsp;France'))"</CODE>
  *     <LI>If identifier of an <CODE>IN</CODE> or <CODE>NOT IN</CODE> 
  *         operation is <CODE>NULL</CODE>, the value of the operation is 
  *         unknown.
  *   </UL>
  *   <LI><CODE><I>identifier</I> [NOT] LIKE <I>pattern-value</I> [ESCAPE 
  *       <I>escape-character</I>]</CODE> (comparison operator, where 
  *       <CODE><I>identifier</I></CODE> has a <CODE>String</CODE> value; 
  *       <CODE><I>pattern-value</I></CODE> is a string literal where 
  *       <CODE>'_'</CODE> stands for any single character; <CODE>'%'</CODE> 
  *       stands for any sequence of characters, including the empty sequence; 
  *       and all other characters stand for themselves. The optional 
  *       <CODE><I>escape-character</I></CODE> is a single-character string 
  *       literal whose character is used to escape the special meaning of the 
  *       <CODE>'_'</CODE> and <CODE>'%'</CODE> in 
  *       <CODE><I>pattern-value</I></CODE>.)
  *   <UL>
  *     <LI><CODE>"phone&nbsp;LIKE&nbsp;'12%3'"</CODE> is true for 
  *         <CODE>'123'</CODE> or <CODE>'12993'</CODE> and false for 
  *         <CODE>'1234'</CODE>
  *     <LI><CODE>"word&nbsp;LIKE&nbsp;'l_se'"</CODE> is true for 
  *         <CODE>'lose'</CODE> and false for <CODE>'loose'</CODE>
  *     <LI><CODE>"underscored&nbsp;LIKE&nbsp;'\_%'&nbsp;ESCAPE&nbsp;'\'"</CODE>
  *          is true for <CODE>'_foo'</CODE> and false for <CODE>'bar'</CODE>
  *     <LI><CODE>"phone&nbsp;NOT&nbsp;LIKE&nbsp;'12%3'"</CODE> is false for 
  *         <CODE>'123'</CODE> or <CODE>'12993'</CODE> and true for 
  *         <CODE>'1234'</CODE>
  *     <LI>If <CODE><I>identifier</I></CODE> of a <CODE>LIKE</CODE> or 
  *         <CODE>NOT LIKE</CODE> operation is <CODE>NULL</CODE>, the value 
  *         of the operation is unknown.
  *   </UL>
  *   <LI><CODE><I>identifier</I> IS NULL</CODE> (comparison operator that tests
  *       for a null header field value or a missing field value)
  *   <UL>
  *     <LI><CODE>"field_name&nbsp;IS&nbsp;NULL"</CODE>
  *   </UL>
  *   <LI><CODE><I>identifier</I> IS NOT NULL</CODE> (comparison operator that
  *       tests for the existence of a non-null header field value or a field
  *       value)
  *   <UL>
  *     <LI><CODE>"field_name&nbsp;IS&nbsp;NOT&nbsp;NULL"</CODE>
  *   </UL>
  *   </ul>
  *
  * <P>The following selector selects log messages with a level of INFO and the logger
  * name should contain jbi:
  *
  * <PRE>"level in ('INFO') and cat like ('%jbi%')"</PRE>
  *
  * <H4>Null Values</H4>
  *
  * <P>As noted above, field values may be <CODE>NULL</CODE>. The evaluation 
  * of selector expressions containing <CODE>NULL</CODE> values is defined by 
  * SQL92 <CODE>NULL</CODE> semantics. A brief description of these semantics 
  * is provided here.
  *
  * <P>SQL treats a <CODE>NULL</CODE> value as unknown. Comparison or arithmetic
  * with an unknown value always yields an unknown value.
  *
  * <P>The <CODE>IS NULL</CODE> and <CODE>IS NOT NULL</CODE> operators convert 
  * an unknown value into the respective <CODE>TRUE</CODE> and 
  * <CODE>FALSE</CODE> values.
  *
  * <P>The boolean operators use three-valued logic as defined by the 
  * following tables:
  *
  * <P><B>The definition of the <CODE>AND</CODE> operator</B>
  *
  * <PRE>
  * | AND  |   T   |   F   |   U
  * +------+-------+-------+-------
  * |  T   |   T   |   F   |   U
  * |  F   |   F   |   F   |   F
  * |  U   |   U   |   F   |   U
  * +------+-------+-------+-------
  * </PRE>
  *
  * <P><B>The definition of the <CODE>OR</CODE> operator</B>
  *
  * <PRE>
  * | OR   |   T   |   F   |   U
  * +------+-------+-------+--------
  * |  T   |   T   |   T   |   T
  * |  F   |   T   |   F   |   U
  * |  U   |   T   |   U   |   U
  * +------+-------+-------+------- 
  * </PRE> 
  *
  * <P><B>The definition of the <CODE>NOT</CODE> operator</B>
  *
  * <PRE>
  * | NOT
  * +------+------
  * |  T   |   F
  * |  F   |   T
  * |  U   |   U
  * +------+-------
  * </PRE>
  *
  * <H4>Special Notes</H4>
  *
  * <P>Date and time values should use the standard <CODE>long</CODE> 
  *    millisecond value. When a date or time literal is included in a 
  *    selector, it should be an integer literal for a millisecond value. The 
  *    standard way to produce millisecond values is to use 
  *    <CODE>java.util.Calendar</CODE>.
  *
  * <P>Although SQL supports fixed decimal comparison and arithmetic, these
  *    selectors do not. This is the reason for restricting exact 
  *    numeric literals to those without a decimal (and the addition of 
  *    numerics with a decimal as an alternate representation for 
  *    approximate numeric values).
  *
  * <P>SQL comments are not supported.
  * 
  * <h3>Supported field values</h3>
  * <p>The following field values are predefined:
  * <ul>
  *   <li>cat, logger, category: logger (string)
  *   <li>level: logging level, non-localized. Typical values are 
  *            SEVERE, WARNING, INFO, CONFIG, FINE, FINER, FINEST (string)
  *   <li>locallevel: localized version of level (string)
  *   <li>msg: the log message (string)
  *   <li>context, Context: the log context (key-value pair where key is "Context" (string)
  *   <li>threadname: the name of the current thread (string)
  *  </ul>
  *  <p>If none of these field values match, the field name will be matched against the
  *  key value pairs that are in the log message, for instance "Service Assembly Name". 
  *  Key value pairs are printed in the log file as part of the group of which _ThreadID
  *  and _ThreadName are part.
  *  Note that spaces in a keyname (e.g. Service Assembly Name) should be replaced
  *  with underscores in the identifier (e.g. Service_Assembly_Name). Example: 
  *  <pre>level = 'ERROR' and Service_Assembly_Name = 'SynchronousApplication'</pre>
  *  An identifier is first resolved without the underscore replacement. If this resolution
  *  does not yield a result, the underscores are replaced with spaces and another attempt
  *  to resolve the identifier is done. As a consequence, keynames that have a mix
  *  of underscores and spaces cannot be expressed in identifiers.
  *   
 * 
 * 
 * @author fkieviet
 */
public class ExtraLog {
    private FileSyslogHandler2 extraHandler;
    private static Logger log = Logger.getLogger(ExtraLog.class.getName());
    
    private static Map<String, ExtraLog> instancesByName = new HashMap<String, ExtraLog>();
    
    /**
     * Creates a new Log and keeps track of it by name
     * 
     * This api may disappear in a future version!
     * 
     * @return a new Log
     * @throws Exception propagated and if name is already used
     */
    public static ExtraLog start(Properties p, String name) throws Exception {
        synchronized (instancesByName) {
            if (instancesByName.get(name) != null) {
                throw new Exception("Instance " + name + " already exists.");
            }
            ExtraLog ret = new ExtraLog(p);
            instancesByName.put(name, ret);
            return ret;
        }
    }
    
    /**
     * Stops the log by name. The log HAS to be created by start(p, name);
     * 
     * This api may disappear in a future version!
     * 
     * @throws Exception propagated, if instance does not exist
     */
    public static void stop(String name) throws Exception {
        synchronized (instancesByName) {
            ExtraLog toStop = instancesByName.remove(name);
            if (toStop == null) {
                throw new Exception("Instance " + name + " could not be found.");
            }
            toStop.stop();
        }
    }
    
    

    /**
     * @return a new Log
     * @throws Exception propagated
     */
    public static ExtraLog start(Properties p) throws Exception {
        return new ExtraLog(p);
    }

    /**
     * Initializes the contexts system 
     * @throws Exception from file handler and selector 
     */
    private ExtraLog(Properties p) throws Exception {
        // Selector
        String selector = p.getProperty("selector");
        final LogFilter filter = (selector != null && selector.length() > 0) ? new LogFilter(selector) : null;
        
        // filename
        final String filename = p.getProperty("filename", "extra.log");
        
        // Stack traces
        final boolean removeStackTraces = Integer.parseInt(p.getProperty("stacktrace", "1")) == 0;
        
        // Level
        final Level level = Level.parse(p.getProperty("level", Level.CONFIG.getName())); 
        
        // Log rotation size / time
        int fileRotationLimit = Integer.parseInt(p.getProperty("maxfilesize", "2000000"));
        fileRotationLimit = fileRotationLimit > 500000 ? fileRotationLimit : 0;
        long fileRotationTimeLimit = Long.parseLong(p.getProperty("maxtime", "0")) * 1000L;
        
        log.info("Creating extra log 'filename'=[" + filename + "]; 'selector'=[" 
            + selector + "]; 'maxfilesize'=[" 
            + fileRotationLimit + "] bytes; 'maxtime'=[" 
            + fileRotationTimeLimit + "] seconds; level=[" + level.getName() + "]");
        
        // Create file handler, take care of error handling
        extraHandler = new FileSyslogHandler2(filename, fileRotationLimit, fileRotationTimeLimit) {
            boolean recursive;
            /**
             * @see org.glassfish.openesb.addons.logredirector.FileSyslogHandler2#publish(java.util.logging.LogRecord)
             */
            @Override
            public synchronized void publish(LogRecord record) {
                if (!recursive && (filter == null || filter.isLoggable(record))) {
                    if (removeStackTraces) {
                        record = new NoStackTraceLogRecord(record);
                    }
                    super.publish(record);
                }
                
                // Check for error
                Exception e;
                if (filter != null && (e = filter.getAndResetException()) != null) {
                    recursive = true;
                    log.log(Level.WARNING, "An error occurred trying to write a log message to the extra log [" 
                        + filename + "]: " + e, e);
                    recursive = false;
                }
            }
        };
        
        extraHandler.setLevel(level);
        
        // Add Handler
        Logger root = Logger.getLogger("");
        synchronized (root) {
            root.addHandler(extraHandler);
        }
    }

    public void stop() {
        Logger root = Logger.getLogger("");
        synchronized (root) {
            root.removeHandler(extraHandler);
        }
        
        extraHandler.close();
    }
}
